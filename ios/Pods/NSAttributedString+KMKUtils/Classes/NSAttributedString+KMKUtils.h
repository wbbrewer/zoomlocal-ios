//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//
#import <UIKit/UIKit.h>


@interface NSAttributedString (KMKUtils)

/**
 Method always returns integer values for width and height.
 If `width` is greater than width that is needed to render string then result width will be less, otherwise returned widht is equal to passed width even if actually needed width is a bit less.
 */
- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width;

// TODO: check correctness of comment
/**
 @param numberOfLines = 0 corresponds the lack of limit for number of lines
 
 Method may be especially useful when it is needed to calculate the height of the label without creating it. For example
 such situation may occur when calculating the height of the cell without creating the cell itself.
 
 @return The size for label with the specified parameters. Returned value is always intergral (values are raised to the nearest hiegher integer value).
 */
- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width
                                numberOfLines:(NSInteger)numberOfLines;

/**
 @return A rectangle whose size component indicates the width and height required to draw the entire contents of the string.
 Method always returns integer values for width and height.
 */
- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width;

@end
