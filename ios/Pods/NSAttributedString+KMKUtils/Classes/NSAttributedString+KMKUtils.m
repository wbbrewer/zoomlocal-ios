//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "NSAttributedString+KMKUtils.h"


@implementation NSAttributedString (KMKUtils)

/**
 @warning
 DESCRIPTION OF WORKAROUND USED IN IMPLEMENTATION:
 It was noticed empirically on the code snippet following below that when using `NSTextAttachment` with small image (16x16) calculated width is smaller on 2 points than it is needed.
 And calculated height was not sufficient to fit image - top edge of image was clipped.
 
 NSMutableAttributedString *durationString = [[NSMutableAttributedString alloc] initWithString:@""];
 NSTextAttachment *textAttachment = [NSTextAttachment new];
 textAttachment.image = [UIImage imageNamed:@"cell_details_schedule"]; // 16x16
 
 [durationString appendAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
 [durationString appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
 
 NSAttributedString *durationText = [[NSAttributedString alloc] initWithString:@"Placeholder"
 attributes: @{
 NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:15]
 }];
 [durationString appendAttributedString:durationText];
 */
- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width
{
    // Width is not taken into account by this method so it does not make sence to set `width` as constraints, changing options doesn't matter.
    CGRect rect = [self boundingRectWithSize:CGSizeMake(0.0, 0.0)
                                     options:NSStringDrawingUsesDeviceMetrics
                                     context:nil];
    const CGFloat kWidthToAddAsWorkaround = 2.0;
    rect.size.width += kWidthToAddAsWorkaround; // description of workaround see in comment to impl of `kmk_oneLineSizeConstrainedToWidth:` method.
    
    const CGFloat kHeightToAddAsWorkaround = 2.0;
    rect.size.height += kHeightToAddAsWorkaround; // description of workaround see in comment to impl of `kmk_oneLineSizeConstrainedToWidth:` method.
    
    rect.size.width = MIN(rect.size.width, width);
    
    rect = CGRectIntegral(rect);
    
    return rect.size;
    
}

// TODO: improve implementation to find correctly height taking into account number of lines
- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width
                                numberOfLines:(NSInteger)numberOfLines
{
    CGRect multilineRect = CGRectZero;
    
    if (numberOfLines > 1) {
        static UILabel *sizingLabel = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sizingLabel = [UILabel new];
        });
        
        sizingLabel.frame = CGRectMake(0, 0, width, 0.0);
        sizingLabel.attributedText = self;
        sizingLabel.numberOfLines = numberOfLines;
        [sizingLabel sizeToFit];
        multilineRect = sizingLabel.bounds;
    }
    else {
        // if `numberOfLines` = 1 then `-[UILabel sizeToFit]` doesn't take into account width constraint and
        // return width needed to fit the whole string
        multilineRect.size = [self kmk_oneLineSizeConstrainedToWidth:width];
    }
    
    CGRect integralRect = CGRectIntegral(multilineRect);
    
    return integralRect.size;
}

- (UIFont *)fontOfTheFirstLetter {
    NSDictionary *attributes = [self attributesAtIndex:0 effectiveRange:NULL];
    UIFont *font = [attributes objectForKey:NSFontAttributeName];
    return font;
}

- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width
{
        // Do not try to implement using method `-[NSString boundingRectWithSize:options:attributes:context:]` as it doesn't work if lineBreakMode in paragraph style is not work or char wrapping.
        // For details why this is done so see comment to `[NSString kmk_boundingRectWithSize:options:attributes:context:]`.
       // line break mode for the whole receiver is set to the mode of the last part which was added to attributed string and in case for example of NSLineBreakByTruncatingTail result will be as if we have one-line label.
    
    CGSize size = [self kmk_multilineSizeConstrainedToWidth:width numberOfLines:2];
    return size;
}

@end
