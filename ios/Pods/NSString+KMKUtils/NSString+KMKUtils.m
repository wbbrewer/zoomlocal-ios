//
//  NSString+KMKUtils.h
//  NSString+KMKUtils
//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "NSString+KMKUtils.h"


@implementation NSString (KMKUtils)


#pragma mark - Factory methods -

+ (NSString *)stringWithGeneratedUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}


#pragma mark - Drawing String in a Given Area -

- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width
                                       font:(UIFont *)font
                              lineBreakMode:(NSLineBreakMode)lineBreakMode {
    
    CGSize size = [self kmk_multilineSizeConstrainedToWidth:width
                                              numberOfLines:1
                                                       font:font
                                              lineBreakMode:lineBreakMode];
    return size;
}

- (CGSize)kmk_multilineSizeConstrainedToSize:(CGSize)size
                                        font:(UIFont *)font
                               lineBreakMode:(NSLineBreakMode)lineBreakMode {
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    if (lineBreakMode == NSLineBreakByTruncatingHead ||
        lineBreakMode == NSLineBreakByTruncatingTail ||
        lineBreakMode == NSLineBreakByTruncatingMiddle ||
        lineBreakMode == NSLineBreakByClipping) {
        
        lineBreakMode = NSLineBreakByCharWrapping;
    }
    
    // Do not use `NSLineBreakByTruncatingTail` because method will return height as if we had one-line label.
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    // Do not use `attributesAtIndex:...` method. See comment to `kmk_boundingRectWithSize:...` method.
    NSDictionary *attributesDictionary = @{
                                           NSFontAttributeName : font,
                                           NSParagraphStyleAttributeName : paragraphStyle
                                           };
    
    
    // NSStringDrawingUsesLineFragmentOrigin - uses the top left corner as origin rather than the baseline (which usually is center)
    // For more details see here https://developer.apple.com/library/ios/documentation/UIKit/Reference/NSAttributedString_UIKit_Additions/#//apple_ref/occ/instm/NSAttributedString/drawWithRect:options:context:
    // NSStringDrawingUsesFontLeading - In typography, leading ( /ˈlɛdɪŋ/) refers to the distance between the baselines of successive lines of type. This flag indicates the call to make use of default line spacing specified by the font
    // DO NOT USE `NSStringDrawingUsesDeviceMetrics` as result will be 0.
    CGRect rect = [self boundingRectWithSize:size
                                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                  attributes:attributesDictionary
                                     context:nil];
    
    CGRect integralRect = CGRectIntegral(rect);
    
    return integralRect.size;
}

- (CGSize)kmk_multilineSizeConstrainedToWidth:(CGFloat)width
                                numberOfLines:(NSInteger)numberOfLines
                                         font:(UIFont *)font
                                lineBreakMode:(NSLineBreakMode)lineBreakMode {
    
    const NSInteger kNumberOfLines = (numberOfLines == 0 ? NSIntegerMax : numberOfLines);
    
    /*
     Constant is added to the number of lines to take into account that final result can be greater than numberOfLines multiplied by lineHeight.
     */
    CGSize constrainingSize = CGSizeMake(width, (kNumberOfLines + 0.99) * [font lineHeight]);
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    // In case of one line height is calculated correctly for all line break modes.
    if (numberOfLines != 1 && (lineBreakMode == NSLineBreakByTruncatingHead || lineBreakMode == NSLineBreakByTruncatingTail ||
                               lineBreakMode == NSLineBreakByTruncatingMiddle || lineBreakMode == NSLineBreakByClipping)) {
        lineBreakMode = NSLineBreakByCharWrapping;
    }
    
    paragraphStyle.lineBreakMode = lineBreakMode;
    
    // Do not use `attributesAtIndex:...` method. See comment to `kmk_boundingRectWithSize:...` method.
    NSDictionary *attributesDictionary = @{
                                           NSFontAttributeName : font,
                                           NSParagraphStyleAttributeName : paragraphStyle
                                           };
    
    // DO NOT USE `NSStringDrawingUsesDeviceMetrics` as result will be 0.
    CGRect rect = [self kmk_boundingRectWithSize:constrainingSize
                                         options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                      attributes:attributesDictionary
                                         context:NULL];
    
    CGRect integralRect = CGRectIntegral(rect);
    
    return integralRect.size;
}

- (CGRect)kmk_boundingRectWithSize:(CGSize)size
                           options:(NSStringDrawingOptions)options
                        attributes:(NSDictionary *)attributes
                           context:(NSStringDrawingContext *)context NS_AVAILABLE_IOS(7_0)
{
    CGRect rect = CGRectZero;
    if (size.height != 0.0) {
        rect = CGRectIntegral([self boundingRectWithSize:size
                                                 options:options
                                              attributes:attributes
                                                 context:context]);
        
        if (rect.size.width > size.width) {
            
            NSAssert([[attributes valueForKey:NSParagraphStyleAttributeName] lineBreakMode] == NSLineBreakByClipping,
                     @"New case when returned height exceeds height constraint: '%d'",
                     (int)[[attributes valueForKey:NSParagraphStyleAttributeName] lineBreakMode]);

            // in case of `NSLineBreakByClipping` width constraint is ignored, we try adhere it
            rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, size.height);
        }

    }
    else {
        rect = CGRectMake(0.0, 0.0, size.width, 0.0);
    }
        
    return rect;
}

@end
