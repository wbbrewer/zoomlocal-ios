//
//  ZLSocialsModel.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 27/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLSocialsModel.h"


@implementation ZLSocialsModel

+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"youtubeLink": @"youtube",
             };
}

@end
