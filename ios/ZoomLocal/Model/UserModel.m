//
//  UserModel.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"userId": @"id",
             @"billingFirstName": @"meta.billing_first_name",
             @"billingLastName": @"meta.billing_last_name",
             @"billingEmail": @"meta.billing_email",
             @"billingAddress1": @"meta.billing_address_1",
             @"billingAddress2": @"meta.billing_address_2",
             @"billingCity": @"meta.billing_city",
             @"billingState": @"meta.billing_state",
             @"billingPostcode": @"meta.billing_postcode",
             @"categoryPreference": @"category_preference",
             @"locationPreference": @"location_preference",
             };
}

@end
