//
//  TokenModel.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "TokenModel.h"

@implementation TokenModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"accessToken": @"access_token",
             @"tokenType": @"token_type",
             @"expiresIn": @"expires_in",
             @"refreshToken": @"refresh_token",
             @"scope": @"scope",
             };
}

@end
