//
//  ZLVendorsModel.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 18/05/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLVendorDetailsModel.h"

#import <GCDMulticastDelegate.h>

#import "ZLWPApiClient.h"


@interface ZLVendorDetailsModel ()

@property (strong, nonatomic) NSNumber *vendorTermId;

@end


@implementation ZLVendorDetailsModel {
    id _multicastDelegate;
}

- (instancetype)initWithVendorTermId:(NSNumber *)vendorTermId
{
    self = [super init];
    if (self) {
        _vendorTermId = vendorTermId;
        _multicastDelegate = [[GCDMulticastDelegate alloc] init];
    }
    return self;
}

#pragma mark - Public -

- (void)update
{
    [self downloadVendorTermWithId:_vendorTermId];
}

#pragma mark Observer

- (void)addObserver:(id<ZLVendorDetailsModelObserver>)observer;
{
    @synchronized(self) {
        [_multicastDelegate addDelegate:observer delegateQueue:dispatch_get_main_queue()];
    }
}

- (void)removeObserver:(id<ZLVendorDetailsModelObserver>)observer;
{
    @synchronized(self) {
        [_multicastDelegate removeDelegate:observer];
    }
}

#pragma mark - Private -

- (void)downloadVendorTermWithId:(NSNumber *)vendorTermId
{
    [_multicastDelegate vendorDetailsModelDidStartUpdating:self];
    
    // Request example: https://listings-dev.zoomlocal.com/wp-json/rest/v2/vendor/555
    [[ZLWPApiClient sharedClient] authorizedGET:[@"wp-json/rest/v2/vendor" stringByAppendingFormat:@"/%@", vendorTermId]
                                     parameters:nil
                                     completion:^(OVCResponse *response, NSError *error)
     {         
         if (!error) {
             self.vendorTerm = (TermModel *)response.result;
             [_multicastDelegate vendorDetailsModel:self didUpdateVendorTermWithError:nil];
         } else {
             [_multicastDelegate vendorDetailsModel:self didUpdateVendorTermWithError:error];

         }
     }];

}

@end
