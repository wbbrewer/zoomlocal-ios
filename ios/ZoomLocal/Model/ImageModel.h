//
//  ImageModel.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/20/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface ImageModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray *thumbnail;
@property (nonatomic, copy) NSArray *medium;
@property (nonatomic, copy) NSArray *large;
@property (nonatomic, copy) NSArray *postThumbnail;

@end
