//
//  ZLVendorsModel.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 18/05/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TermModel.h"

@class ZLVendorDetailsModel;


@protocol ZLVendorDetailsModelObserver <NSObject>

- (void)vendorDetailsModelDidStartUpdating:(ZLVendorDetailsModel *)vendorDetailsModel;

/**
 If success `error` is `nil`.
 */
- (void)vendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel didUpdateVendorTermWithError:(NSError *)error;

@end


@interface ZLVendorDetailsModel : NSObject

- (instancetype)initWithVendorTermId:(NSNumber *)vendorTermId;
- (id)init NS_UNAVAILABLE;

@property (strong, nonatomic) TermModel *vendorTerm;

/**
 Update `vendorTerm`.
 */
- (void)update;

/**
 Repeated invocation of this method leads to notifying the same delegate several times.
 Be sure to invoke  @c removeObserver: before @c observer is deallocated.
 */
- (void)addObserver:(id<ZLVendorDetailsModelObserver>)observer;
- (void)removeObserver:(id<ZLVendorDetailsModelObserver>)observer;

@end
