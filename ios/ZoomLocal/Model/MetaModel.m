//
//  MetaModel.m
//  Zoom Local
//
//  Created by Aric Brown on 11/18/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import "MetaModel.h"


@implementation MetaModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"regularPrice": @"_regular_price",
             @"salePrice": @"_sale_price",
             @"featured": @"_featured",
             @"price": @"_price",
             @"eventStartDate": @"_rq_event_start_date",
             @"eventEndDate": @"_rq_event_end_date",
             @"eventStartTime": @"_rq_event_start_time",
             @"eventStopTime": @"_rq_event_start_time",
             @"eventCountry": @"_rq_event_country_name",
             @"eventAddress": @"_rq_event_address_name",
             @"eventCity": @"_rq_event_city_name",
             @"eventRegion": @"_rq_event_region_name",
             @"eventZip": @"_rq_event_zip_code",
             @"eventLat": @"_rq_event_lat_name",
             @"eventLon": @"_rq_event_lon_name",
             @"couponEndDate": @"coupon_end_date"
             };
}

+ (NSDateFormatter*)dateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    return dateFormatter;
}

+ (NSValueTransformer *)eventStartDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)eventEndDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)couponEndDateJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

- (NSString *)localizedDateTime:(NSDate*)date
{
    return [NSString stringWithFormat:@"%@", [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle]];
}

@end
