//
//  StoreHours.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/21/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface StoreHoursModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray *mon;
@property (nonatomic, copy) NSArray *tue;
@property (nonatomic, copy) NSArray *wed;
@property (nonatomic, copy) NSArray *thu;
@property (nonatomic, copy) NSArray *fri;
@property (nonatomic, copy) NSArray *sat;
@property (nonatomic, copy) NSArray *sun;

@end
