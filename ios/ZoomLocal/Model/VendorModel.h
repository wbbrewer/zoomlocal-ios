//
//  VendorModel.h
//  Zoom Local
//
//  Created by Aric Brown on 9/21/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import <Mantle/Mantle.h>

#import "ImageModel.h"
#import "StoreHoursModel.h"
#import <CoreLocation/CoreLocation.h>

#import "ZLSocialsModel.h"


@interface VendorModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong, readonly) ImageModel *images;
@property (nonatomic, strong, readonly) ImageModel *bannerImages;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *storeDescription;
//owner
//@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *address2;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zip;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, strong, readonly) StoreHoursModel *hours;
@property (nonatomic, copy) NSString *geoloc;
//
@property (nonatomic, strong, readonly) CLLocation *location;
@property (nonatomic, copy) NSString *displayAddress;

// `nil` if user is not authenticated and "Follow" flag otherwise.
@property (nonatomic, copy) NSNumber *isFollowed;
@property (nonatomic, copy) NSNumber *followCount;

@property (nonatomic, copy) ZLSocialsModel *socialsModel;

@end
