//
//  VendorModel.m
//  Zoom Local
//
//  Created by Aric Brown on 9/21/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import "VendorModel.h"

#import "ZLSocialsModel.h"

@implementation VendorModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"images": @"images",
             @"bannerImages": @"banner_images",
             @"name": @"name",
             @"storeDescription": @"description",
             //@"location": @"location",
             @"telephone": @"telephone",
             @"email": @"store_email",
             @"address1": @"address1",
             @"address2": @"address2",
             @"city": @"city",
             @"state": @"state",
             @"zip": @"zip",
             @"website": @"website",
             @"hours": @"hours",
             @"geoloc": @"geoloc",
             @"isFollowed" : @"is_followed",
             @"followCount" : @"follow_count",
             @"socialsModel" : @"socials",
             };
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:ImageModel.class];
}

+ (NSValueTransformer *)hoursJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:StoreHoursModel.class];
}

+ (NSValueTransformer *)socialsJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:ZLSocialsModel.class];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    if (_geoloc.length >0) {
        NSArray *array = [_geoloc componentsSeparatedByString:@","];
        
        _location = [[CLLocation alloc] initWithLatitude:[array[0] floatValue] longitude:[array[1] floatValue]];
    }
    
    _displayAddress = [NSString stringWithFormat:@"%@\r\n%@, %@", _address1, _city, _state];
    
    return self;
}

@end
