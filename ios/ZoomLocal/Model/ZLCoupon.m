//
//  ZLCoupon.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 09/07/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLCoupon.h"

#import "ZLConstants.h"


@implementation ZLCoupon

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *keyPaths = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    keyPaths[@"redeemingDate"] = @"redeeming_date";
    return keyPaths;
}

+ (NSValueTransformer *)redeemingDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [[NSDateFormatter JSONDateFormatter] dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [[NSDateFormatter JSONDateFormatter] stringFromDate:date];
    }];
}

@end
