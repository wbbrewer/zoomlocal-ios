//
//  ProductModel.m
//  Zoom Local
//
//  Created by Aric Brown on 11/17/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import "ProductModel.h"

#import "ZLConstants.h"


@implementation ProductModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"listingId": @"id",
             @"listingDate": @"date",
             @"link": @"link",
             @"listingModified": @"modified",
             @"title": @"title",
             @"shortDesc": @"short_description",
             @"images": @"images",
             @"meta": @"meta",
             @"categories": @"terms.category",
             @"term": @"terms.vendor",
             @"productLink" : @"link",
             @"likes" : @"likes",
             @"isLiked" : @"is_liked"
             };
}

- (NSString*)localizedDateTime:(NSDate*)date {
    return [NSString stringWithFormat:@"%@", [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle]];
}

+ (NSValueTransformer *)listingDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [[NSDateFormatter JSONDateFormatter] dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [[NSDateFormatter JSONDateFormatter] stringFromDate:date];
    }];
}

+ (NSValueTransformer *)listingModifiedJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^(NSString *str, BOOL *success, NSError **error){
        return [[NSDateFormatter JSONDateFormatter] dateFromString:str];
    } reverseBlock:^(NSDate *date, BOOL *success, NSError **error) {
        return [[NSDateFormatter JSONDateFormatter] stringFromDate:date];
    }];
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:ImageModel.class];
}

+ (NSValueTransformer *)metaJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:MetaModel.class];
}

+ (NSValueTransformer *)categoriesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:TermModel.class];
}

+ (NSValueTransformer *)termJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:TermModel.class];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    if (_meta.eventLat.length > 0 && _meta.eventLon.length > 0) {
        _location = [[CLLocation alloc] initWithLatitude:[_meta.eventLat floatValue] longitude:[_meta.eventLon floatValue]];
    } else {
        if (_term.vendor.geoloc.length > 0) {
            NSArray *array = [_term.vendor.geoloc componentsSeparatedByString:@","];
            
            _location = [[CLLocation alloc] initWithLatitude:[array[0] floatValue] longitude:[array[1] floatValue]];
        }
    }
    
    //_displayAddress = [NSString stringWithFormat:@"%@\r\n%@,%@", _address1, _city, _state];
    
    return self;
}

#pragma mark -

- (BOOL)isEqual:(id)other
{
    BOOL isEqual = ([other class] == [self class] && [self isEqualToProductModel:other]);
    return isEqual;
}

- (BOOL)isEqualToProductModel:(ProductModel *)other
{
    BOOL isEqual = ([other.listingId isEqualToNumber:self.listingId]);
    return isEqual;
}

@end
