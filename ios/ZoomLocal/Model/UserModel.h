//
//  UserModel.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface UserModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *userId;
//billing_first_name
@property (nonatomic, copy) NSArray *billingFirstName;
//billing_last_name
@property (nonatomic, copy) NSArray *billingLastName;
//billing_email
@property (nonatomic, copy) NSArray *billingEmail;
//billing_address_1
@property (nonatomic, copy) NSArray *billingAddress1;
//billing_address_2
@property (nonatomic, copy) NSArray *billingAddress2;
//billing_city
@property (nonatomic, copy) NSArray *billingCity;
//billing_state
@property (nonatomic, copy) NSArray *billingState;
//billing_postcode
@property (nonatomic, copy) NSArray *billingPostcode;
//
@property (nonatomic, copy) NSArray *categoryPreference;
@property (nonatomic, copy) NSArray *locationPreference;

@end
