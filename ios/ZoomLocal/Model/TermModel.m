//
//  TermModel.m
//  Zoom Local
//
//  Created by Aric Brown on 9/16/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import "TermModel.h"

@implementation TermModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"termId": @"id",
             @"name": @"name",
             @"slug": @"slug",
             @"taxonomy": @"taxonomy",
             @"termDescription": @"description",
             @"parent": @"parent",
             @"count": @"count",
             @"images": @"termmeta.images",
             @"vendor": @"vendor_meta",
             @"vendorDetailsURL" : @"link",
             };
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:ImageModel.class];
}

+ (NSValueTransformer *)vendorJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:VendorModel.class];
}

@end
