//
//  ZLSocialsModel.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 27/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>


@interface ZLSocialsModel : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *youtubeLink;

@end
