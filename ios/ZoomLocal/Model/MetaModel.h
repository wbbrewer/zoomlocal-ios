//
//  MetaModel.h
//  Zoom Local
//
//  Created by Aric Brown on 11/18/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MetaModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *regularPrice;
@property (nonatomic, copy) NSString *salePrice;
@property (nonatomic, copy) NSString *featured;
@property (nonatomic, copy) NSString *price;

@property (nonatomic, copy) NSDate *eventStartDate;
@property (nonatomic, copy) NSDate *eventEndDate;
@property (nonatomic, copy) NSString *eventStartTime;
@property (nonatomic, copy) NSString *eventStopTime;
@property (nonatomic, copy) NSString *eventCountry;
@property (nonatomic, copy) NSString *eventAddress;
@property (nonatomic, copy) NSString *eventCity;
@property (nonatomic, copy) NSString *eventRegion;
@property (nonatomic, copy) NSString *eventZip;
@property (nonatomic, copy) NSString *eventLat;
@property (nonatomic, copy) NSString *eventLon;

@property (nonatomic, copy) NSDate *couponEndDate;

- (NSString*)localizedDateTime:(NSDate*)date;

@end
