//
//  StoreHours.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/21/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "StoreHoursModel.h"

@implementation StoreHoursModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"mon": @"mon",
             @"tue": @"tue",
             @"wed": @"wed",
             @"thu": @"thu",
             @"fri": @"fri",
             @"sat": @"sat",
             @"sun": @"sun",
             };
}

@end
