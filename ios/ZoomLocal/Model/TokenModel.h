//
//  TokenModel.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface TokenModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSString *tokenType;
@property (nonatomic, copy, readonly) NSNumber *expiresIn;
@property (nonatomic, copy) NSString *refreshToken;
@property (nonatomic, copy) NSString *scope;

@end
