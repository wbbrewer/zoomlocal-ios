//
//  ZLCoupon.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 09/07/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ProductModel.h"


@interface ZLCoupon : ProductModel

// This field is returned by server only when user is logged in.
@property (nonatomic, copy) NSDate *redeemingDate;

@end
