//
//  TermModel.h
//  Zoom Local
//
//  Created by Aric Brown on 9/16/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "ImageModel.h"
#import "VendorModel.h"

@interface TermModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *termId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *slug;
@property (nonatomic, copy) NSString *taxonomy;
@property (nonatomic, copy) NSString *termDescription;
@property (nonatomic, copy, readonly) NSNumber *parent;
@property (nonatomic, readonly) NSInteger count;
@property (nonatomic, strong, readonly) ImageModel *images; // is not used for "Vendor" term
@property (nonatomic, strong, readonly) VendorModel *vendor;
@property (nonatomic, strong, readonly) NSString *vendorDetailsURL; // used for sharing button

@end
