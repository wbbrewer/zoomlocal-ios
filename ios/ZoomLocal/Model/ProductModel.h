//
//  ProductModel.h
//  Zoom Local
//
//  Created by Aric Brown on 11/17/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "ImageModel.h"
#import "MetaModel.h"
#import "TermModel.h"
#import "VendorModel.h"


@interface ProductModel : MTLModel <MTLJSONSerializing>

//product
@property (nonatomic, copy, readonly) NSNumber *listingId;
// This property is not used anywhere
@property (nonatomic, copy) NSDate *listingDate;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSDate *listingModified;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *shortDesc;
@property (nonatomic, copy) NSString *productLink;
@property (nonatomic, copy) NSNumber *likes; // Total number of likes
@property (nonnull, copy) NSNumber *isLiked; // non Null only if user is authenticated. This property has individual value for each user.
//images
@property (nonatomic, strong, readonly) ImageModel *images;
//meta
@property (nonatomic, strong, readonly) MetaModel *meta;
//terms
@property (nonatomic, copy) NSArray *categories;
//vendor
@property (nonatomic, strong, readonly) TermModel *term;
//
@property (nonatomic, strong, readonly) CLLocation *location;

- (NSString*)localizedDateTime:(NSDate*)date;

@end
