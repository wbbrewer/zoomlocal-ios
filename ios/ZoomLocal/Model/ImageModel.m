//
//  ImageModel.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/20/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ImageModel.h"

@implementation ImageModel

#pragma mark - Mantle

+ (NSDictionary*)JSONKeyPathsByPropertyKey
{
    return @{
             @"thumbnail": @"thumbnail",
             @"medium": @"medium",
             @"large": @"large",
             @"postThumbnail": @"post-thumbnail",
             };
}

@end
