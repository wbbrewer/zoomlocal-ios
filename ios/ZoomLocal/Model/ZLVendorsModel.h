//
//  ZLVendorsModel.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 18/05/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TermModel.h"


@interface ZLVendorDetailsModel : NSObject

- (instancetype)initWith

@property (strong, nonatomic) TermModel *vendorTerm;

@end
