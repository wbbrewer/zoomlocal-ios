//
//  ZLItemsCollectionView.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLItemsCollectionView.h"

static double kResponsiveCellHeightCompact = 128.0;
static double kResponsiveCellHeightRegular = 156.0;


@interface ZLItemsCollectionView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (readonly, nonatomic) UICollectionView *collectionView;

@end


@implementation ZLItemsCollectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self common_init];
        _collectionView.frame = frame;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self common_init];
        _collectionView.frame = self.bounds;
    }
    return self;
}

- (void)common_init
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self addSubview:_collectionView];
}

- (void)dealloc
{
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
}

#pragma mark - Override -

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSParameterAssert(self.traitCollection);
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
        self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular) {
        
        self.collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4); // top, left, bottom, right
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 4;
        self.collectionView.collectionViewLayout = flow;
    }
    else {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 1;
        self.collectionView.collectionViewLayout = flow;
    }

}

#pragma mark - Public -

- (UIScrollView *)underlyingScrollView
{
    return _collectionView;
}

- (void)reloadData
{
    [self.collectionView reloadData];
}

- (void)registerNib:(nullable UINib *)nib forCellWithReuseIdentifier:(NSString *)identifier
{
    [_collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
}

- (id)dequeueReusableCellWithReuseIdentifier:(NSString *)identifier forRow:(NSInteger)row
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                forIndexPath:indexPath];
    return cell;
}

- (NSInteger)rowOfSelectedItem
{
    NSIndexPath *indexPath = [_collectionView indexPathsForSelectedItems].firstObject;
    return (indexPath != nil ? indexPath.row : NSNotFound);
}

- (UICollectionViewCell *)cellForItemAtRow:(NSInteger)row
{
    const NSUInteger indexes[2] = {0, row};
    return [self.collectionView cellForItemAtIndexPath:[[NSIndexPath alloc] initWithIndexes:indexes  length:2]];
}

- (NSArray *)visibleCells
{
    return [_collectionView visibleCells];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = [self.dataSource numberOfRowsInItemsCollectionView:self];
    return number;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.dataSource itemsCollectionView:self cellForItemInRow:indexPath.row];
    return cell;
}

#pragma mark
#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_delegate respondsToSelector:@selector(itemsCollectionView:didSelectItemAtRow:)]) {
        [_delegate itemsCollectionView:self didSelectItemAtRow:indexPath.row];
    }
}

#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = CGSizeZero;
    
    // iPad
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
        self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular) {
        cellSize.height = kResponsiveCellHeightRegular;
        cellSize.width = (self.bounds.size.width/2) - 6;
    }
    else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
             self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
        cellSize.height = kResponsiveCellHeightRegular;
        cellSize.width = (self.bounds.size.width/2) - 0.5;
    }
    else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact &&
             self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
        cellSize.height = kResponsiveCellHeightCompact;
        cellSize.width = self.bounds.size.width;
    }
    else {
        cellSize.height = kResponsiveCellHeightCompact;
        cellSize.width = self.bounds.size.width;
    }
    
    return cellSize;
}



@end
