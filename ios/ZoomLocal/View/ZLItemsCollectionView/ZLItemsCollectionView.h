//
//  ZLItemsCollectionView.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZLItemsCollectionViewDataSource;
@protocol ZLItemsCollectionViewDelegate;


/**
 Has 2 columns of items for regular-regular size class and one column for all other size classes.
 All rows are supposed to have the same hight.
 */
@interface ZLItemsCollectionView : UIView

/**
 Unserlying scroll view, used for implementing behavior of class.
 Some libraries (like "DZNEmptyDataSet" library) require access to scroll view.
*/
@property (readonly, nonatomic) UIScrollView *underlyingScrollView;

@property (weak, nonatomic) IBOutlet id<ZLItemsCollectionViewDataSource> dataSource;
@property (weak, nonatomic) IBOutlet id<ZLItemsCollectionViewDelegate> delegate;

- (void)reloadData;

- (void)registerNib:(UINib *)nib forCellWithReuseIdentifier:(NSString *)identifier;
- (id)dequeueReusableCellWithReuseIdentifier:(NSString *)identifier forRow:(NSInteger)row;

/// `NSNotFound` if there is no selected item.
- (NSInteger)rowOfSelectedItem;

- (UICollectionViewCell *)cellForItemAtRow:(NSInteger)row;
- (NSArray *)visibleCells;

@end



@protocol ZLItemsCollectionViewDataSource <NSObject>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView;

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row;

@end


@protocol ZLItemsCollectionViewDelegate <NSObject>

@optional
- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row;

@end;
