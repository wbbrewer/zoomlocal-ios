//
//  ImageViewHeader.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ImageViewHeader.h"

@implementation ImageViewHeader

- (void)awakeFromNib
{
    // Initialization code
    CALayer *viewlayer = _imageView.layer;
    viewlayer.masksToBounds = NO;
    viewlayer.cornerRadius = 0.0;
    viewlayer.shadowOffset = CGSizeMake(-2.0, 2.0);
    viewlayer.shadowRadius = 2.0;
    viewlayer.shadowOpacity = 0.5;
}

- (void)setViewWithModel:(ProductModel*)model
{
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[model.images.large firstObject]]
                  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
}

@end
