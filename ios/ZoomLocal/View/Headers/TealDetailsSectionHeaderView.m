//
//  TealDetailsSectionHeaderView.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 16/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "TealDetailsSectionHeaderView.h"


@implementation TealDetailsSectionHeaderView

- (void)awakeFromNib
{
    [self.rightButton setTitle:@"" forState:UIControlStateNormal];
}

@end
