//
//  TealDetailsSectionHeaderView.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 16/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TealDetailsSectionHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@end
