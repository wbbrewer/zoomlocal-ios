//
//  ImageViewHeader.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ImageViewHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void)setViewWithModel:(ProductModel*)model;

@end
