//
//  ListingCollectionViewCell.h
//  Zoom Local
//
//  Created by Aric Brown on 11/15/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProductModel;


@interface ZLProductCollectionViewCell : UICollectionViewCell

@property(strong, nonatomic) ProductModel *productModel;

@end
