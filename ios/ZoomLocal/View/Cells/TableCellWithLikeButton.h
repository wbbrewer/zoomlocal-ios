//
//  TableCellWithLikeButton.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 01/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableCellWithLikeButton : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
