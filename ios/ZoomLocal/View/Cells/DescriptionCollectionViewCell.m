//
//  DescriptionCollectionViewCell.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/11/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "DescriptionCollectionViewCell.h"

@implementation DescriptionCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    UICollectionViewLayoutAttributes *attr = [layoutAttributes copy];
    CGSize size = [self.descriptionLabel sizeThatFits:CGSizeMake(CGRectGetWidth(layoutAttributes.frame),CGFLOAT_MAX)];
    CGRect newFrame = attr.frame;
    newFrame.size.height = size.height;
    attr.frame = newFrame;
    return attr;
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    self.contentView.frame = bounds;
}

@end
