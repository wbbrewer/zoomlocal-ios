//
//  ZLDealsCollectionViewCell.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 24/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZLCoupon.h"


@interface ZLDealsCollectionViewCell : UICollectionViewCell

@property(strong, nonatomic) ZLCoupon *coupon;

@end
