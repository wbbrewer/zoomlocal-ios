//
//  ImageViewHeaderWithFooter.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ImageViewHeaderWithFooter : UIView

+ (instancetype)instantiateFromNib;
- (void)setViewWithModel:(ProductModel*)model;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
