//
//  MapViewHeader.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/5/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VendorModel.h"
#import <MapKit/MapKit.h>

@interface MapViewHeader : UIView <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

+ (instancetype)instantiateFromNib;

- (void)setViewWithModel:(VendorModel*)model;

@end
