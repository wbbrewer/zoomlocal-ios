//
//  MapHeaderTableViewCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/5/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "VendorModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MapHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;

- (void)initWithModel:(VendorModel*)model;

@end
