//
//  ImageViewHeaderWithFooter.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ImageViewHeaderWithFooter.h"

@implementation ImageViewHeaderWithFooter

+ (instancetype)instantiateFromNib {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"%@", [self class]] owner:nil options:nil];
    
    return [views firstObject];
}

- (void)setViewWithModel:(ProductModel*)model {
    //
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[model.images.large firstObject]]
                  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    [_titleLabel setText:model.title];
    [_subtitleLabel setText:[NSString stringWithFormat:@"%@, %@, %@", model.term.vendor.address1, model.term.vendor.city, model.term.vendor.state]];
    [_priceLabel setText:[NSString stringWithFormat:@"$%@", model.meta.price]];
}

@end
