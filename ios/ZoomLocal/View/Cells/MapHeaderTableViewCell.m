//
//  MapHeaderTableViewCell.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/5/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "MapHeaderTableViewCell.h"

@implementation MapHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    _image.layer.cornerRadius = 5;
    _image.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)initWithModel:(VendorModel*)model {
    
    [self.mapView setShowsPointsOfInterest:NO];
    
    // Add an annotation.
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    point.coordinate = model.location.coordinate;
    
    point.title = model.name;
    
    point.subtitle = model.displayAddress;
    
    [_mapView addAnnotation:point];
    
    // Zoom the region.
    MKCoordinateRegion region;
    
    float spanX = 0.00725;
    float spanY = 0.00725;
    region.center.latitude = model.location.coordinate.latitude;
    region.center.longitude = model.location.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    
    [_mapView setRegion:region animated:YES];
    
    //
    [_image sd_setImageWithURL:[NSURL URLWithString:[model.images.medium firstObject]]
     
              placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
    
    [_name setText:model.name];
    [_address setText:model.displayAddress];
    
}

@end
