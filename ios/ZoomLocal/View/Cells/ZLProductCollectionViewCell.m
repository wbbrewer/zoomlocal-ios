//
//  ListingCollectionViewCell.m
//  Zoom Local
//
//  Created by Aric Brown on 11/15/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import "ZLProductCollectionViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "ListingItemModel.h"
#import "EventModel.h"
#import "LocationService.h"
#import "ZLConstants.h"


@interface ZLProductCollectionViewCell ()

@property (readonly, nonatomic) NSDateComponentsFormatter *numberOfDaysFormatter;

@property (weak, nonatomic) IBOutlet UIImageView *listingImage;
@property (weak, nonatomic) IBOutlet UILabel *listingTitle;
@property (weak, nonatomic) IBOutlet UILabel *listingRetailer;
@property (weak, nonatomic) IBOutlet UILabel *additionalDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *listingPrice;

@end

@implementation ZLProductCollectionViewCell

#pragma mark - Public -

- (void)setProductModel:(ProductModel *)productModel
{
    _productModel = productModel;
    
    [self.listingImage sd_setImageWithURL:[NSURL URLWithString:[productModel.images.thumbnail firstObject]]
                         placeholderImage:[UIImage imageNamed:@"launch_logo"]];
    
    [self.listingTitle setText:productModel.title];
    [self.listingRetailer setText:productModel.term.vendor.name];
    
    // When downloading items for "Retailer" details screen there is no possibility to distinguish types of items.
    BOOL isListingType = ([productModel isKindOfClass:[ProductModel class]] && productModel.meta.eventStartDate == nil);
    BOOL IsEventType = [productModel isKindOfClass:[ProductModel class]] && productModel.meta.eventStartDate != nil;
    
    if ([productModel isKindOfClass:[ListingItemModel class]] || isListingType) {

        // Events have event location. But since we are out of room, we are using for event date.
        self.additionalDetailsLabel.text = [NSString stringWithFormat:@"%@, %@", productModel.term.vendor.city, productModel.term.vendor.state];
    }
    else if ([productModel isKindOfClass:[EventModel class]] || IsEventType) {
        
        // Format the date.
        if ([productModel.meta.eventEndDate timeIntervalSinceDate:productModel.meta.eventStartDate] > 0) {
            self.additionalDetailsLabel.text = [NSString stringWithFormat:@"%@-%@",
                                                [[NSDateFormatter shortStyleDateFormatter] stringFromDate:productModel.meta.eventStartDate],
                                                [[NSDateFormatter shortStyleDateFormatter] stringFromDate:productModel.meta.eventEndDate]];
            
        }
        else {
            self.additionalDetailsLabel.text = [[NSDateFormatter shortStyleDateFormatter] stringFromDate:productModel.meta.eventStartDate];
        }
    }
    else {
        NSAssert(NO, @"Unknown type!");
    }
    
    CLLocationDistance distance = [[LocationService sharedInstance] distanceToLocation:productModel.location];
    const double kMetesInMile = 1609;
    double distanceInMiles = distance / kMetesInMile;
    [self.distanceLabel setText:[NSString stringWithFormat:@"%0.1f miles", distanceInMiles]];
    
    [self.listingPrice setText:[NSString stringWithFormat:@"$%@", productModel.meta.price]];
}

// Example: "2d"
- (NSDateComponentsFormatter *)numberOfDaysFormatter
{
    static dispatch_once_t initialized;
    static NSDateComponentsFormatter *componentFormatter = nil;
    
    dispatch_once(&initialized, ^{
        componentFormatter = [[NSDateComponentsFormatter alloc] init];
        componentFormatter.allowedUnits = NSCalendarUnitDay;
        componentFormatter.unitsStyle = NSDateComponentsFormatterUnitsStylePositional;
        componentFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorDropAll;
    });

    return componentFormatter;
}

@end
