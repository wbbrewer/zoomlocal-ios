//
//  VendorCollectionViewCell.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "VendorCollectionViewCell.h"

@implementation VendorCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    _view.layer.cornerRadius = 5;
    _view.layer.masksToBounds = YES;
}

@end
