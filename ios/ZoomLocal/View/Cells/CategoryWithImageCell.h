//
//  CategoryWithImageCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/3/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryWithImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
