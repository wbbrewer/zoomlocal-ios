//
//  CollectionReusableView.m
//  Zoom Local
//
//  Created by Aric Brown on 11/17/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import "CollectionReusableView.h"

@implementation CollectionReusableView

+ (id)collectionReusableViewForCollectionView:(UICollectionView*)collectionView
                                     fromNib:(UINib*)nib
                                forIndexPath:(NSIndexPath*)indexPath
                                    withKind:(NSString*)kind{
    
    NSString *cellIdentifier = [self cellIdentifier];
    [collectionView registerClass:[self class] forSupplementaryViewOfKind:kind withReuseIdentifier:cellIdentifier];
    [collectionView registerNib:nib forSupplementaryViewOfKind:kind withReuseIdentifier:cellIdentifier];
    CollectionReusableView *cell = [collectionView dequeueReusableSupplementaryViewOfKind: kind
                                                                      withReuseIdentifier: cellIdentifier
                                                                             forIndexPath: indexPath];
    return cell;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes{
    [super applyLayoutAttributes:layoutAttributes];
    
    //
}

+ (id)collectionReusableViewForCollectionView:(UICollectionView*)collectionView
                                forIndexPath:(NSIndexPath*)indexPath
                                    withKind:(NSString*)kind {
    
    return [[self class] collectionReusableViewForCollectionView:collectionView
                                                         fromNib:[self nib]
                                                    forIndexPath:indexPath
                                                        withKind:kind];
}

+ (NSString *)nibName {
    return [self cellIdentifier];
}

+ (NSString *)cellIdentifier {
    [NSException raise:NSInternalInconsistencyException format:@"WARNING: YOU MUST OVERRIDE THIS GETTER IN YOUR CUSTOM CELL .M FILE"];
    static NSString* _cellIdentifier = nil;
    _cellIdentifier = NSStringFromClass([self class]);
    return _cellIdentifier;
}

+(UINib*)nib{
    NSBundle * classBundle = [NSBundle bundleForClass:[self class]];
    UINib * nib = [UINib nibWithNibName:[self nibName]
                                 bundle:classBundle];
    return nib;
}

@end
