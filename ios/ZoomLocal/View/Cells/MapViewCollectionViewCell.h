//
//  MapViewCollectionViewCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/10/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
#import "VendorModel.h"

@interface MapViewCollectionViewCell : UICollectionViewCell <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (void)setViewWithModel:(VendorModel*)model;

@end
