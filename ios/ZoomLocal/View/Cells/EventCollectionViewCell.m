
//
//  EventCollectionViewCell.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/11/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "EventCollectionViewCell.h"

@implementation EventCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    _view.layer.masksToBounds = NO;
    _view.layer.cornerRadius = 0.0;
    _view.layer.shadowOffset = CGSizeMake(-1.0, 1.0);
    _view.layer.shadowRadius = 1.0;
    _view.layer.shadowOpacity = 0.33;
    
//    CALayer *viewlayer = _bottomView.layer;
//    viewlayer.masksToBounds = NO;
//    viewlayer.cornerRadius = 0.0;
//    viewlayer.shadowOffset = CGSizeMake(-2.0, 2.0);
//    viewlayer.shadowRadius = 2.0;
//    viewlayer.shadowOpacity = 0.5;
}

@end
