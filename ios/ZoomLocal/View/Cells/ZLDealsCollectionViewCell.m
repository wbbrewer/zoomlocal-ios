//
//  ZLDealsCollectionViewCell.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 24/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLDealsCollectionViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "ZLConstants.h"


@interface ZLDealsCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *couponImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *vendorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *validEndDateLabel;

@end

@implementation ZLDealsCollectionViewCell

#pragma mark - Public -

- (void)setCoupon:(ZLCoupon *)coupon
{
    _coupon = coupon;
    
    [_couponImageView sd_setImageWithURL:[NSURL URLWithString:[coupon.images.thumbnail firstObject]]
                        placeholderImage:[UIImage imageNamed:@"launch_logo"]];
    
    _titleLabel.text = coupon.title;
    _vendorNameLabel.text = coupon.term.vendor.name;
    _addressLabel.text = [NSString stringWithFormat:@"%@, %@", coupon.term.vendor.city, coupon.term.vendor.state];
    
    if (coupon.redeemingDate == nil) {
        NSString *endDateStr = [[NSDateFormatter shortStyleDateFormatter] stringFromDate:coupon.meta.couponEndDate];
        _validEndDateLabel.text = [NSString stringWithFormat:@"Valid Thru %@", endDateStr];
    }
    else {
        _validEndDateLabel.text = @"Redeemed";
    }
}

@end
