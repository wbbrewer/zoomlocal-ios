//
//  ListingDetailViewCell.m
//  Zoom Local
//
//  Created by Aric Brown on 9/22/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import "ListingDetailViewCell.h"

@implementation ListingDetailViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// Need to implement layoutSubiews and set the preferred max layout width of the multi-line label or
// the cell height does not get correctly calculated when the device changes orientation.

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.detailLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.detailLabel.frame);
}

@end
