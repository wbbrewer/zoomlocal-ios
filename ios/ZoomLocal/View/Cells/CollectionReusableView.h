//
//  CollectionReusableView.h
//  Zoom Local
//
//  Created by Aric Brown on 11/17/15.
//  Copyright © 2015 EquatorApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableView : UICollectionReusableView

+(NSString*)cellIdentifier;
+(id)collectionReusableViewForCollectionView:(UICollectionView*)collectionView
                                     fromNib:(UINib*)nib
                                forIndexPath:(NSIndexPath*)indexPath
                                    withKind:(NSString*)kind;

+(id)collectionReusableViewForCollectionView:(UICollectionView*)collectionView
                                forIndexPath:(NSIndexPath*)indexPath withKind:(NSString*)kind;

+(UINib*)nib;

@end
