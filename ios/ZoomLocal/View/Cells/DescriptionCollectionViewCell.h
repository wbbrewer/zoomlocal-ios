//
//  DescriptionCollectionViewCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/11/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes;
@end
