//
//  ListingDetailViewCell.h
//  Zoom Local
//
//  Created by Aric Brown on 9/22/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingDetailViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
