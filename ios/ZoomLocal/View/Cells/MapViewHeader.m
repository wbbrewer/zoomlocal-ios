//
//  MapViewHeader.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/5/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "MapViewHeader.h"

@implementation MapViewHeader

+ (instancetype)instantiateFromNib {
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithFormat:@"%@", [self class]] owner:nil options:nil];
    
    return [views firstObject];
}

- (void)setViewWithModel:(VendorModel*)model {
    //
    [_mapView setShowsPointsOfInterest:NO];
    
    // Add an annotation.
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    point.coordinate = model.location.coordinate;
    
    point.title = model.name;
    
    point.subtitle = model.displayAddress;
    
    [_mapView addAnnotation:point];
    
    // Zoom the region.
    MKCoordinateRegion region;
    
    float spanX = 0.00725;
    float spanY = 0.00725;
    region.center.latitude = model.location.coordinate.latitude;
    region.center.longitude = model.location.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    
    [_mapView setRegion:region animated:YES];
}

@end
