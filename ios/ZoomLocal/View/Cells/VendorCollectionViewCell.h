//
//  VendorCollectionViewCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VendorCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIImageView *vendorImageView;

@property (weak, nonatomic) IBOutlet UILabel *vendorNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *vendorAddressLabel;

@property (weak, nonatomic) IBOutlet UILabel *vendorLocationLabel;

@property (weak, nonatomic) IBOutlet UILabel *vendorDistanceLabel;

@end
