//
//  TabBarItem.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/21/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "TabBarItem.h"

@implementation TabBarItem

- (void)awakeFromNib {
    [self setImage:self.image]; // calls setter below to adjust image from storyboard / nib file
}

- (void)setImage:(UIImage *)image {
    //[super setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    //self.selectedImage = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
