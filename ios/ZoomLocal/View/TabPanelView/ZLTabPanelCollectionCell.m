//
//  ZLTabPanelCollectionCell.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLTabPanelCollectionCell.h"


@implementation ZLTabPanelCollectionCell {
    UIColor *_textLabelColor;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        [self addObserver:self forKeyPath:@"textLabel.tintColor" options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:@"textLabel.textColor" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"textLabel.tintColor"];
    [self removeObserver:self forKeyPath:@"textLabel.textColor"];
}

- (void)setSelected:(BOOL)selected
{
    [self removeObserver:self forKeyPath:@"textLabel.textColor"];
    
    [super setSelected:selected];
    
    if (selected) {
        if (_textLabelColor == nil) {
            // Remember color for unselected state.
            _textLabelColor = self.textLabel.textColor;
        }
        self.textLabel.textColor = self.tintColor;
    }
    else {
        NSParameterAssert(_textLabelColor);
        self.textLabel.textColor = _textLabelColor;
    }
    
    [self addObserver:self forKeyPath:@"textLabel.textColor" options:NSKeyValueObservingOptionNew context:nil];
}

#pragma mark NSKeyValueObserving

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"textLabel.tintColor"]) {
        if (self.selected) {
            [self removeObserver:self forKeyPath:@"textLabel.textColor"];
            self.textLabel.textColor = change[NSKeyValueChangeNewKey];
            [self addObserver:self forKeyPath:@"textLabel.textColor" options:NSKeyValueObservingOptionNew context:nil];
        }
    }
    else if ([keyPath isEqualToString:@"textLabel.textColor"]) {
        _textLabelColor = self.textLabel.textColor;
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

@end
