//
//  ZLTabPanelCollectionCell.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "KMKUniversalCollectionViewCellWithBorder.h"


// Class is added to sync animation of changing textColor when selecting cell with animation of adding stripe.
// To set color of `textLabel` in selected state use `tintColor` property of textLabel itself.
@interface ZLTabPanelCollectionCell : KMKUniversalCollectionViewCellWithBorder


@end
