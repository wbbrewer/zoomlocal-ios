//
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 05/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLTabPanelView.h"

#import "UIView+KMKSeparator.h"

#import "KMKStripeViewSelector.h"
#import "ZLTabPanelCollectionCell.h"


static NSString *const kCollectionViewCellID = @"kCollectionViewDateID";
static const CGFloat kSeparatorHeight = 0.0;


@interface ZLTabPanelView () <
                                KMKOneRowCollectionViewDatasource,
                                KMKOneRowCollectionViewDelegate
                                >

@property (strong, nonatomic) KMKOneRowCollectionView *oneRowCollectionView;

@end


@implementation ZLTabPanelView

@synthesize delegate = _delegate;

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        // Place for top separator is not allocated as for some reason collection view add transperent one-point height stripe at the top of itself,
        // right at the place where cell is located accoring debugging
        CGRect frame = CGRectMake(0.0, 0.0, self.bounds.size.width, self.bounds.size.height - kSeparatorHeight);
        _oneRowCollectionView = [[KMKOneRowCollectionView alloc] initWithFrame:frame];
        _oneRowCollectionView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        _oneRowCollectionView.dataSource = self;
        _oneRowCollectionView.delegate = self;
        [self addSubview:_oneRowCollectionView];
        [self addSeparatorToEdge:UIRectEdgeTop height:kSeparatorHeight color:[UIColor redColor]];
        [self addSeparatorToEdge:UIRectEdgeBottom height:kSeparatorHeight color:[UIColor redColor]];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [_oneRowCollectionView registerClass: [ZLTabPanelCollectionCell class]
              forCellWithReuseIdentifier: kCollectionViewCellID];
}

#pragma mark -
#pragma mark Public

- (void)setTabTitles:(NSArray *)tabTitles
{
    _tabTitles = tabTitles;
    
    CGFloat cellWidth = [UIScreen mainScreen].bounds.size.width;
    if (_tabTitles.count > 1) {
        cellWidth = ceilf([UIScreen mainScreen].bounds.size.width / _tabTitles.count);
    }
    [_oneRowCollectionView setCellWidth:cellWidth];
    
    [_oneRowCollectionView reloadData];
    [_oneRowCollectionView selectItemAtColumn:0 animated:YES scrollPosition:UICollectionViewScrollPositionNone];
}

- (void)setSelectedTitle:(NSString *)selectedTitle
{
    _selectedTitle = selectedTitle;
    NSInteger index = [self.tabTitles indexOfObject:selectedTitle];
    if (index != NSNotFound) {
        [_oneRowCollectionView selectItemAtColumn:index animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
    else {
        NSAssert(NO, @"App error!");
    }
}

#pragma mark <KMKOneRowCollectionViewDatasource>

- (NSInteger)numberOfColumnsInOneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView
{
    return _tabTitles.count;
}

- (UICollectionViewCell *)oneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView
                           cellForItemInColumn:(NSInteger)column
{
    UICollectionViewCell *cell = nil;
    
    static KMKStripeViewSelector *cellSelector = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cellSelector = [[KMKStripeViewSelector alloc] init];
        cellSelector.tintColor = self.tintColor;
    });
    
    ZLTabPanelCollectionCell *aCell = [oneRowCollectionView dequeueReusableCellWithReuseIdentifier: kCollectionViewCellID
                                                                                         forColumn: column];
    aCell.viewSelector = cellSelector;
    aCell.rectEdgeToDrawBorder = UIRectEdgeNone;
    aCell.textLabel.font = [UIFont systemFontOfSize:17.0];
    aCell.textLabel.textAlignment = NSTextAlignmentCenter;
    aCell.textLabel.text = _tabTitles[column];
    aCell.textLabel.textColor = _tabTitlesColor;
    aCell.textLabel.tintColor = self.tintColor;
    
    cell = aCell;
    return cell;
}

#pragma mark <KMKOneRowCollectionViewDelegate>

- (void)oneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView didSelectItemAtColumn:(NSInteger)column
{
    [self.delegate tabPanelView:self didSelectTitle:_tabTitles[column]];
}

@end
