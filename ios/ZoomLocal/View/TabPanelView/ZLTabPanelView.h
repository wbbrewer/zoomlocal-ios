//
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 05/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "KMKOneRowCollectionView.h"

@protocol ZLTabPanelViewDelegate;


// Preferred height is 47 pt.
@interface ZLTabPanelView : UIView

@property (strong, nonatomic) NSArray *tabTitles;
/// Color for non-selected state. For selected state use `tintColor` property.
@property (strong, nonatomic) UIColor *tabTitlesColor;

// Delegate is not invoked.
@property (assign, nonatomic) NSString *selectedTitle;


@property (weak, nonatomic) IBOutlet id<ZLTabPanelViewDelegate> delegate;

@end


@protocol ZLTabPanelViewDelegate <NSObject>

- (void)tabPanelView:(ZLTabPanelView *)tabPanelView didSelectTitle:(NSString *)title;

@end
