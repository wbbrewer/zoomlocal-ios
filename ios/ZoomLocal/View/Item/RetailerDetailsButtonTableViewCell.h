//
//  ItemVendorReferenceCell.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 15/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RetailerDetailsButtonTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *vendorButton;

@end
