//
//  ItemVendorReferenceCell.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 15/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "RetailerDetailsButtonTableViewCell.h"


@implementation RetailerDetailsButtonTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
