//
//  ItemDetailsDescriptionTableViewCell.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ItemDetailsDescriptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descLabel;

@end
