//
//  ProductDetailsStickyView.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/15/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MetaModel.h"
#import "ZLTypes.h"


@class ProductDetailsStickyView;


@protocol ProductDetailsStickyViewDelegate <NSObject>

- (void)productDetailsStickyView:(ProductDetailsStickyView *)view
       didPressShareButtonOfType:(ZLShareButtonType)shareButtonType;

@end


@interface ProductDetailsStickyView : UIView

- (void)setViewWithModel:(MetaModel*)model;
@property (weak, nonatomic) id<ProductDetailsStickyViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
