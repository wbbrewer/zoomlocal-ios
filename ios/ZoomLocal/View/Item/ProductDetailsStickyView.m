//
//  ProductDetailsStickyView.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/15/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ProductDetailsStickyView.h"


@implementation ProductDetailsStickyView

- (void)setViewWithModel:(MetaModel*)model
{
    [_priceLabel setText:[NSString stringWithFormat:@"$%@", model.price]];
}

#pragma mark - Actions

- (IBAction)postToFacebookButtonPressed:(UIButton *)sender
{
    NSParameterAssert(_delegate);
    [_delegate productDetailsStickyView:self didPressShareButtonOfType:ZLShareButtonTypePostToFacebook];
}

- (IBAction)postToTweetButtonPressed:(UIButton *)sender
{
    NSParameterAssert(_delegate);
    [_delegate productDetailsStickyView:self didPressShareButtonOfType:ZLShareButtonTypePostToTwitter];
}

@end
