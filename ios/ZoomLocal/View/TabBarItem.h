//
//  TabBarItem.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/21/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarItem : UITabBarItem

@end
