//
//  ZLRetailerContactsTableCell.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRetailerContactsTableCell.h"


@interface ZLRetailerContactsTableCell ()

@end


@implementation ZLRetailerContactsTableCell

#pragma mark - Actions

- (IBAction)phoneButtonPressed:(UIButton *)sender
{
    NSString *phoneNumber = sender.titleLabel.text;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
}

- (IBAction)postToFacebookButtonPressed:(UIButton *)sender
{
    NSParameterAssert(_delegate);
    [_delegate retailerContactsTableCell:self didPressShareButtonOfType:ZLShareButtonTypePostToFacebook];
}

- (IBAction)postToTweetButtonPressed:(UIButton *)sender
{
    NSParameterAssert(_delegate);
    [_delegate retailerContactsTableCell:self didPressShareButtonOfType:ZLShareButtonTypePostToTwitter];
}

@end
