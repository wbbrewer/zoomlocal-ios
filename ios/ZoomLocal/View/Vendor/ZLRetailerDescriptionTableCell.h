//
//  ZLRetailerDescriptionTableCell.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZLRetailerDescriptionTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UILabel *followButtonLabel;
@property (weak, nonatomic) IBOutlet UIButton *youtubeButton;
@property (weak, nonatomic) IBOutlet UIButton *youtubeAppLinkButton;

/// Default `NO`.
@property (assign, nonatomic) BOOL showFollowButton;

/// Default `NO`.
@property (assign, nonatomic) BOOL showYoutubeButton;

@end
