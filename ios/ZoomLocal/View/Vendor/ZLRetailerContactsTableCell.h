//
//  ZLRetailerContactsTableCell.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZLTypes.h"

@class ZLRetailerContactsTableCell;


@protocol ZLRetailerContactsTableCellDelegate <NSObject>

- (void)retailerContactsTableCell:(ZLRetailerContactsTableCell *)view
        didPressShareButtonOfType:(ZLShareButtonType)shareButtonType;

@end


@interface ZLRetailerContactsTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *vendorImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;

@property (weak, nonatomic) id<ZLRetailerContactsTableCellDelegate> delegate;

@end
