//
//  ZLRetailerDescriptionTableCell.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRetailerDescriptionTableCell.h"

#import "UIButton+Indicator.h"


@interface ZLRetailerDescriptionTableCell ()

@property (weak, nonatomic) IBOutlet UIView *customBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youtubeViewHeightConstraint;

@end

@implementation ZLRetailerDescriptionTableCell

- (void)setShowFollowButton:(BOOL)showFollowButton
{
    _showFollowButton = showFollowButton;
    _followViewHeightConstraint.priority = showFollowButton ? 997 : 999;
}

- (void)setShowYoutubeButton:(BOOL)showYoutubeButton
{
    _showYoutubeButton = showYoutubeButton;
    _youtubeViewHeightConstraint.priority = showYoutubeButton ? 997 : 999;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.followButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
    // As UI is not blocked it is possible to reuse cell with started indicator.
    [self.followButton hideIndicator];
    [self.youtubeButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    _customBackgroundView.layer.cornerRadius = 2.0;
    _customBackgroundView.layer.shadowOffset = CGSizeMake(0, 1);
    _customBackgroundView.layer.shadowRadius = 2.0;
    _customBackgroundView.layer.shadowOpacity = 0.3;
    
    self.showFollowButton = NO;
    self.showYoutubeButton = NO;
}

@end
