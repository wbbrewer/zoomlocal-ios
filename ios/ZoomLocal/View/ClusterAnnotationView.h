//
//  ClusterAnnotationView.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/07/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ClusterAnnotationView : MKAnnotationView

@property (assign, nonatomic) NSUInteger count;

@end
