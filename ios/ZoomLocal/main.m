//
//  main.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/19/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZLAppDelegate class]));
    }
}
