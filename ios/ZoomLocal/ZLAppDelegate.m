//
//  AppDelegate.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/19/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLAppDelegate.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ACTReporter.h"
#import "TAGContainer.h"
#import "TAGContainerOpener.h"
#import "TAGManager.h"
#import <Parse/Parse.h>
#import <MobileDeepLinking.h>
#import <UIAlertController+Blocks.h>
#import <MZFormSheetPresentationViewController.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "KMKLumberjackFrameworkConfigurator.h"
#import "ZLConstants.h"
#import "ZLListingsViewController.h"
#import "CategoriesViewController.h"
#import "ZLEventsViewController.h"
#import "ZLMyAreaViewController.h"

#ifdef DEBUG
    #undef LOG_LEVEL_DEF
    #define LOG_LEVEL_DEF OverridenLogLevel
    static const int OverridenLogLevel = LOG_LEVEL_DEBUG; // log level for current m-file.
#endif

enum {
    ZLTabBarCotrollerIndexListings = 1,
    ZLTabBarCotrollerIndexCategories,
    ZLTabBarCotrollerIndexEvents,
    ZLTabBarControllerIndexMyArea,
    ZLTabBarCotrollerIndexSearch
};


@interface ZLAppDelegate ()<TAGContainerOpenerNotifier>

// Used to enable GCM service.
@property (nonatomic, readonly) NSString *gcmSenderID;

@end


@implementation ZLAppDelegate

#pragma mark <UIApplicationDelegate>

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class]]]; // Crash reporting subsystem

    [self setUpCocoaLumberjackFramework]; // Login framework should be set up ASAP
    [self setUpFrameworksWithApplication:application options:launchOptions];

    [self registerForPushNotifications];
    
    [self setUpAppearanceOfSomeControls];
    
    //[self goToLoginScreen];

    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    BOOL successToHandleURL = NO;
    
    // Details are here https://developers.facebook.com/docs/ios/getting-started
    successToHandleURL = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                        openURL:url
                                                              sourceApplication:sourceApplication
                                                                     annotation:annotation];
    
    if (!successToHandleURL && [[url scheme] isEqualToString:@"zoomlocal"]) {
        // http://mobiledeeplinking.org/
        [[MobileDeepLinking sharedInstance] routeUsingUrl:url];
        successToHandleURL = YES;
    }
    
    NSAssert(successToHandleURL, @"Attempt to open unknown URL: '%@'", url);

    return successToHandleURL;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

#pragma mark APN remote notifications

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[@"global"];
    [currentInstallation saveInBackground];
    
    DDLogDebug(@"Did register for remote notification with device token: %@", deviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    DDLogError(@"Failed to get APN token with error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DDLogVerbose(@"Received remote notification: %@", userInfo);
    
//    if (userInfo[@"deep-link"] != nil) {
//        NSString *title = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey];
//        
//        NSString *message = nil;
//        id alert = userInfo[@"aps"][@"alert"];
//        if ([alert isKindOfClass:[NSString class]]) {
//            message = alert;
//        }
//        else {
//            title = alert[@"title"] ?: title;
//            message = alert[@"body"];
//        }
//        
//        [UIAlertController showAlertInViewController:self.window.rootViewController
//                                           withTitle:title
//                                             message:message
//                                   cancelButtonTitle:@"Cancel"
//                              destructiveButtonTitle:nil
//                                   otherButtonTitles:@[@"Follow"]
//                                            tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
//                                                BOOL followLink = (buttonIndex != controller.cancelButtonIndex);
//                                                if (followLink) {
//                                                    NSString *deepLink = userInfo[@"deep-link"];
//                                                    NSParameterAssert(deepLink);
//                                                    if (deepLink != nil) {
//                                                        NSURL *url = [NSURL URLWithString:deepLink];
//                                                        NSParameterAssert(url);
//                                                        //        [[MobileDeepLinking sharedInstance] routeUsingUrl:url];
//                                                        [application openURL:url];
//                                                    }
//                                                }
//                                            }];
//    }
//    else {
        [PFPush handlePush:userInfo];
//    }
}

#pragma mark <TAGContainerOpenerNotifier>

// TAGContainerOpenerNotifier callback.
- (void)containerAvailable:(TAGContainer *)container
{
    // Note that containerAvailable may be called on any thread, so you may need to dispatch back to
    // your main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        self.GTMContainer = container;
    });
}


#pragma mark - Helpers -

- (void)setUpFrameworksWithApplication:(UIApplication *)application options:(NSDictionary *)launchOptions
{
    [self setUpGoogleTagManagerSDK];
    [self setUpGoogleAdWordsConversionTrackingSDK];
    
    [self setUpFaceBookSDKWithApplication:application options:launchOptions];
    
    [Parse setApplicationId:@"4LPNb9FhX8rtXB7Y6jDb8LCq2Qxp81KTWGTnkwxJ"
                  clientKey:@"flMAIF3E5mewqjqRzte7uZkd0t0nMzR0D5EAJTdn"];
    
    [self registerHandlersOfMobileDeepLinking];
}

/**
 Description of "zoomlocal" scheme:
 
 1. zoomlocal://listings
     Open "Listings" screen.
 
 2. zoomlocal://listings/:listingId
     Example: "zoomlocal://listings/123"
     Open listing with given id "listingId".
 
 3. zoomlocal://events
     Open "Events" screen.
 
 4. zoomlocal://events/:eventId
     Example: "zoomlocal://events/123"
     Open event with given id "eventId".
 
 5. zoomlocal://myarea
     Open "My Area" screen.
 
 6. zoomlocal://myarea/:vendorId
     Example: "zoomlocal://myarea/123"
     Open vendor with given id "vendorId".
 */
- (void)registerHandlersOfMobileDeepLinking
{
#if DEBUG
    NSError *error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MobileDeepLinkingConfig" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    NSAssert(error == nil, @"Error in 'MobileDeepLinkingConfig.json' file. You can use http://www.jsoneditoronline.org/ service for finding error!");
#endif
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToListings" handler:^(NSDictionary *properties)
     {
         [tabBarController setSelectedIndex:ZLTabBarCotrollerIndexListings];
         UINavigationController *nc = (UINavigationController *)tabBarController.selectedViewController;
         [nc popToRootViewControllerAnimated:YES];
     }];
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToSpecificListingItem" handler:^(NSDictionary *properties)
     {
         [tabBarController setSelectedIndex:ZLTabBarCotrollerIndexListings];
         UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
         [navigationController popToRootViewControllerAnimated:YES];
         
         ZLListingsViewController *listingisViewController = (ZLListingsViewController *)navigationController.topViewController;
         NSString *listingItemId = properties[@"listingItemId"];
         [listingisViewController goToItemDetailsScreenWithItemId:listingItemId];
     }];
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToEvents" handler:^(NSDictionary *properties)
     {
         [tabBarController setSelectedIndex:ZLTabBarCotrollerIndexEvents];
         UINavigationController *nc = (UINavigationController *)tabBarController.selectedViewController;
         [nc popToRootViewControllerAnimated:YES];
     }];
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToSpecificEvent" handler:^(NSDictionary *properties)
     {
         [tabBarController setSelectedIndex:ZLTabBarCotrollerIndexEvents];
         UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
         [navigationController popToRootViewControllerAnimated:YES];
         
         ZLEventsViewController *eventsViewController = (ZLEventsViewController *)navigationController.topViewController;
         NSString *eventId = properties[@"eventId"];
         [eventsViewController goToItemDetailsScreenWithItemId:eventId];
     }];
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToMyArea" handler:^(NSDictionary *properties)
    {
        [tabBarController setSelectedIndex:ZLTabBarControllerIndexMyArea];
        UINavigationController *nc = (UINavigationController *)tabBarController.selectedViewController;
        [nc popToRootViewControllerAnimated:YES];
    }];
    
    [[MobileDeepLinking sharedInstance] registerHandlerWithName:@"goToSpecificVendor" handler:^(NSDictionary *properties)
     {
         [tabBarController setSelectedIndex:ZLTabBarControllerIndexMyArea];
         UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
         [navigationController popToRootViewControllerAnimated:YES];
         
         ZLMyAreaViewController *myAreaViewController = (ZLMyAreaViewController *)navigationController.topViewController;
         NSString *itemId = properties[@"vendorId"];
         [myAreaViewController goToVendorDetailsScreenWithId:itemId];
     }];
}

// https://developers.facebook.com/docs/ios/getting-started
- (void)setUpFaceBookSDKWithApplication:(UIApplication *)application options:(NSDictionary *)launchOptions
{
    //     Connecting FBSDKApplicationDelegate to AppDelegate.
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
}

// https://developers.google.com/tag-manager/ios/v3/#add-sdk
- (void)setUpGoogleTagManagerSDK
{
    self.tagManager = [TAGManager instance];
    
#if DEBUG
    [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelDebug];
#else 
    [self.tagManager.logger setLogLevel:kTAGLoggerLogLevelInfo];
#endif
    
    NSString *const kContainerId = @"GTM-MTBTS3";
    NSAssert([[NSBundle mainBundle] pathForResource:kContainerId ofType:nil], @"No default container found!");
    /*
     * Opens a container.
     *
     * @param containerId The ID of the container to load.
     * @param tagManager The TAGManager instance for getting the container.
     * @param openType The choice of how to open the container.
     * @param timeout The timeout period (default is 2.0 seconds).
     * @param notifier The notifier to inform on container load events.
     */
    [TAGContainerOpener openContainerWithId:kContainerId
                                 tagManager:self.tagManager
                                   openType:kTAGOpenTypePreferFresh
                                    timeout:nil
                                   notifier:self];
    
    // Wait until container has been opened. Usually takes <1.0 sec.
    // Do not use semaphore (dispatch_semaphore_t) as container can be loaded in the main thread therefore main thread must not be locked.
    // Events pushed to the data layer will fire tags in that container only if container has been opened.
    CFRunLoopRunResult status = 0;
    while (self.GTMContainer == nil && status != kCFRunLoopRunTimedOut) {
        const NSTimeInterval timeout = 10.0;
        status = CFRunLoopRunInMode(kCFRunLoopDefaultMode, timeout, YES);
    }
}

// https://developers.google.com/app-conversion-tracking/ios/
- (void)setUpGoogleAdWordsConversionTrackingSDK
{
    // Setting up "AdWords Conversion Tracking SDK"
    NSString *const kConversionID = @"947950921";
    [ACTConversionReporter reportWithConversionID:kConversionID
                                            label:@"sQ73CJH102IQyaqCxAM"
                                            value:@"2.00"
                                     currencyCode:@"USD"
                                     isRepeatable:NO];
#if !DEBUG
    // Enable automated usage reporting.
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:kConversionID];
#else
    // Disable automated usage reporting for a single conversion ID.
    [ACTAutomatedUsageTracker disableAutomatedUsageReportingWithConversionID:kConversionID];
#endif

}

- (void)setUpCocoaLumberjackFramework
{
    [[KMKLumberjackFrameworkConfigurator sharedConfigurator] setUp];
    [KMKLumberjackFrameworkConfigurator sharedConfigurator].logIntoFile = YES;
}

// Prompts iOS to ask the user if they would like to receive push notifications
- (void)registerForPushNotifications
{
    UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)setUpAppearanceOfSomeControls
{
    UIColor *magenta = [UIColor magentaAppColor];
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                           [UIFont systemFontOfSize:21], NSFontAttributeName,
                                                           nil]];
    
    // UITabBar customizations.
    [[UITabBar appearance] setTintColor:magenta]; // "Global Tint Color" doesn't work
    
    // Add this code to change StateNormal text Color,
    //    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : teal}
    //                                           forState:UIControlStateNormal];
    
    
    // then if StateSelected should be different, you should add this code
    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : magenta}
                                           forState:UIControlStateSelected];
}

- (void)goToLoginScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"LoginNavigationController"];
    MZFormSheetPresentationViewController *formSheetController = [[MZFormSheetPresentationViewController alloc] initWithContentViewController:navigationController];
    //formSheetController.presentationController.contentViewSize = CGSizeMake(vc.view.bounds.size.width * .8, vc.view.bounds.size.height * .8);
    [self.window makeKeyAndVisible]; // otherwise `formSheetController` won't be presented when launching app
    [self.window.rootViewController presentViewController:formSheetController animated:YES completion:nil];
}

@end
