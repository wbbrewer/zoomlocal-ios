//
//  UIViewController+ZoomLocal.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "UIViewController+ZoomLocal.h"

@import Twitter;
#import "UIAlertController+Blocks.h"


@implementation UIViewController (ZoomLocal)

- (void)presentComposeViewControllerForTwitterWithInitialText:(NSString *)text
                                                        image:(UIImage *)image
                                                         link:(NSURL *)url
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:text];
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
        if (image != nil) {
            [tweetSheet addImage:image];
        }
        
        if (url != nil) {
            [tweetSheet addURL:url];
        }
    }
    else {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@"Sorry"
                                             message:@"You can't send a tweet right now, make sure "
         @"your device has an internet connection and you have"
         @"at least one Twitter account setup"
                                   cancelButtonTitle:@"OK"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil
                                            tapBlock:^(UIAlertController * _Nonnull controller,
                                                       UIAlertAction * _Nonnull action,
                                                       NSInteger buttonIndex) {
                                                ;
                                            }];
    }
}

@end
