//
//  UIViewController+ZoomLocal.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIViewController (ZoomLocal)

/**
 @param image Can be `nil`.
 @param url Can be `nil`.
 If servise Twitter is not available (there is no set up account in settings) then alert is shown with warning.
 */
- (void)presentComposeViewControllerForTwitterWithInitialText:(NSString *)text
                                                        image:(UIImage *)image
                                                         link:(NSURL *)url;


@end
