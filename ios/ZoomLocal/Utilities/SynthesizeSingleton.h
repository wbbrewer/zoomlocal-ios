//
// SynthesizeSingleton.h
// Copyright 2009 Matt Gallagher. All rights reserved.
//
// Modified by Oliver Jones and Byron Salau
//
// Permission is given to use this source code file without charge in any
// project, commercial or otherwise, entirely at your risk, with the condition
// that any redistribution (in part or whole) of source code must retain
// this copyright and permission notice. Attribution in compiled projects is
// appreciated but not required.

#if __has_feature(objc_arc)

    #define SYNTHESIZE_SINGLETON_FOR_CLASS(classname, accessorname) \
    static id shared##classname = nil;\
    \
    +(classname *)accessorname\
    {\
        static dispatch_once_t pred;\
        dispatch_once(&pred, ^{\
            shared##classname = [[super alloc] initUniqueInstance];\
        });\
        return shared##classname;\
    }\
    \
    -(classname *)initUniqueInstance\
    {\
        return [super init];\
    }\
    \
    /*For use by test frameworks only!*/\
    + (void)destroyAndRecreateSingleton\
    {\
        shared##classname = [[super alloc] initUniqueInstance];\
    }

#else

    #define SYNTHESIZE_SINGLETON_FOR_CLASS(classname, accessorname) \
    \
    static classname *shared##classname = nil; \
    \
    + (void)cleanupFromTerminate \
    { \
        classname *temp = shared##classname; \
        shared##classname = nil; \
        [temp dealloc]; \
    } \
    + (void)registerForCleanup \
    { \
        [[NSNotificationCenter defaultCenter] addObserver:self \
        selector:@selector(cleanupFromTerminate) \
        name:UIApplicationWillTerminateNotification \
        object:nil]; \
    } \
    \
    + (classname *)accessorname \
    { \
        static dispatch_once_t p; \
        dispatch_once(&p, ^{ \
            if (shared##classname == nil) { \
                NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; \
                shared##classname = [[super alloc] initUniqueInstance]; \
                [self registerForCleanup]; \
                [pool drain]; \
            } \
        }); \
        return shared##classname; \
    } \
    \
    + (id)allocWithZone:(NSZone *)zone \
    { \
    static dispatch_once_t p; \
    __block classname* temp = nil; \
    dispatch_once(&p, \
    ^{ \
    if (shared##classname == nil) \
    { \
    temp = shared##classname = [super allocWithZone:zone]; \
    } \
    }); \
    return temp; \
    } \
    \
    \
    -(classname *)initUniqueInstance {\
        return [super init];\
    }\
    \
    - (id)copyWithZone:(NSZone *)zone { return self; } \
    - (id)retain { return self; } \
    - (NSUInteger)retainCount { return NSUIntegerMax; } \
    - (oneway void)release { } \
    - (id)autorelease { return self;\
    }
    
#endif

/**

/// *** This part for .h file ***

#pragma mark - Singleton -

#error Rename to sharedClassType
/// @return Returns the singleton instance.
+ (instancetype)sharedClassType;

/// Only for use by test frameworks!
+ (void)destroyAndRecreateSingleton;

// Clue for improper use (produces compile time error)
 + (instancetype) alloc  NS_UNAVAILABLE;
 - (instancetype) init   NS_UNAVAILABLE;
 + (instancetype) new    NS_UNAVAILABLE;

#pragma mark -


/// *** This part for .m file ***

#pragma mark - Singleton -

static id sSharedInstance = nil;

#error Rename to sharedClassType
+ (instancetype)sharedClassType
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sSharedInstance = [[super alloc] initUniqueInstance];
    });
    return sSharedInstance;
}

- (instancetype)initUniqueInstance
{
    self = [super init];
    if (self != nil) {
    }
    return self;
}

+ (void)destroyAndRecreateSingleton
{
    sSharedInstance = [[super alloc] initUniqueInstance];
}
 
#pragma mark -
 
 */
