//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "KMKLogFileManager.h"


@implementation KMKLogFileManager

- (void)didArchiveLogFile:(NSString *)logFilePath
{
    if ([_delegate respondsToSelector:@selector(kmk_didArchiveLogFile:)]) 
    {
        NSAssert([NSThread isMainThread] == NO, @"Supposed that method is invokod in other than main thread!");
        dispatch_async(dispatch_get_main_queue(), ^(){
            if ([_delegate respondsToSelector:@selector(kmk_didArchiveLogFile:)]) {
                [_delegate kmk_didArchiveLogFile:logFilePath];
            }
        });
    }
}

- (void)didRollAndArchiveLogFile:(NSString *)logFilePath
{
    NSAssert([NSThread isMainThread] == NO, @"Supposed that method is invokod in other than main thread!");
    dispatch_async(dispatch_get_main_queue(), ^(){
        if ([_delegate respondsToSelector:@selector(kmk_didRollAndArchiveLogFile:)]) {
            [_delegate kmk_didRollAndArchiveLogFile:logFilePath];
        }
    });
}

@end
