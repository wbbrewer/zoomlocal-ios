//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "DDFileLogger.h"


@protocol KMKLogFileManagerDelegate <NSObject>

@optional

/**
 This method correspondes to the `didArchiveLogFile:` method. It is not clear when this corresponding method must be invoked.
 But it is established that it is invoked in case when 
    - you start your app
    - try to log some message
    - log file is old enough according to "rollingFrequency" to be archived.
 
 `kmk_didRollAndArchiveLogFile` method is not invoked in this case.
 */
- (void)kmk_didArchiveLogFile:(NSString *)logFilePath;

// Log file was archived due to reaching max size or due to "rollingFrequency" setting.
- (void)kmk_didRollAndArchiveLogFile:(NSString *)logFilePath;

@end

/**
 Serves the next targets:
    - implements optional methods of `DDLogFileManager` protocol, that are not implemented in `DDLogFileManagerDefault`
 */
@interface KMKLogFileManager : DDLogFileManagerDefault

@property (assign, nonatomic) id<KMKLogFileManagerDelegate> delegate;

- (void)didArchiveLogFile:(NSString *)logFilePath; // override
- (void)didRollAndArchiveLogFile:(NSString *)logFilePath; // override

@end
