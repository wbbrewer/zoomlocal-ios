//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import <Foundation/Foundation.h>


/**
 Class is intended to increase modularity. In that way it lightens reusing code.
 It was not intended to have capabilities to be configured from outside. 
 
 Log level is set in "KMKLumberjackLogging.h"
 
 Class adds the following loggers:
    - in 'DEBUG' mode logging to XCode's console
    - logging to file located in the "Documents" directory.
 
 Before using `setUp` method must be invoked to initilize underlying framwork.
 
 Example:
     [[KMKLumberjackFrameworkConfigurator sharedConfigurator] setUp];
     [KMKLumberjackFrameworkConfigurator sharedConfigurator].logIntoFile = YES;
 
 When file is archived (in case logIntoFile == YES) app version is added into the very beginning of the new log file.
 */
@interface KMKLumberjackFrameworkConfigurator : NSObject

// TODO: replace this property with property for 'log directory'
/// If YES then logging into file is performs.
/// Log files are placed into 'Documents' directory.
/// Default is NO.
@property (nonatomic) BOOL logIntoFile;

#pragma mark Singleton

+ (instancetype)sharedConfigurator;

// Clue for improper use (produces compile time error)
+ (instancetype) alloc __attribute__((unavailable("alloc not available!")));
- (instancetype) init __attribute__((unavailable("init not available!")));
+ (instancetype) new __attribute__((unavailable("new not available!")));

/// Invoke this method before using configurator.
- (void)setUp;

@end
