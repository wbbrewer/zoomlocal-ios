//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "KMKLogFormatter.h"

NSString *logFlagToString(int logFlag);


@interface KMKLogFormatter ()

@property (readonly, nonatomic) NSDateFormatter *dateFormatter;

@end


@implementation KMKLogFormatter {
    NSDateFormatter *_dateFormatter;
}

#pragma mark Properties

- (NSDateFormatter *)dateFormatter
{
    if (_dateFormatter == nil) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"YYYY-MM-dd HH.mm.ss:SSS"];
        // To prevent formatter from rewriting the format string when user override the default AM/PM versus 24-hour time setting
        // xcdoc://?url=developer.apple.com/library/ios/documentation/Cocoa/Conceptual/DataFormatting/Articles/dfDateFormatting10_4.html#
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [_dateFormatter setLocale:locale];
    }
    
    return _dateFormatter;
}

#pragma mark - Public -

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
    NSString *formattedMsgWithoutDate = [NSString stringWithFormat:@"%@ %s line %d $ %@",
                                         logFlagToString(logMessage->logFlag),
                                         logMessage->function,
                                         logMessage->lineNumber,
                                         logMessage->logMsg];
    
    NSString *formattedMsg = [NSString stringWithFormat:@"%@ %@",
                              [self.dateFormatter stringFromDate:[NSDate date]],
                              formattedMsgWithoutDate];
    
    if ([_delegate respondsToSelector:@selector(logFormatterWillReturnFormattedLogMessage:)])
    {
        [_delegate logFormatterWillReturnFormattedLogMessage:formattedMsgWithoutDate];
    }
    
    return formattedMsg;
}

@end


NSString *logFlagToString(int logFlag)
{
    switch (logFlag) {
            
        case LOG_FLAG_ERROR:
            return @"ERROR";
            
        case LOG_FLAG_WARN:
            return @"WARN";
            
        case LOG_FLAG_INFO:
            return @"INFO";
            
        case LOG_FLAG_DEBUG:
            return @"DEBUG";
            
        case LOG_FLAG_VERBOSE:
            return @"VERBOSE";
            
        case LOG_LEVEL_OFF:
            return @"ALL";
            
        default:
            NSCAssert(NO, @"Unknown value %d", logFlag);
            return @"";
    }
}