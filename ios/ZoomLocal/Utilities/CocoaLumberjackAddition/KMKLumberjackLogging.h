//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//


// USAGE: IMPORT THIS FILE IN PCH FILE.

#ifndef _LumberjackLogging_h
#define _LumberjackLogging_h

#import "DDLog.h"

#ifdef LOG_LEVEL_DEF
#undef LOG_LEVEL_DEF
    // "ddLogLevel" name can be used in other frameworks (for example in "MagicalRecord" framework), so we must change its name to avoid name collision.
    #define LOG_LEVEL_DEF kmk_ddLogLevel
#endif

// Setting log level
#ifdef DEBUG
    static const int kmk_ddLogLevel = LOG_LEVEL_DEBUG;
#else
    static const int kmk_ddLogLevel = LOG_LEVEL_INFO;
#endif

/* TO OVERRIDE LOG LOVEL IN ANY IMPLEMENTATION FILE USE THE FOLLOWING WAY:
     #ifdef DEBUG
         #undef LOG_LEVEL_DEF
         #define LOG_LEVEL_DEF OverridenLogLevel
         static const int OverridenLogLevel = LOG_LEVEL_VERBOSE; // log level for current m-file.
     #endif
 */

/**
 Replace 'sel_getName(_cmd)` with implicitly declared identifier `__PRETTY_FUNCTION__`, which provide method name in more convenient way.
 For example for `formatLogMessage:` method of LogFormatter looks as follows:
 
                                    -[LogFormatter formatLogMessage:]
 */
#ifdef LOG_OBJC_MAYBE
#undef LOG_OBJC_MAYBE
#define LOG_OBJC_MAYBE(async, lvl, flg, ctx, frmt, ...) LOG_MAYBE(async, lvl, flg, ctx, __PRETTY_FUNCTION__, frmt, ##__VA_ARGS__)
#endif


// Switch on synchronized mode of loging in debug configaration.
// Solution from:
// http://stackoverflow.com/questions/8691399/immediately-flushing-log-statements-using-the-cocoa-lumberjack-logging-framework
#ifdef DEBUG

    #undef LOG_ASYNC_WARN
    #define LOG_ASYNC_WARN     NO

    #undef LOG_ASYNC_INFO
    #define LOG_ASYNC_INFO     NO

    #undef LOG_ASYNC_DEBUG
    #define LOG_ASYNC_DEBUG    NO

    #undef LOG_ASYNC_VERBOSE
    #define LOG_ASYNC_VERBOSE  NO

    #endif

#endif
