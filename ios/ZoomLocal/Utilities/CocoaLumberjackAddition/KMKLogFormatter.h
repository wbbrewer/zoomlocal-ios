//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import <Foundation/Foundation.h>

#import "DDLog.h"


@protocol KMKLogFormatterDelegate <NSObject>

@optional

/**
 @param formattedLogMessage The whole being logged string without date in the very beginning (string may be passed to oher logger which adds date prefix ifself).
 */
- (void)logFormatterWillReturnFormattedLogMessage:(NSString *)formattedMessageWithoutDate;

@end


@interface KMKLogFormatter : NSObject<DDLogFormatter>

@property (weak, nonatomic) id<KMKLogFormatterDelegate> delegate;

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage;

@end
