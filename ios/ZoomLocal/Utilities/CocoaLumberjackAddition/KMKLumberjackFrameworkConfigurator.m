
//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "KMKLumberjackFrameworkConfigurator.h"

#import <DDTTYLogger.h>
#import <DDLog.h>

#import "KMKLogFileManager.h"
#import "KMKLogFormatter.h"


static const NSUInteger kMaximumFileSize = 1024 * 1024;             // 1 MiB
static const NSUInteger kRollingFrequency_sec = 60 * 60 * 24 * 30;  // 30 days
static const NSUInteger kMaximumNumberOfLogFiles = 2;


@interface KMKLumberjackFrameworkConfigurator() <KMKLogFileManagerDelegate, KMKLogFileManagerDelegate>

@end

@implementation KMKLumberjackFrameworkConfigurator {
    DDFileLogger *_fileLogger;
}

#pragma mark Singleton

static id uniqueInstance = nil;

+ (instancetype)sharedConfigurator
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        uniqueInstance = [[super alloc] initUniqueInstance];
    });
    return uniqueInstance;
}

- (void)setUp
{
    // Does nothing, it is sufficient to access at least once the shared instance of this class to set up all.
}

// TODO: make possible to get logs using iTunes
- (instancetype)initUniqueInstance
{
    self = [super init];
    if (self != nil)
    {
        [DDTTYLogger sharedInstance].logFormatter = [[KMKLogFormatter alloc] init];
        [DDLog addLogger:[DDTTYLogger sharedInstance]]; // output to XCode's console (stder)
    }
    return self;
}

#pragma mark -
#pragma mark Public

- (void)setLogIntoFile:(BOOL)logIntoFile
{
    if (logIntoFile && (_fileLogger == nil))
    {
        _fileLogger = [self createFileLogger];
        [DDLog addLogger:_fileLogger];
        [self logAppVersion];
    }
    else if (!logIntoFile && (_fileLogger != nil))
    {
        // TODO: make sure that log files are removed too and notice this in comment to the method
        [DDLog removeLogger:_fileLogger];
    }
}

#pragma mark <KMKLogFileManagerDelegate>

- (void)kmk_didArchiveLogFile:(NSString *)logFilePath
{
    [self logAppVersion]; 
}

- (void)kmk_didRollAndArchiveLogFile:(NSString *)logFilePath
{
    [self logAppVersion];    
}

#pragma mark - Private

- (void)logAppVersion
{
    if (self.appVersion) {
        DDLogInfo(@"App version is '%@'", self.appVersion);
    }
}

- (NSString *)appVersion
{
    NSString *version = [[NSBundle mainBundle].infoDictionary valueForKey:@"CFBundleShortVersionString"];
    return version;
}

- (DDFileLogger *)createFileLogger
{
    NSString *logsDirectory = [self logsDirectory];
    
    KMKLogFileManager *documentsFileManager = [[KMKLogFileManager alloc] initWithLogsDirectory:logsDirectory];
    documentsFileManager.delegate = self;
    
    DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:documentsFileManager];
    DDLogDebug(@"Logging to file using 'CocoaLumberjack' is added. Log directory: '%@'.", logsDirectory);
    KMKLogFormatter *fileLogFormatter = [[KMKLogFormatter alloc] init];
    fileLogger.logFormatter = fileLogFormatter;
    // Configure File Logger
    [fileLogger setMaximumFileSize:kMaximumFileSize];
    [fileLogger setRollingFrequency:kRollingFrequency_sec];
    [[fileLogger logFileManager] setMaximumNumberOfLogFiles:kMaximumNumberOfLogFiles];
    return fileLogger;
}

- (NSString *)logsDirectory
{
    NSURL *URLForDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                     inDomains:NSUserDomainMask] lastObject];
    return [URLForDirectory path];
}

@end
