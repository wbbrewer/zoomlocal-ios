//
//  FilterByDistanceContainerViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 13/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat ZLFilterByDistanceHeight;


@interface FilterByDistanceContainerViewController : UIViewController

@property (assign, nonatomic) float radiusOfSearchInMiles;
@property (strong, nonatomic) NSString *locationByName;

@end
