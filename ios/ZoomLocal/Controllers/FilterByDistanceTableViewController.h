//
//  FilterByDistanceTableViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 14/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

@import UIKit;

@class FilterByDistanceTableViewController;

extern const double ZLFilterByDistanceDefaultRadiusOfViewing;



@protocol FilterByDistanceTableViewControllerDelegate <NSObject>

// Invoked when pressing "Save" button
- (void)filterByDistanceTableViewControllerDidSaveSettings:(FilterByDistanceTableViewController *)viewController;

@end


@interface FilterByDistanceTableViewController : UITableViewController

@property (weak, nonatomic) id<FilterByDistanceTableViewControllerDelegate> delegate;

// Default value is `ZLFilterByDistanceDefaultRadius`.
@property (assign, nonatomic) double radiusInMiles;

@end
