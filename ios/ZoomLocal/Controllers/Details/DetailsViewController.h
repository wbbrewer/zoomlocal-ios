//
//  DetailsViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 28/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"

#warning remove
@interface DetailsViewController : ZLEmptyDataSetViewController

/**
 @param image Can be `nil`.
 @param url Can be `nil`.
 If servise Twitter is not available (there is no set up account in settings) then alert is shown with warning.
 */
- (void)presentComposeViewControllerForTwitterWithInitialText:(NSString *)text
                                                        image:(UIImage *)image
                                                         link:(NSURL *)url;

@end
