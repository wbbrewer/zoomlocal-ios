
#import <UIKit/UIKit.h>

#import "ZLVendorDetailsModel.h"
#import "ZLEmptyDataSetViewController.h"

@class TermModel;


@interface ZLRetailerDetailsViewContoller : ZLEmptyDataSetViewController

@property (weak, nonatomic) ZLVendorDetailsModel *vendorDetailsModel;

@end
