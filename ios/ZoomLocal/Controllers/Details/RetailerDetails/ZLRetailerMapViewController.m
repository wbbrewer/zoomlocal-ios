//
//  ZLRetailerMapViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRetailerMapViewController.h"

@import MapKit;
@import AddressBook;

#import "TermModel.h"
#import "ZLConstants.h"


@interface ZLRetailerMapViewController () <ZLVendorDetailsModelObserver>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation ZLRetailerMapViewController

// `vendorDetailsModel` can be `nil`.
- (void)setVendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [_vendorDetailsModel removeObserver:self];
    _vendorDetailsModel = vendorDetailsModel;
    [_vendorDetailsModel addObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.vendorDetailsModel.vendorTerm) {
        [self addVendorPinToMapWithCoordinate:self.vendorDetailsModel.vendorTerm.vendor.location.coordinate];
    }
    
    self.navigationController.toolbar.tintColor = [UIColor whiteTintColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setToolbarHidden:NO animated:NO];
    
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                   target:self
                                                                                   action:nil];
    UIBarButtonItem *routeItem = [[UIBarButtonItem alloc] initWithTitle:@"Route"
                                                                  style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(routeButtonPressed:)];
    [self.parentViewController setToolbarItems:@[flexiableItem, routeItem] animated:YES];
    self.navigationController.toolbar.barTintColor = [UIColor tealColor];
}

// In `viewDidDisappear` navigatation controller is unaccessible (nil).
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:NO];
    [self.parentViewController setToolbarItems:nil animated:YES];
}

#pragma mark <ZLVendorDetailsModelObserver>

- (void)vendorDetailsModelDidStartUpdating:(ZLVendorDetailsModel *)vendorDetailsModel
{
    
}

- (void)vendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel didUpdateVendorTermWithError:(NSError *)error
{
    if (error == nil) {
        [self addVendorPinToMapWithCoordinate:self.vendorDetailsModel.vendorTerm.vendor.location.coordinate];
    }
    else {
        DDLogError(@"Error: '%@'", error);
    }
}
                              
#pragma mark Actions

- (void)routeButtonPressed:(UIButton *)button
{
    NSDictionary *addressDictionary = @{(id)kABPersonAddressStreetKey : self.vendorDetailsModel.vendorTerm.vendor.displayAddress };
    CLLocationCoordinate2D vendorCoordinate = self.vendorDetailsModel.vendorTerm.vendor.location.coordinate;
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:vendorCoordinate addressDictionary:addressDictionary];
    MKMapItem *vendorMapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    
    [vendorMapItem openInMapsWithLaunchOptions:@{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving}];
}

#pragma mark Helpers

- (void)addVendorPinToMapWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    // Add an annotation.
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    point.coordinate = coordinate;
    [_mapView addAnnotation:point];
    _mapView.showsUserLocation = YES;
    
    // Zoom the region.
    MKCoordinateRegion region;
    
    float spanX = 0.01;
    float spanY = 0.01;
    region.center = coordinate;
    region.span = MKCoordinateSpanMake(spanX, spanY);
    
    [_mapView setRegion:region animated:YES];
}

@end
