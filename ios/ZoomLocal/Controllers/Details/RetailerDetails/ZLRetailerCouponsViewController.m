//
//  ZLRetailerCouponsViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 09/08/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRetailerCouponsViewController.h"

#import "ZLItemsCollectionView.h"
#import "ZLDealsCollectionViewCell.h"
#import "ZLDealDetailsViewController.h"
#import "ZLWPApiClient.h"
#import "ZLCoupon.h"
#import "ZLErrorHandler.h"
#import "ZLUserSessionManager.h"

static NSString *const kDealsCellReuseIdentifier = @"DealsCell";


@interface ZLRetailerCouponsViewController () <
                                                ZLItemsCollectionViewDataSource,
                                                ZLItemsCollectionViewDelegate,
                                                ZLVendorDetailsModelObserver,
                                                ZLDealDetailsViewControllerDelegate,
                                                ZLUserSessionManagerObserver
                                                >

@property (strong, nonatomic) NSMutableArray *coupons;

@property (weak, nonatomic) IBOutlet ZLItemsCollectionView *couponsCollectionView;

@end


@implementation ZLRetailerCouponsViewController {
    BOOL _userHasLoggedIn;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        _coupons = [NSMutableArray array];
        [[ZLUserSessionManager sharedManager] addUserSessionObserver:self];
    }
    return self;
}

- (void)dealloc
{
    [[ZLUserSessionManager sharedManager] removeUserSessionObserver:self];
}

// `vendorDetailsModel` can be `nil`.
- (void)setVendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [_vendorDetailsModel removeObserver:self];
    _vendorDetailsModel = vendorDetailsModel;
    [_vendorDetailsModel addObserver:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.couponsCollectionView.underlyingScrollView.emptyDataSetSource = self; // imlemented in base class
    self.couponsCollectionView.underlyingScrollView.emptyDataSetDelegate = self; // implemented in base class
    
    UINib *dealsCellNib = [UINib nibWithNibName:NSStringFromClass([ZLDealsCollectionViewCell class]) bundle:nil];
    [self.couponsCollectionView registerNib:dealsCellNib
               forCellWithReuseIdentifier:kDealsCellReuseIdentifier];
    
    if (self.vendorDetailsModel.vendorTerm != nil) {
        [self downloadCoupons];
    }
}

#pragma mark <ZLUserSessionManagerObserver>

- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus
{
    if (loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        _userHasLoggedIn = YES;
    }
}

#pragma mark <ZLItemsCollectionViewDataSource>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView
{
    return _coupons.count;
}

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row
{
    UICollectionViewCell *cell = nil;
    
    ZLDealsCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:kDealsCellReuseIdentifier
                                                                                       forRow:row];
    aCell.coupon = _coupons[row];
    cell = aCell;
    
    return cell;
}

#pragma mark <ZLItemsCollectionViewDelegate>

- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row
{
    ZLCoupon *coupon = self.coupons[row];
    ZLDealDetailsViewController *dealDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DealDetailsViewController"];
    [dealDetailsViewController showCoupon:coupon withParentViewController:self delegate:self];
}

#pragma mark <ZLVendorDetailsModelObserver>

- (void)vendorDetailsModelDidStartUpdating:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [self moveToBusyState];
}

- (void)vendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel didUpdateVendorTermWithError:(NSError *)error
{
    if (error == nil) {
        [self moveToNormalStateWithEmptySetFlag:NO];
        [self downloadCoupons];
    }
    else {
        DDLogError(@"Error: '%@'", error);
        [self moveToErrorStateWithError:error];
    }
}

#pragma mark <ZLDealDetailsViewControllerDelegate>

- (void)dealDetailsViewControllerDidPressCloseButton:(ZLDealDetailsViewController *)vc
{
    // It is possible that user has logged in, then displayed data should be updated in cells
    [self updateListIfNeeded];
}

- (void)dealDetailsViewController:(ZLDealDetailsViewController *)vc didRedeemCoupon:(ZLCoupon *)coupon
{
    NSUInteger indexOfCoupon = NSNotFound;
    for (indexOfCoupon = 0; indexOfCoupon < [_coupons count]; indexOfCoupon++) {
        if ([[_coupons[indexOfCoupon] listingId] isEqualToNumber:coupon.listingId]) {
            break;
        }
    }
    
    [_coupons replaceObjectAtIndex:indexOfCoupon withObject:coupon];
    
    for (ZLDealsCollectionViewCell *cell in [self.couponsCollectionView visibleCells]) {
        if ([cell.coupon.listingId isEqualToNumber:coupon.listingId]) {
            cell.coupon = coupon;
        }
    }
}


#pragma mark - Private -

- (void)updateListIfNeeded
{
    if (_userHasLoggedIn) {
        [self updateList];
    }
}

- (void)updateList
{
    [self.coupons removeAllObjects];
    [self downloadCoupons];
}


// https://listings-dev.zoomlocal.com/wp-json/rest/v2/all?featured=yes&filter[yith_shop_vendor]=panache&page=1&per_page=0
- (void)downloadCoupons
{
    _userHasLoggedIn = NO;

    [self moveToBusyState];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"yes" forKey:@"featured"];
    [parameters setObject:_vendorDetailsModel.vendorTerm.slug forKey:@"filter[yith_shop_vendor]"];
    [parameters setObject:@(1) forKey:@"page"];
    [parameters setObject:@(0) forKey:@"per_page"];
    
    // Example: https://listings-dev.zoomlocal.com/wp-json/rest/v2/coupons
    [[ZLWPApiClient sharedClient] authorizedGET:@"wp-json/rest/v2/coupons"
                                     parameters:parameters     
                                     completion:^(OVCResponse *response, NSError *error) {
                                         
                                         if (error == nil) {
                                             
                                             // store the items into the existing list
                                             for (ZLCoupon *obj in response.result) {
                                                 [_coupons addObject:obj];
                                             }
                                             
                                             [self moveToNormalStateWithEmptySetFlag:(_coupons.count == 0)];
                                             [self.couponsCollectionView reloadData];
                                         }
                                         else {
                                             [self moveToErrorStateWithError:error];
                                             [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                             DDLogError(error.localizedDescription);
                                         }
                                     }];
}

@end
