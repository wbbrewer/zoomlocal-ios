//
//  ZLIRetailerItemsViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"
#import "ZLVendorDetailsModel.h"


@interface ZLIRetailerItemsViewController : ZLEmptyDataSetViewController

@property (weak, nonatomic) ZLVendorDetailsModel *vendorDetailsModel;

@end
