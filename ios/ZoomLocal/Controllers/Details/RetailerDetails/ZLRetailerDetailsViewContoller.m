#import "ZLRetailerDetailsViewContoller.h"

@import Twitter;
@import SafariServices;

#import "UIImageView+WebCache.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIButton+Indicator.h"
#import <UIAlertController+Blocks.h>

#import "ZLRetailerContactsTableCell.h"
#import "TermModel.h"
#import "ZLRetailerDescriptionTableCell.h"
#import "UIViewController+ZoomLocal.h"
#import "ZLWPApiClient.h"
#import "ZLErrorHandler.h"
#import "ZLSocialsModel.h"

static NSString *const kVendorDetailsSharingMessage = @"Check out this retailer I found on ZoomLocal!\r\n\r\n";



@interface ZLRetailerDetailsViewContoller () <UITableViewDataSource,
                                            UITableViewDelegate,
                                            ZLRetailerContactsTableCellDelegate,
                                            SFSafariViewControllerDelegate,
                                            ZLVendorDetailsModelObserver>

@property (strong, nonatomic) TermModel *vendorTermModel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end



@implementation ZLRetailerDetailsViewContoller {
    UIButton *_followButton;
}

// `vendorDetailsModel` can be `nil`.
- (void)setVendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [_vendorDetailsModel removeObserver:self];
    _vendorDetailsModel = vendorDetailsModel;
    [_vendorDetailsModel addObserver:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ZLRetailerContactsTableCell class]) bundle:nil]
         forCellReuseIdentifier:@"ZLRetailerContactsTableCell"];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ZLRetailerDescriptionTableCell class]) bundle:nil]
         forCellReuseIdentifier:@"ZLRetailerDescriptionTableCell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // When user swipe back the amount of fading coincides with how far user has panned.
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
    
    self.tableView.estimatedRowHeight = 120.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.vendorTermModel == nil) {
        return 0;
    }
    else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell *cell = nil;
    
    switch(indexPath.row) {
        case 0: {
            ZLRetailerContactsTableCell *retailerDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"ZLRetailerContactsTableCell" forIndexPath:indexPath];
            [retailerDetailsCell.vendorImageView sd_setImageWithURL:[NSURL URLWithString:[_vendorTermModel.vendor.images.medium firstObject]]
                                                   placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
            retailerDetailsCell.delegate = self;
            
            [retailerDetailsCell.locationLabel setText:_vendorTermModel.vendor.displayAddress];
            [retailerDetailsCell.phoneButton setTitle:_vendorTermModel.vendor.telephone forState:UIControlStateNormal];
            
            [retailerDetailsCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            cell = retailerDetailsCell;
            break;
        }
        case 1: {
            ZLRetailerDescriptionTableCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"ZLRetailerDescriptionTableCell"
                                                                                    forIndexPath:indexPath];
            aCell.titleLabel.text = @"Description";
            aCell.contentTextLabel.text = _vendorTermModel.vendor.storeDescription;
            
            [self configureCellWithFollowButton:aCell];
            [self configureYoutubeButton:aCell];

            cell = aCell;
            break;
        }
        case 2: {
            ZLRetailerDescriptionTableCell *aCell = [tableView dequeueReusableCellWithIdentifier:@"ZLRetailerDescriptionTableCell"
                                                                                    forIndexPath:indexPath];
            aCell.titleLabel.text = @"Hours of operation";
            aCell.contentTextLabel.text = [self scheduleOfWorking];
            aCell.showFollowButton = NO;
            aCell.showYoutubeButton = NO;
            cell = aCell;
            break;
        }
    }
    
    return cell;
}

#pragma mark <UITableViewDelegate>

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // WORKAROUND: Setting background color of cell in cell's xib file doesn't work for iPad
    // http://stackoverflow.com/questions/27551291/uitableview-backgroundcolor-always-white-on-ipad
    [cell setBackgroundColor:[UIColor clearColor]];
}

#pragma <SFSafariViewControllerDelegate>

- (void)safariViewControllerDidFinish:(nonnull SFSafariViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark <ZLRetailerContactsTableCellDelegate>

- (void)retailerContactsTableCell:(ZLRetailerContactsTableCell *)view
          didPressShareButtonOfType:(ZLShareButtonType)shareButtonType
{
    switch (shareButtonType) {
        case ZLShareButtonTypePostToFacebook: {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            content.contentURL = [NSURL URLWithString:_vendorTermModel.vendorDetailsURL];
            // Title is taken into account when sharing zoomlocal links
            content.contentTitle = [NSString stringWithFormat:@"Check out %@ | %@",
                                    _vendorTermModel.vendor.name,
                                    [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey]];
            // Description is not taken into account when sharing zoomlocal links
            //    content.contentDescription = @"Content Description";
            content.imageURL = [NSURL URLWithString:[_vendorTermModel.vendor.images.thumbnail firstObject]];
            
            FBSDKShareDialog *shareDialog = [FBSDKShareDialog new];
            // When using `FBSDKShareDialogModeAutomatic` sdk chooses sometimes modes in which the wrong image is taken for sharing.
            [shareDialog setMode:FBSDKShareDialogModeFeedBrowser];
            [shareDialog setShareContent:content];
            [shareDialog setFromViewController:self];
            [shareDialog show];
            break;
        }
        case ZLShareButtonTypePostToTwitter: {
            UIImageView *imageView = [[UIImageView alloc] init];
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:[_vendorTermModel.vendor.images.medium firstObject]]
                         placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
            [self presentComposeViewControllerForTwitterWithInitialText:kVendorDetailsSharingMessage
                                                                  image:imageView.image
                                                                   link:[NSURL URLWithString:_vendorTermModel.vendorDetailsURL]];
        }
            
            break;
            
        default:
            NSAssert(NO, @"Unknown case!");
            break;
    }
}

#pragma mark <ZLVendorDetailsModelObserver>

- (void)vendorDetailsModelDidStartUpdating:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [self moveToBusyState];
}

- (void)vendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel didUpdateVendorTermWithError:(NSError *)error
{
    if (error == nil) {
        [self moveToNormalStateWithEmptySetFlag:NO];
        self.vendorTermModel = vendorDetailsModel.vendorTerm;
        [self.tableView reloadData];        
    }
    else {
        DDLogError(@"Error: '%@'", error);
        [self moveToErrorStateWithError:error];
    }
}

#pragma mark - Private -

#pragma mark Actions

- (void)followButtonPressed:(UIButton *)button
{
    BOOL wasVendorDataReceivedForAuthenticatedUser = (_vendorTermModel.vendor.isFollowed != nil);
    
    if (!wasVendorDataReceivedForAuthenticatedUser) { // "Follow" status is unknown, which means that user is not "Logged in".
        
        NSString *URLString = [@"wp-json/rest/v2/vendor/" stringByAppendingFormat:@"%@", self.vendorTermModel.termId];
        [_followButton showIndicator];
        
        // Attempt to post nothing, just to ensure that user is logged in. If user is not logged in then we don't know actual state
        // of Follow button and show it as if user does not follow the vendor. If he will happen to follow the vendor in after idle request
        // then nothing will be done, otherwise there will be repeated request to check user as "Following".
        // Method `authorizedPOST:...` will login user automatically.
        // https://listings-dev.zoomlocal.com/wp-json/rest/v2/vendor/642
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString // idle request to get personilized produt (with populated "is_liked" field for current user)
                                          parameters:nil
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:YES
                                          completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                            
                                            [_followButton hideIndicator];
                                            
                                            if (error == nil) {
                                                self.vendorTermModel = (TermModel *)response.result;
                                                [self updateUIWithVendorTerm:self.vendorTermModel];
                                                
                                                // The initial state of button is unknown and title is "Follow". And if after authentication received state is "Followed"
                                                // then nothing should be done any more.
                                                
                                                if ([self.vendorTermModel.vendor.isFollowed boolValue] != YES) {
                                                    [self followButtonPressedWhenPersonilizedInfoWasAccessible];
                                                }
                                                
                                            }
                                            else {
                                                DDLogError(@"Error: '%@'", error);
                                                [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                            }
                                        }];
    }
    else {
        [self followButtonPressedWhenPersonilizedInfoWasAccessible];
    }
}

- (void)followButtonPressedWhenPersonilizedInfoWasAccessible
{
    NSParameterAssert(self.vendorTermModel.vendor.isFollowed != nil);
    
    NSString *URLString = [@"wp-json/rest/v2/vendor/" stringByAppendingFormat:@"%@", self.vendorTermModel.termId];
    
    if ([self.vendorTermModel.vendor.isFollowed boolValue] == NO) {
        
        [_followButton showIndicator];
        
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString
                                          parameters:@{ @"is_followed" : @YES }
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:NO
                                          completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                              
                                            [_followButton hideIndicator];
                                            
                                            if (error == nil) {
                                                self.vendorTermModel = (TermModel *)response.result;
                                                [self updateUIWithVendorTerm:self.vendorTermModel];
                                            }
                                            else {
                                                DDLogError(@"Error: '%@'", error);
                                                [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                            }
                                        }];
    }
    else if ([self.vendorTermModel.vendor.isFollowed boolValue]) { // "Unlike" button pressed and user currently is not in "likes" list
        
        [_followButton showIndicator];
        
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString
                                          parameters:@{ @"is_followed" : @NO }
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:NO
                                          completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                            
                                            [_followButton hideIndicator];
                                            
                                            if (error == nil) {
                                                self.vendorTermModel = (TermModel *)response.result;
                                                [self updateUIWithVendorTerm:self.vendorTermModel];
                                            }
                                            else {
                                                DDLogError(@"Error: '%@'", error);
                                                [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                            }
                                        }];
    }
}

- (void)youtubeButtonPressed:(UIButton *)button
{
    [self openLinkInSafariViewController:self.vendorTermModel.vendor.socialsModel.youtubeLink];
}

- (void)youtubeAppLinkButtonPressed:(UIButton *)button
{
    NSString *msg = @"You are being directed to the YouTube App Download Page, "
    @"once you download come back to ZoomLocal to Zoom around all the great 360 vendor videos";
    
    [UIAlertController showAlertInViewController:self
                                       withTitle:nil
                                         message:msg
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@[@"OK"]
                                        tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                            if (buttonIndex != controller.cancelButtonIndex) {
                                                NSString *iTunesLink = @"https://itunes.apple.com/by/app/youtube/id544007664?mt=8";
                                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                                            }
                                        }];
}


#pragma mark Helpers

- (NSString *)scheduleOfWorking
{
    NSMutableString *scheduleOfWorking = [NSMutableString new];
    for (int i = 0; i < 7; i++) {
        if ([self workingHoursForWeekDay:i] != nil) {
            [scheduleOfWorking appendString:[self nameOfWeekDay:i]];
            // If to remove leading 2 spaces then for "Fri" dash is moved to the left ralative to locations of other dashes
            [scheduleOfWorking appendString:@"  \t- "];
            [scheduleOfWorking appendString:[self workingHoursForWeekDay:i]];
            [scheduleOfWorking appendString:@"\n"];
        }
    }
    // Remove last "\n"
    [scheduleOfWorking stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    return [scheduleOfWorking copy];
}

/// 0 corresponds to "Sun"
- (NSString *)nameOfWeekDay:(NSInteger)weekDay
{
    NSParameterAssert(weekDay < 7);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *weekdaySymbol = [[formatter shortWeekdaySymbols] objectAtIndex:weekDay];
    return weekdaySymbol;
}

/// Example "11:00 AM to 06:00 PM"
/// `nil` if there is no hours.
- (NSString *)workingHoursForWeekDay:(NSInteger)weekDay
{
    StoreHoursModel *hours = _vendorTermModel.vendor.hours;
    NSArray *dayHours = nil;
    switch (weekDay) {
        case 0:
            dayHours = hours.sun;
            break;
        case 1:
            dayHours = hours.mon;
            break;
        case 2:
            dayHours = hours.tue;
            break;
        case 3:
            dayHours = hours.wed;
            break;
        case 4:
            dayHours = hours.thu;
            break;
        case 5:
            dayHours = hours.fri;
            break;
        case 6:
            dayHours = hours.sat;
            break;
        default:
            NSParameterAssert(weekDay < 7);
            return @"";
    }
    
    NSString *workingHours = nil;
    if ([dayHours[0] length] > 0) {
        workingHours = [NSString stringWithFormat:@"%@ to %@", (dayHours[0]) ?  dayHours[0] : @"", (dayHours[1]) ? dayHours[1] : @""];
    }
    
    return workingHours;
}

- (void)updateUIWithVendorTerm:(TermModel *)vendorTerm
{
    [self.tableView reloadData];
    [self configureFollowButtonAccordingAccessibleData];
}

- (void)configureCellWithFollowButton:(ZLRetailerDescriptionTableCell *)cell
{
    _followButton = cell.followButton;
    
    cell.showFollowButton = YES;
    [self configureFollowButtonAccordingAccessibleData];
    cell.followButtonLabel.text = [self messageForTableCellWithFollowButton];
    
    [cell.followButton addTarget:self
                          action:@selector(followButtonPressed:)
                forControlEvents:UIControlEventTouchUpInside];
}

/// Message that is shown on the right from the button.
- (NSString *)messageForTableCellWithFollowButton
{
    NSString *followMessage = nil;
    NSInteger followCount = [self.vendorTermModel.vendor.followCount integerValue];
    
    if (self.vendorTermModel.vendor.isFollowed != nil) { // "Follow" status is known for current user (user is authenticated)
        BOOL isFollowed = [self.vendorTermModel.vendor.isFollowed boolValue];
        
        if (isFollowed) {
            followMessage = [NSString stringWithFormat:@"You and %ld others follow this", (long)(followCount - 1)]; // excepting user
        }
        else {
            followMessage = [NSString stringWithFormat:@"%ld people follow this", (long)followCount];
        }
    }
    else {
        followMessage = [NSString stringWithFormat:@"%ld people follow this", (long)followCount];
    }
    
    return (followCount == 0 ? @"Be The First" : followMessage);
}


/**
 If user is not authenticated and "Like" status is unaccessible then button title is set to "Like"
 */
- (void)configureFollowButtonAccordingAccessibleData
{
    NSParameterAssert(_followButton);
    
    if (self.vendorTermModel.vendor.isFollowed == nil) {
        [_followButton setTitle:@"Follow" forState:UIControlStateNormal];
    }
    else if ([self.vendorTermModel.vendor.isFollowed boolValue] == NO) {
        [_followButton setTitle:@"Follow" forState:UIControlStateNormal];
    }
    else if ([self.vendorTermModel.vendor.isFollowed boolValue] == YES) {
        [_followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
    }
}

- (void)configureYoutubeButton:(ZLRetailerDescriptionTableCell *)descriptionCell
{
    if (self.vendorTermModel.vendor.socialsModel.youtubeLink.length != 0) { // server can return empty string
        descriptionCell.showYoutubeButton = YES;
        [descriptionCell.youtubeButton addTarget:self
                                          action:@selector(youtubeButtonPressed:)
                                forControlEvents:UIControlEventTouchUpInside];
        
        [descriptionCell.youtubeAppLinkButton addTarget:self
                                                 action:@selector(youtubeAppLinkButtonPressed:)
                                       forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *image = [[UIImage imageNamed:@"like_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [descriptionCell.youtubeButton setBackgroundImage:image forState:UIControlStateNormal];
        descriptionCell.youtubeButton.tintColor = [UIColor colorWithRed:238/255.0 green:28/255.0 blue:27/255.0 alpha:1.0];
    }
    else {
        descriptionCell.showYoutubeButton = NO;
    }
}

- (void)openLinkInSafariViewController:(NSString *)urlString
{
    NSURL *URL = [NSURL URLWithString:urlString];
    if (URL) {
        SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
        sfvc.delegate = self;
        [self presentViewController:sfvc animated:YES completion:nil];
    }
}

@end
