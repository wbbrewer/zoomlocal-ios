//
//  ZLIRetailerItemsViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLIRetailerItemsViewController.h"

#import "ZLItemsCollectionView.h"
#import "ZLProductCollectionViewCell.h"
#import "ZLWPApiClient.h"
#import "TermModel.h"
#import "ProductModel.h"
#import "ListingDetailsViewController.h"
#import "EventDetailsViewController.h"
#import "ZLErrorHandler.h"


@interface ZLIRetailerItemsViewController () <
                                                ZLItemsCollectionViewDataSource,
                                                ZLItemsCollectionViewDelegate,
                                                ZLVendorDetailsModelObserver
                                             >

@property (strong, nonatomic) NSMutableArray *items;

@property (weak, nonatomic) IBOutlet ZLItemsCollectionView *itemsCollectionView;

@end


@implementation ZLIRetailerItemsViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        _items = [NSMutableArray array];
    }
    return self;
}

// `vendorDetailsModel` can be `nil`.
- (void)setVendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [_vendorDetailsModel removeObserver:self];
    _vendorDetailsModel = vendorDetailsModel;
    [_vendorDetailsModel addObserver:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.itemsCollectionView.underlyingScrollView.emptyDataSetSource = self; // imlemented in base class
    self.itemsCollectionView.underlyingScrollView.emptyDataSetDelegate = self; // implemented in base class

    UINib *productCollectionViewCellNib = [UINib nibWithNibName:NSStringFromClass([ZLProductCollectionViewCell class])
                                                         bundle:nil];
    [self.itemsCollectionView registerNib:productCollectionViewCellNib
               forCellWithReuseIdentifier:@"ProductCollectionViewCell"];
    
    if (self.vendorDetailsModel.vendorTerm != nil) {
        [self downloadItems];
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showListingDetails"]) {
        ListingDetailsViewController *controller = (ListingDetailsViewController *)[segue destinationViewController];
        NSInteger row = [self.itemsCollectionView rowOfSelectedItem];
        controller.productModel = _items[row];
    }
    else if ([[segue identifier] isEqualToString:@"showEventDetails"]) {
        EventDetailsViewController *controller = (EventDetailsViewController *)[segue destinationViewController];
        NSInteger row = [self.itemsCollectionView rowOfSelectedItem];
        controller.productModel = _items[row];
    }
}

#pragma mark <ZLItemsCollectionViewDataSource>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView
{
    return _items.count;
}

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row
{
    UICollectionViewCell *cell = nil;
    
    ZLProductCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCollectionViewCell"
                                                                                       forRow:row];
    aCell.productModel = _items[row];
    cell = aCell;
    
    return cell;
}

#pragma mark <ZLItemsCollectionViewDelegate>

- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row
{
    ProductModel *productModel = _items[row];
    BOOL isListing = (productModel.meta.eventStartDate == nil);
    if (isListing) {
        [self performSegueWithIdentifier:@"showListingDetails" sender:self];
    }
    else { // event
        [self performSegueWithIdentifier:@"showEventDetails" sender:self];
    }
}

#pragma mark <ZLVendorDetailsModelObserver>

- (void)vendorDetailsModelDidStartUpdating:(ZLVendorDetailsModel *)vendorDetailsModel
{
    [self moveToBusyState];
}

- (void)vendorDetailsModel:(ZLVendorDetailsModel *)vendorDetailsModel didUpdateVendorTermWithError:(NSError *)error
{
    if (error == nil) {
        [self moveToNormalStateWithEmptySetFlag:NO];
        [self downloadItems];
    }
    else {
        DDLogError(@"Error: '%@'", error);
        [self moveToErrorStateWithError:error];
    }
}

#pragma mark - Private -

// https://listings-dev.zoomlocal.com/wp-json/rest/v2/all?featured=yes&filter[yith_shop_vendor]=panache&page=1&per_page=0
- (void)downloadItems
{
    [self moveToBusyState];
    
    //Fetch the items
    [[ZLWPApiClient sharedClient] GET:@"wp-json/rest/v2/all"
                         parameters:@{@"featured" : @"yes",
                                      @"filter[yith_shop_vendor]" : _vendorDetailsModel.vendorTerm.slug,
                                      @"page" : @(1),
                                      @"per_page" : @"0"} // all items
                         completion:^(OVCResponse *response, NSError *error) {
                             
                             if (error == nil) {
                                 [self moveToNormalStateWithEmptySetFlag:(_items.count == 0)];
                                 
                                 // store the items into the existing list
                                 for (ProductModel *obj in response.result) {
                                     [_items addObject:obj];
                                 }
                                 
                                 [self.itemsCollectionView reloadData];
                             }
                             else {
                                 [self moveToErrorStateWithError:error];
                                 [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                 DDLogError(error.localizedDescription);
                             }
                         }];
}

@end
