//
//  ZLRetailerDetailsTopTabBarViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 10/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRetailerDetailsTopTabBarViewController.h"

#import "UIImageView+WebCache.h"
#import "UINavigationItem+KMKUtils.h"

#import "TermModel.h"
#import "ZLTabPanelView.h"
#import "ZLConstants.h"
#import "ZLIRetailerItemsViewController.h"
#import "ZLRetailerDetailsViewContoller.h"
#import "ZLRetailerMapViewController.h"
#import "ZLVendorDetailsModel.h"
#import "ZLDealsViewController.h"

static NSString *const kVendorDetailsSharingMessage = @"Check out this retailer I found on ZoomLocal!\r\n\r\n";
const int kNumOfViewControllers = 4;


@interface ZLRetailerDetailsTopTabBarViewController () <ZLTabPanelViewDelegate>

@property (strong, nonatomic) TermModel *termModel;

@property (weak, nonatomic) IBOutlet ZLTabPanelView *tabPanelView;
@property (nonatomic, weak) IBOutlet UIView *contentView;

@end


@implementation ZLRetailerDetailsTopTabBarViewController {
    ZLVendorDetailsModel *_vendorDetailsModel;
    
    UIViewController *_viewControllers[kNumOfViewControllers];
    NSUInteger _activeIndex;
}

- (void)dealloc
{
    for (int i = 0; i < kNumOfViewControllers; i++) {
        [_viewControllers[i] removeFromParentViewController];
    }
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    self.title = self.termModel.vendor.name;
    [self.navigationItem kmk_prepereForAdjustingFontSizeWithNumberOfLines:2];
    
    self.tabPanelView.tabTitles = @[@"Details", @"Items", @"Coupons", @"Map"];
    self.tabPanelView.tabTitlesColor = [UIColor grayTextColor];
    self.tabPanelView.tintColor = [UIColor blueTintColor];
    
//    NSParameterAssert(self.vendorTermId);
    _vendorDetailsModel = [[ZLVendorDetailsModel alloc] initWithVendorTermId:self.vendorTermId];
    
    ZLRetailerDetailsViewContoller *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZLRetailerDetailsViewContoller"];
    [self addChildViewController:detailsViewController atIndex:0];
    
    ZLIRetailerItemsViewController *itemsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZLIRetailerItemsViewController"];
    [self addChildViewController:itemsViewController atIndex:1];
    
    ZLDealsViewController *dealsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZLRetailerCouponsViewController"];
    [self addChildViewController:dealsViewController atIndex:2];
    
    ZLRetailerMapViewController *mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ZLRetailerMapViewController"];
    [self addChildViewController:mapViewController atIndex:3];
    
    _activeIndex = NSNotFound;
    [self switchToViewControllerAtIndex:0];
    
    [_vendorDetailsModel update]; // updating must be done after setting model to any tab.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    UIViewController *currentVC = self.childViewControllers[_activeIndex];
    if ([currentVC respondsToSelector:@selector(setVendorDetailsModel:)]) {
        [(id)currentVC setVendorDetailsModel:nil];
    }
}

#pragma mark <ZLTabPanelViewDelegate>

- (void)tabPanelView:(ZLTabPanelView *)tabPanelView didSelectTitle:(NSString *)title
{
    NSInteger index = [self.tabPanelView.tabTitles indexOfObject:title];
    [self switchToViewControllerAtIndex:index];
}

#pragma mark Actions

- (IBAction)shareAction:(id)sender
{
    UIImageView *imageView = [[UIImageView alloc] init];
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[_termModel.vendor.images.medium firstObject]]
                 placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
    
    NSArray *activityItems = [NSArray arrayWithObjects:
                              kVendorDetailsSharingMessage,
                              _termModel.vendorDetailsURL,
                              imageView.image,
                              nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                         applicationActivities:nil];
    
    // Looks like on iOS <= 8.2 ecluding doesn't work because of Apple's bug
    // http://stackoverflow.com/questions/30078598/cant-exclude-uiactivitytypeposttofacebook-from-uiactivityviewcontroller-exclud
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook, UIActivityTypePostToTwitter];
    
    // On iPad the activity view controller will be displayed as a popover, it requires to specify an anchor point for the presentation of the popover:
    activityViewController.popoverPresentationController.barButtonItem = sender;
    
    [self.navigationController presentViewController:activityViewController animated:YES completion:nil];
}


#pragma mark - Private -

- (void)addChildViewController:(UIViewController *)childController atIndex:(NSUInteger)index
{
    NSParameterAssert(index < kNumOfViewControllers);
    [self addChildViewController:childController];
    [childController didMoveToParentViewController:self];
    _viewControllers[index] = childController;
}

- (void)switchToViewControllerAtIndex:(NSUInteger)index
{
    if (_activeIndex != NSNotFound) {
        UIViewController *prevousVC = self.childViewControllers[_activeIndex];
        if ([prevousVC respondsToSelector:@selector(setVendorDetailsModel:)]) {
            [(id)prevousVC setVendorDetailsModel:nil];
        }
    }
    
    NSParameterAssert(index < kNumOfViewControllers);
    if (_activeIndex != NSNotFound) {
        [_viewControllers[_activeIndex].view removeFromSuperview];
    }
    _activeIndex = index;
    
    UIViewController *nextVC = self.childViewControllers[_activeIndex];
    if ([nextVC respondsToSelector:@selector(setVendorDetailsModel:)]) {
        [(id)nextVC setVendorDetailsModel:_vendorDetailsModel];
    }
    
    _viewControllers[_activeIndex].view.frame = self.contentView.bounds;
    _viewControllers[_activeIndex].view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.contentView insertSubview:_viewControllers[_activeIndex].view atIndex:0];
}

@end
