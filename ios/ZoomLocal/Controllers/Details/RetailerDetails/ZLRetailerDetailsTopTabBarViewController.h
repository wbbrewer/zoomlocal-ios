//
//  ZLRetailerDetailsTopTabBarViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 10/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "DetailsViewController.h"

@class TermModel;


@interface ZLRetailerDetailsTopTabBarViewController : DetailsViewController

@property (strong, nonatomic) NSNumber *vendorTermId;

@end
