//
//  ZLRetailerMapViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 11/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLVendorDetailsModel.h"

@class TermModel;


@interface ZLRetailerMapViewController : UIViewController

@property (weak, nonatomic) ZLVendorDetailsModel *vendorDetailsModel;

@end
