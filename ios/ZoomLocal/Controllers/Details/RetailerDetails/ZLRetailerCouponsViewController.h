//
//  ZLRetailerCouponsViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 09/08/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"

#import "ZLVendorDetailsModel.h"


@interface ZLRetailerCouponsViewController : ZLEmptyDataSetViewController 

@property (weak, nonatomic) ZLVendorDetailsModel *vendorDetailsModel;

@end
