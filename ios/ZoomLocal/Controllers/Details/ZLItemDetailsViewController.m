//
//  ItemDetailsViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLItemDetailsViewController.h"

@import Twitter;
#import <UIScrollView+VGParallaxHeader.h>
#import "UIView+KMKNib.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "UIButton+Indicator.h"

#import "ProductModel.h"
#import "ImageViewHeader.h"
#import "ProductDetailsStickyView.h"
#import "TealDetailsSectionHeaderView.h"
#import "UIViewController+ZoomLocal.h"
#import "ZLWPApiClient.h"
#import "ZLErrorHandler.h"
#import "ZLZoomImageViewController.h"
#import "ZLConstants.h"


NSString *const kProductDetailsSharingMessage = @"Hey, look what I found on ZoomLocal!\r\n\r\n";


@interface ZLItemDetailsViewController () <ProductDetailsStickyViewDelegate>

@property (strong, nonatomic) ProductModel *productModel;

@property ImageViewHeader *imageViewHeader;
@property (nonatomic, weak) UITableView *tableView;

@end


@implementation ZLItemDetailsViewController {
    __weak UIButton *_likeButton;
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TableCellWithLikeButton" bundle:nil]
         forCellReuseIdentifier:ZLTableCellWithLikeButtonIdentifier];
    
    if (_productModel != nil) {
        [self configureUIWithProductModel:_productModel];
    }
    else {
        NSParameterAssert(_productId);
        [self downloadProductModelWithId:_productId];
    }
}

#pragma mark <UIScrollViewDelegate>

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // This must be called in order to work
    [self.tableView shouldPositionParallaxHeader];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSAssert(NO, @"Abstract method!");
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSAssert(NO, @"Abstract method!");
    return nil;
}


#pragma mark <ProductDetailsStickyViewDelegate>

- (void)productDetailsStickyView:(ProductDetailsStickyView *)view didPressShareButtonOfType:(ZLShareButtonType)shareButtonType
{
    switch (shareButtonType) {
        case ZLShareButtonTypePostToFacebook: {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            content.contentURL = [NSURL URLWithString:_productModel.productLink];
            // Title is taken into account when sharing zoomlocal links
            content.contentTitle = [NSString stringWithFormat:@"%@ @ %@ | %@",
                                    _productModel.title,
                                    _productModel.term.vendor.name,
                                    [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey]];
            content.imageURL = [NSURL URLWithString:[_productModel.images.thumbnail firstObject]];
            // Description is not taken into account when sharing zoomlocal links
            //    content.contentDescription = @"Content Description";
            
            FBSDKShareDialog *shareDialog = [FBSDKShareDialog new];
            // When using `FBSDKShareDialogModeAutomatic` sdk chooses sometimes modes in which the wrong image is taken for sharing.
            [shareDialog setMode:FBSDKShareDialogModeFeedBrowser];
            [shareDialog setShareContent:content];
            [shareDialog setFromViewController:self];
            [shareDialog show];
            break;
        }
        case ZLShareButtonTypePostToTwitter:
            [self presentComposeViewControllerForTwitterWithInitialText:kProductDetailsSharingMessage
                                                                  image:_imageViewHeader.imageView.image
                                                                   link:[NSURL URLWithString:_productModel.productLink]];
            
            break;
            
        default:
            NSAssert(NO, @"Unknown case!");
            break;
    }
}


#pragma mark Actions

- (IBAction)shareAction:(UIBarButtonItem *)sender
{
    // If you need a significant amount of time to create objects then consider using a UIActivityItemProvider object
    // as constructing is perfomed in background thread when using it.

    NSArray *activityItems = [NSArray arrayWithObjects:kProductDetailsSharingMessage,
                              _productModel.productLink,
                              _imageViewHeader.imageView.image,
                              nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                         applicationActivities:nil];
    
    // On iPad the activity view controller will be displayed as a popover, it requires to specify an anchor point for the presentation of the popover:
    activityViewController.popoverPresentationController.barButtonItem = sender;
    
    // Looks like on iOS <= 8.2 ecluding doesn't work because of Apple's bug
    // http://stackoverflow.com/questions/30078598/cant-exclude-uiactivitytypeposttofacebook-from-uiactivityviewcontroller-exclud
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook, UIActivityTypePostToTwitter];
    
    [self.navigationController presentViewController:activityViewController animated:YES completion:nil];
}


#pragma mark - Subclass -

- (void)downloadProductModelWithId:(NSString *)productId
{
    NSAssert(NO, @"Abstract method!");
}

- (void)configureUIWithProductModel:(ProductModel *)productModel
{
    NSParameterAssert(self.productModel);
    _imageViewHeader = [ImageViewHeader viewWithDefaultNib];
    [_imageViewHeader setViewWithModel:productModel];
    
    UITapGestureRecognizer *tagGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(imageTapped:)];
    [_imageViewHeader addGestureRecognizer:tagGestureRecognizer];
    
    CGFloat imageWidth = [productModel.images.medium[1] floatValue];
    CGFloat imageHeight =[productModel.images.medium[2] floatValue];
    
    CGFloat imageViewHeight = 0.0;
    
    DDLogDebug(@"%f, %f", imageWidth, imageHeight);
    
    const BOOL isIPad = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
    self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular;
    
    if (imageWidth > imageHeight) {
        // Image in landscape
        
        if (isIPad) {
            imageViewHeight = 0.5 * self.view.bounds.size.width;
        }
        else {
            imageViewHeight = imageHeight;
            [_imageViewHeader.imageView setContentMode:UIViewContentModeScaleAspectFill];
        }
    }
    else if (imageWidth < imageHeight) {
        // Image in portrait
        
        if (isIPad) {
            imageViewHeight = 0.5 * self.view.bounds.size.width;
        }
        else {
            if (imageHeight < self.view.bounds.size.width) {
                imageViewHeight = (self.view.bounds.size.width < self.view.bounds.size.height) ? self.view.bounds.size.width : self.view.bounds.size.height *0.5;
            }
            else {
                imageViewHeight = imageWidth;
            }
        }
    }
    else {
        // Square
        imageViewHeight = (self.view.bounds.size.width < self.view.bounds.size.height) ? self.view.bounds.size.width : self.view.bounds.size.height *0.5;
    }
    
    //(self.view.bounds.size.width < self.view.bounds.size.height) ? self.view.bounds.size.width : self.view.bounds.size.height *.7)
    
    [self.tableView setParallaxHeaderView:_imageViewHeader
                                     mode:VGParallaxHeaderModeFill // For more modes have a look in UIScrollView+VGParallaxHeader.h
                                   height:imageViewHeight];
    
    ProductDetailsStickyView *productDetailsStickyView = [ProductDetailsStickyView viewWithDefaultNib];
    [productDetailsStickyView setViewWithModel:productModel.meta];
    productDetailsStickyView.delegate = self;
    
    self.tableView.parallaxHeader.stickyViewPosition = VGParallaxHeaderStickyViewPositionBottom;
    [self.tableView.parallaxHeader setStickyView:productDetailsStickyView
                                      withHeight:36];
    
    [self.tableView reloadData];
    [self configureLikeButtonAccordingAccessibleData];
}

- (void)configureCellWithLikeButton:(TableCellWithLikeButton *)cell
{
    cell.label.text = [self messageForTableCellWithLikeButton];
    
    _likeButton = cell.button;
    [cell.button addTarget:self
                    action:@selector(likeButtonPressed:)
          forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark Actions

- (void)likeButtonPressed:(UIButton *)button
{
    if (self.productModel.isLiked == nil) { // "Like" status is unknown, which means that user is not "Logged in".
        
        NSString *URLString = [@"wp-json/rest/v2/listing/" stringByAppendingString:self.productModel.listingId.stringValue];
        [_likeButton showIndicator];
        
        // Attempt to post nothing, just to ensure that user is logged in.
        // Method `authorizedPOST:...` will login user automatically.
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString // idle request to get personilized produt (with populated "is_liked" field for current user)
                                          parameters:nil
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:YES
                                          completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                              
                                              [_likeButton hideIndicator];
                                              
                                              if (error == nil) {
                                                  self.productModel = (ProductModel *)response.result;
                                                  NSAssert([_likeButton.titleLabel.text isEqualToString:@"Like"],
                                                           @"In unauthenticated state button is supposed to be in 'unliked' (title 'Like') state!");
                                                  [self configureUIWithProductModel:self.productModel];
                                                  
                                                  // The initial state of button is "Unliked" (title "Like"). And if after authentication received state is also "Liked"
                                                  // then nothing should be done any more.
                                                  
                                                  if ([self.productModel.isLiked boolValue] != YES) {
                                                      [self likeButtonPressedWhenPersonilizedInfoWasAccessible];
                                                  }
                                                  
                                              }
                                              else {
                                                  DDLogError(@"Error: '%@'", error);
                                                  [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                              }
                                          }];
    }
    else {
        [self likeButtonPressedWhenPersonilizedInfoWasAccessible];
    }
}

- (void)likeButtonPressedWhenPersonilizedInfoWasAccessible
{
    NSParameterAssert(self.productModel.isLiked != nil);
    
    NSString *URLString = [@"wp-json/rest/v2/listing/" stringByAppendingString:self.productModel.listingId.stringValue];
    
    if ([self.productModel.isLiked boolValue] == NO) {
        
        [_likeButton showIndicator];
        
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString
                                        parameters:@{ @"is_liked" : @YES }
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:NO
                                        completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                            
                                            [_likeButton hideIndicator];
                                            
                                            if (error == nil) {
                                                self.productModel = (ProductModel *)response.result;
                                                [self configureUIWithProductModel:self.productModel];
                                            }
                                            else {
                                                DDLogError(@"Error: '%@'", error);
                                                [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                            }
                                        }];
    }
    else if ([self.productModel.isLiked boolValue]) { // "Unlike" button pressed and user currently is not in "likes" list
        
        [_likeButton showIndicator];
        
        [[ZLWPApiClient sharedClient] authorizedPOST:URLString
                                          parameters:@{ @"is_liked" : @NO }
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:NO
                                          completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                              
                                              [_likeButton hideIndicator];
                                              
                                              if (error == nil) {
                                                  self.productModel = (ProductModel *)response.result;
                                                  [self configureUIWithProductModel:self.productModel];
                                              }
                                              else {
                                                  DDLogError(@"Error: '%@'", error);
                                                  [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                              }
                                          }];
    }
}

- (void)imageTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    ZLZoomImageViewController *vc = [[ZLZoomImageViewController alloc] initWithImage:_imageViewHeader.imageView.image];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    nc.navigationBar.barTintColor = [UIColor tealColor];
    nc.view.tintColor = [UIColor whiteTintColor];
    vc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(closeButtonOnZoomImageViewControllerPressed:)];
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)closeButtonOnZoomImageViewControllerPressed:(UIBarButtonItem *)item
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private -

/// Message that is shown on the right from the button.
- (NSString *)messageForTableCellWithLikeButton
{
    NSString *likeMsg = nil;
    NSInteger likes = [self.productModel.likes integerValue];
    
    if (self.productModel.isLiked != nil) { // "Like" status is known for current user (user is authenticated)
        BOOL isLiked = [self.productModel.isLiked boolValue];
        
        if (isLiked) {
            likeMsg = [NSString stringWithFormat:@"You and %ld others like this", (long)(likes - 1)]; // excepting user
        }
        else {
            likeMsg = [NSString stringWithFormat:@"%ld people like this", (long)likes];
        }
    }
    else {
        likeMsg = [NSString stringWithFormat:@"%ld people like this", (long)likes];
    }
    
    return (likes == 0 ? @"Be The First" : likeMsg);
}

/**
 If user is not authenticated and "Like" status is unaccessible then button title is set to "Like"
 */
- (void)configureLikeButtonAccordingAccessibleData
{
    if (self.productModel.isLiked == nil) {
        [_likeButton setTitle:@"Like" forState:UIControlStateNormal];
    }
    else if ([self.productModel.isLiked boolValue] == NO) {
        [_likeButton setTitle:@"Like" forState:UIControlStateNormal];
    }
    else if ([self.productModel.isLiked boolValue] == YES) {
        [_likeButton setTitle:@"Unlike" forState:UIControlStateNormal];
    }
}

@end
