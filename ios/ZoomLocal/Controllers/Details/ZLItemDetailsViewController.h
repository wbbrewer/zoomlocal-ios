//
//  ItemDetailsViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/8/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"

#import "TableCellWithLikeButton.h"

@class ProductModel;

static NSString *const ZLTableCellWithLikeButtonIdentifier = @"TableCellWithLikeButton";


extern NSString *const ZLItemDetailsSharingMessage;


@interface ZLItemDetailsViewController : ZLEmptyDataSetViewController <UITableViewDataSource,
                                                                UITableViewDelegate> {
@protected NSString *_productId;
@protected ProductModel *_productModel;
}

/**
 If `productModel` is not set then controller will try to download data using this id.
 */
@property (strong, nonatomic) NSString *productId;

@end


@interface ZLItemDetailsViewController (Subclass)

@property (strong, nonatomic) ProductModel *productModel;

@property (nonatomic, weak) IBOutlet UITableView *tableView;


- (void)downloadProductModelWithId:(NSString *)productId;

/**
 This method should be invoked when product has been downloaded.
 In particular it sets up image at the top of page.
 */
- (void)configureUIWithProductModel:(ProductModel *)productModel;
- (void)configureCellWithLikeButton:(TableCellWithLikeButton *)cell;
- (IBAction)shareAction:(UIBarButtonItem *)sender;

@end
