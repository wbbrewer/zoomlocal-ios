//
//  EventDetailsViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 29/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "EventDetailsViewController.h"

#import "TagDataLayer.h"
#import "TAGManager.h"
#import "UIImageView+WebCache.h"
#import "UIView+KMKNib.h"

#import "ItemDetailsDescriptionTableViewCell.h"
#import "ProductModel.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"
#import "TitleTableViewCell.h"
#import "ItemDetailsDescriptionTableViewCell.h"
#import "VendorDetailTableViewCell.h"
#import "RetailerDetailsButtonTableViewCell.h"
#import "EventModel.h"
#import "TealDetailsSectionHeaderView.h"


@interface EventDetailsViewController ()

@end


@implementation EventDetailsViewController

#pragma mark - Override -

- (void)setProductModel:(ProductModel *)productModel
{
    [super setProductModel:productModel];
    NSParameterAssert([productModel isKindOfClass:[EventModel class]] ||
                      [productModel isKindOfClass:[ProductModel class]]); // when moving from "Vendor details" model is kind of `ProductModel`
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TitleTableViewCell" bundle:nil] forCellReuseIdentifier:@"TitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ItemDetailsDescriptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"DescCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"VendorDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"VendorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RetailerDetailsButtonTableViewCell" bundle:nil] forCellReuseIdentifier:@"VendorReferenceCell"];
    
    self.tableView.estimatedRowHeight = 120.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Event Details"}];
    
    [self.tableView reloadData];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showVendor"]) {
        ZLRetailerDetailsTopTabBarViewController *controller = (ZLRetailerDetailsTopTabBarViewController *)[segue destinationViewController];
        TermModel *vendorTerm = (TermModel *)sender;
        controller.vendorTermId = vendorTerm.termId;
    }
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_productModel != nil) {
        return 4;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section) {
        case 2:
            return 2;
        case 3:
            return 2;
        default:
            return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0: {
            TitleTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell" forIndexPath:indexPath];
            [titleCell.titleLabel setText:_productModel.title];
            cell = titleCell;
            break;
        }
        case 1: {
            ItemDetailsDescriptionTableViewCell *eventDetailCell = [tableView dequeueReusableCellWithIdentifier:@"DescCell" forIndexPath:indexPath];
            
            NSString *startDate = [NSString stringWithFormat:@"Start Date: %@", [_productModel.meta localizedDateTime:_productModel.meta.eventStartDate]];
            
            NSString *endDate = [NSString stringWithFormat:@"End Date: %@", [_productModel.meta localizedDateTime:_productModel.meta.eventEndDate]];
            
            NSString *eventVenue = [NSString stringWithFormat:@"Venue: %@ %@, %@", _productModel.meta.eventAddress, _productModel.meta.eventCity, _productModel.meta.eventRegion];
            
            if (!_productModel.meta.eventEndDate) {
                [eventDetailCell.descLabel setText:[NSString stringWithFormat:@"%@\r\n%@", startDate, eventVenue]];
            }
            else {
                [eventDetailCell.descLabel setText:[NSString stringWithFormat:@"%@\r\n%@\r\n%@", startDate, endDate, eventVenue]];
            }
            
            cell = eventDetailCell;
            break;
        }
        case 2: {
            if (indexPath.row == 0) {
                ItemDetailsDescriptionTableViewCell *shortDescCell = [tableView dequeueReusableCellWithIdentifier:@"DescCell" forIndexPath:indexPath];
                [shortDescCell.descLabel setText:_productModel.shortDesc];
                cell = shortDescCell;
            }
            else if (indexPath.row == 1) {
                TableCellWithLikeButton *cellWithLikeButton = [tableView dequeueReusableCellWithIdentifier:ZLTableCellWithLikeButtonIdentifier
                                                                                              forIndexPath:indexPath];
                [self configureCellWithLikeButton:cellWithLikeButton];
                cell = cellWithLikeButton;
            }
            break;
        }
        case 3: {
            switch(indexPath.row) {
                case 0: {
                    VendorDetailTableViewCell *vendorCell = [tableView dequeueReusableCellWithIdentifier:@"VendorCell" forIndexPath:indexPath];
                    [vendorCell.vendorImageView sd_setImageWithURL:[NSURL URLWithString:[_productModel.term.vendor.images.medium firstObject]]
                                                  placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
                    [vendorCell.nameLabel setText:_productModel.term.vendor.name];
                    [vendorCell.locationLabel setText:_productModel.term.vendor.displayAddress];
                    cell = vendorCell;
                    break;
                }
                case 1: {
                    RetailerDetailsButtonTableViewCell *referenceCell = [tableView dequeueReusableCellWithIdentifier:@"VendorReferenceCell" forIndexPath:indexPath];
                    [referenceCell.vendorButton addTarget:self
                                                   action:@selector(showVendor)
                                         forControlEvents:UIControlEventTouchDown];
                    cell = referenceCell;
                    break;
                }
            }
            break;
        }
            
        default:
            NSAssert(NO, @"");
    }
    
    return cell;
}


#pragma mark <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch(section) {
        case 0:
            return 0;
        default:
            return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Dequeue the custom header cell
    TealDetailsSectionHeaderView *sectionHeaderView = [TealDetailsSectionHeaderView viewWithDefaultNib];
    
    switch (section) {
        case 0:
            [sectionHeaderView.textLabel setText:@"Title"];
            break;
        case 1:
            [sectionHeaderView.textLabel setText:@"Event Detail"];
            break;
        case 2:
            [sectionHeaderView.textLabel setText:@"Description"];
            break;
        case 3:
            [sectionHeaderView.textLabel setText:@"Offered by"];
            break;
        default: ;
    }
    
    return sectionHeaderView;
}


#pragma mark - Helpers -

- (void)showVendor
{
    [self performSegueWithIdentifier:@"showVendor" sender:_productModel.term];
}


@end
