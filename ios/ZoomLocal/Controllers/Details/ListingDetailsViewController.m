//
//  ListingDetailsViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 29/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ListingDetailsViewController.h"

#import <UIScrollView+VGParallaxHeader.h>
#import "UIView+KMKNib.h"
#import "TagDataLayer.h"
#import "TAGManager.h"
#import <UIAlertController+Blocks.h>

#import "RetailerDetailsButtonTableViewCell.h"
#import "ProductModel.h"
#import "ImageViewHeader.h"
#import "TealDetailsSectionHeaderView.h"
#import "TitleTableViewCell.h"
#import "ItemDetailsDescriptionTableViewCell.h"
#import "VendorDetailTableViewCell.h"
#import "ListingItemModel.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"
#import "ZLWPApiClient.h"
#import "ZLErrorHandler.h"


 #ifdef DEBUG
     #undef LOG_LEVEL_DEF
     #define LOG_LEVEL_DEF OverridenLogLevel
     static const int OverridenLogLevel = LOG_LEVEL_VERBOSE; // log level for current m-file.
 #endif


@interface ListingDetailsViewController ()

@end


@implementation ListingDetailsViewController

#pragma mark - UIViewController Override -

- (void)setProductModel:(ProductModel *)productModel
{
    [super setProductModel:productModel];
    NSParameterAssert([productModel isKindOfClass:[ListingItemModel class]] ||
                      [productModel isKindOfClass:[ProductModel class]]); // when moving from "Vendor details" model is kind of `ProductModel`
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TitleTableViewCell" bundle:nil] forCellReuseIdentifier:@"TitleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ItemDetailsDescriptionTableViewCell" bundle:nil] forCellReuseIdentifier:@"DescCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"VendorDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"VendorCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"RetailerDetailsButtonTableViewCell" bundle:nil] forCellReuseIdentifier:@"VendorReferenceCell"];
    
    self.tableView.estimatedRowHeight = 120.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 20.0)];;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Listing details"}];
    
    [self.tableView reloadData];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showVendor"]) {
        TermModel *vendorTerm = (TermModel *)sender;
        ZLRetailerDetailsTopTabBarViewController *controller = (ZLRetailerDetailsTopTabBarViewController *)[segue destinationViewController];
        controller.vendorTermId = vendorTerm.termId;
    }
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_productModel != nil) {
        return 3;
    }
    else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if (self.productModel != nil) {
        switch(section) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 2;
            default:
                NSAssert(NO, @"The app error!");
                return 0;
        }
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    switch (indexPath.section) {
        case 0: {
            TitleTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell" forIndexPath:indexPath];
            [titleCell.titleLabel setText:_productModel.title];
            cell = titleCell;
            break;
        }
        case 1: {
            if (indexPath.row == 0) {
                ItemDetailsDescriptionTableViewCell *shortDescCell = [tableView dequeueReusableCellWithIdentifier:@"DescCell" forIndexPath:indexPath];
                [shortDescCell.descLabel setText:_productModel.shortDesc];
                cell = shortDescCell;
            }
            else if (indexPath.row == 1) {
                TableCellWithLikeButton *cellWithLikeButton = [tableView dequeueReusableCellWithIdentifier:ZLTableCellWithLikeButtonIdentifier
                                                                                              forIndexPath:indexPath];
                [self configureCellWithLikeButton:cellWithLikeButton];
                cell = cellWithLikeButton;
            }
            break;
        }
        case 2: {
            switch(indexPath.row) {
                case 0: {
                    VendorDetailTableViewCell *vendorCell = [tableView dequeueReusableCellWithIdentifier:@"VendorCell" forIndexPath:indexPath];
                    [vendorCell.vendorImageView sd_setImageWithURL:[NSURL URLWithString:[_productModel.term.vendor.images.medium firstObject]]
                                                  placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
                    [vendorCell.nameLabel setText:_productModel.term.vendor.name];
                    [vendorCell.locationLabel setText:_productModel.term.vendor.displayAddress];
                    cell = vendorCell;
                    break;
                }
                case 1: {
                    RetailerDetailsButtonTableViewCell *referenceCell = [tableView dequeueReusableCellWithIdentifier:@"VendorReferenceCell"
                                                                                                        forIndexPath:indexPath];
                    [referenceCell.vendorButton addTarget:self
                                                   action:@selector(showVendor:)
                                         forControlEvents:UIControlEventTouchUpInside];
                    cell = referenceCell;
                    break;
                }
            }
            break;
        }
            
        default:
            NSAssert(NO, @"");
    }
    
    return cell;
}

#pragma mark <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 0;
        default:
            return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Dequeue the custom header cell
    TealDetailsSectionHeaderView *sectionHeaderView = [TealDetailsSectionHeaderView viewWithDefaultNib];
    
    switch (section) {
        case 0:
            [sectionHeaderView.textLabel setText:@"Title"];
            break;
        case 1:
            [sectionHeaderView.textLabel setText:@"Description"];
            break;
        case 2:
            [sectionHeaderView.textLabel setText:@"Offered by"];
            break;
        default: ;
    }
    
    return sectionHeaderView;
}

#pragma mark - Helpers -

#pragma mark Actions

- (void)showVendor:(UIButton *)button
{
    [self performSegueWithIdentifier:@"showVendor" sender:_productModel.term];
}

- (void)downloadProductModelWithId:(NSString *)productId
{
    [self moveToBusyState];
    
    // https://listings-dev.zoomlocal.com/wp-json/rest/v2/listing/
    // "authorized" variant of GET is used so that to get "is_liked" attribute in case if user is authenticated.
    [[ZLWPApiClient sharedClient] authorizedGET:[@"wp-json/rest/v2/listing/" stringByAppendingString:self.productId]
                                     parameters:nil
                                     completion:^(OVCResponse *response, NSError *error) {
                                       
                                       if (error == nil) {
                                           [self moveToNormalStateWithEmptySetFlag:NO];
                                           
                                           self.productModel = (ProductModel *)response.result;
                                           [self configureUIWithProductModel:self.productModel];
                                       }
                                       else {
                                           [self moveToErrorStateWithError:error];
                                           DDLogError(error.localizedDescription);
                                       }
                                   }];
}

@end

