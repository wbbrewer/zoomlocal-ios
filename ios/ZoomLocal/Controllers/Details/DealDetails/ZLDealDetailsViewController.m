//
//  ZLDealDetailsViewController.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 28/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLDealDetailsViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>

#import "ZLUserSessionManager.h"
#import "UIAlertController+Blocks.h"
#import "ZLWPApiClient.h"
#import "ZLErrorHandler.h"
#import "ZLLoginViewController.h"


@interface ZLDealDetailsViewController ()<ZLUserSessionManagerObserver, UIScrollViewDelegate>

@property (strong, nonatomic) ZLCoupon *coupon;
@property (weak, nonatomic) id<ZLDealDetailsViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *redeemButton;
@property (weak, nonatomic) IBOutlet UILabel *mustBeLoggedInLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidhtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelToSuperViewTrailingConstraint;

@end


@implementation ZLDealDetailsViewController {
    BOOL _wasRotated;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [[ZLUserSessionManager sharedManager] addUserSessionObserver:self];
    }
    return self;
}

- (void)dealloc
{
    [[ZLUserSessionManager sharedManager] removeUserSessionObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contentView.layer.cornerRadius = 6.0;
    
    [_imageView sd_setImageWithURL:[NSURL URLWithString:[self.coupon.images.large firstObject]]
                  placeholderImage:[UIImage imageNamed:@"launch_logo"]];
    
    [self adjustConstraintsOfContentView];
    [self configureUIAccordingLoginStatus];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [self rotateContentViewIfNeeded];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection
              withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
    
    if (newCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
        _wasRotated = NO;
        [self adjustConstraintsOfContentView];
    }
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [self rotateContentViewIfNeeded];
}

#pragma mark - Public -

- (void)showCoupon:(ZLCoupon *)coupon
withParentViewController:(UIViewController *)parentViewController
          delegate:(id<ZLDealDetailsViewControllerDelegate>)delegate
{
    self.delegate = delegate;
    self.coupon = coupon;
    
    [parentViewController addChildViewController:self];
    self.view.frame = parentViewController.view.bounds;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [parentViewController.view addSubview:self.view];
    [self didMoveToParentViewController:self];
    
    [self.closeButton addTarget:self
                         action:@selector(hideCoupon)
               forControlEvents:UIControlEventTouchUpInside];
    
    [self.loginButton addTarget:self
                         action:@selector(loginButtonOnCouponPressed)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    // Fade it in.
    self.view.alpha = 0.0f;
    [UIView animateWithDuration:0.4 animations:^{
        self.view.alpha = 1.0f;
    }];
}

- (void)hideCoupon
{
    [UIView animateWithDuration:0.2 animations:^{
        self.view.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
    [self.delegate dealDetailsViewControllerDidPressCloseButton:self];
}

#pragma mark <ZLUserSessionManagerObserver>

- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus
{
    if (loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        [self downloadCoupon];
    }
    else {
       [self configureUIAccordingLoginStatus];
    }
}

#pragma mark <UIScrollViewDelegate>

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

#pragma mark Actions

- (IBAction)redeemButtonPressed:(UIButton *)sender
{
    [UIAlertController showAlertInViewController:self
                                       withTitle:@"REDEEM COUPON"
                                         message:@"Present to Vendor. By clicking Accept You are granting this Coupon"
     @" to the user of this device."
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@[@"Accept"]
                                        tapBlock:^(UIAlertController * _Nonnull controller,
                                                   UIAlertAction * _Nonnull action,
                                                   NSInteger buttonIndex) {
                                            
                                            BOOL isAcceptButtonPressed = (buttonIndex != controller.cancelButtonIndex);
                                            if (isAcceptButtonPressed) {
                                                [self redeemCoupon];
                                            }
                                        }];
}

- (void)loginButtonOnCouponPressed
{
    [ZLLoginViewController presentLoginScreenWithDidDismissHandler:^{
        
    }];
}


#pragma mark Helpers

- (void)rotateContentViewIfNeeded
{
    if (self.traitCollection.verticalSizeClass != UIUserInterfaceSizeClassCompact &&
        self.traitCollection.horizontalSizeClass != UIUserInterfaceSizeClassCompact) {
        // iPad -- do nothing
        
        [self adjustConstraintsOfContentView];
    }
    else {
        if (self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
            self.contentView.transform = CGAffineTransformIdentity;
        }
        else {
            self.contentView.transform = CGAffineTransformMakeRotation(M_PI * 3/2);//270º
        }
        
        [self adjustConstraintsOfContentView];
    }
}

- (void)adjustConstraintsOfContentView
{
    if (CGAffineTransformIsIdentity(self.contentView.transform)) {
        self.contentViewWidhtConstraint.constant = self.view.frame.size.width - 40.0;
        self.contentViewHeightConstraint.constant = self.view.frame.size.height - 40.0;
    }
    else {
        self.contentViewWidhtConstraint.constant = self.view.frame.size.height - 40.0;
        self.contentViewHeightConstraint.constant = self.view.frame.size.width - 40.0;
    }
}

- (void)configureUIAccordingLoginStatus
{
    if ([ZLUserSessionManager sharedManager].loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        _mustBeLoggedInLabel.hidden = YES;
        _loginButton.hidden = YES;
        _redeemButton.hidden = YES;
        
        [self downloadCoupon];
    }
    else {
        _mustBeLoggedInLabel.hidden = NO;
        _loginButton.hidden = NO;
        _redeemButton.hidden = YES;
    }
}

- (void)expandLabelToTheEnd
{
    self.labelToSuperViewTrailingConstraint.priority = 999;
}

/**
 @param `redeemingDate` can be `nil`.
 */
- (void)updateUIAccordingRedeemingStatusOfCoupon:(NSDate *)redeemingDate
{
    if (redeemingDate != nil) {
        _mustBeLoggedInLabel.hidden = NO;
        _mustBeLoggedInLabel.alpha = 0.0;
        
        [UIView animateWithDuration:0.2 animations:^{
            _mustBeLoggedInLabel.alpha = 1.0;
            _redeemButton.alpha = 0.0;
            _loginButton.alpha = 0.0;
        } completion:^(BOOL finished) {
            _redeemButton.hidden = YES;
            _loginButton.hidden = YES;
            _redeemButton.alpha = 1.0;
            _loginButton.alpha = 1.0;
        }];

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MM/dd/yyyy";
        NSString *redeemingDateStr = [dateFormatter stringFromDate:redeemingDate];
        _mustBeLoggedInLabel.text = [NSString stringWithFormat:@"This coupon was redeemed on %@", redeemingDateStr];
    }
    else {
        _redeemButton.hidden = NO;
        _redeemButton.alpha = 0.0;
        _loginButton.hidden = YES;
        _mustBeLoggedInLabel.hidden = YES;
        [UIView animateWithDuration:0.2 animations:^{
            _redeemButton.alpha = 1.0;
            _mustBeLoggedInLabel.alpha = 1.0;
        } completion:^(BOOL finished) {
            _mustBeLoggedInLabel.hidden = YES;
        }];
    }
}

- (void)downloadCoupon
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *URLString = [@"wp-json/rest/v2/coupons/" stringByAppendingFormat:@"%@", self.coupon.listingId];
    
    [[ZLWPApiClient sharedClient] authorizedGET:URLString
                                     parameters:nil
                                     completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                          
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                          
                                          if (error == nil) {
                                              _coupon = response.result;
                                              [self updateUIAccordingRedeemingStatusOfCoupon:_coupon.redeemingDate];
                                          }
                                          else {
                                              DDLogError(@"Error: '%@'", error);
                                              [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                          }
                                      }];
}

#pragma mark Networking

- (void)redeemCoupon
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSParameterAssert(_coupon);
    NSString *URLString = [@"wp-json/rest/v2/coupons/" stringByAppendingFormat:@"%@", self.coupon.listingId];
    
    [[ZLWPApiClient sharedClient] authorizedPOST:URLString
                                      parameters:@{ @"redeemed" : @YES }
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:NO
                                      completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
                                          
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                          
                                          if (error == nil) {
                                              ZLCoupon *coupon = response.result;
                                              [self updateUIAccordingRedeemingStatusOfCoupon:coupon.redeemingDate];
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [self.delegate dealDetailsViewController:self didRedeemCoupon:coupon];
                                              });
                                          }
                                          else {
                                              DDLogError(@"Error: '%@'", error);
                                              [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                          }
                                      }];
}

@end
