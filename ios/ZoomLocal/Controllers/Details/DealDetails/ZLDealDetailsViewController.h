//
//  ZLDealDetailsViewController.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 28/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZLCoupon.h"

@class ZLDealDetailsViewController;

@protocol ZLDealDetailsViewControllerDelegate <NSObject>

- (void)dealDetailsViewControllerDidPressCloseButton:(ZLDealDetailsViewController *)vc;
- (void)dealDetailsViewController:(ZLDealDetailsViewController *)vc didRedeemCoupon:(ZLCoupon *)coupon;

@end


@interface ZLDealDetailsViewController : UIViewController

- (void)showCoupon:(ZLCoupon *)coupon
withParentViewController:(UIViewController *)vc
          delegate:(id<ZLDealDetailsViewControllerDelegate>)delegate;

- (void)hideCoupon;


@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end
