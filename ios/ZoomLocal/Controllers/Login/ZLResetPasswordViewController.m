//
//  ZLForgotPasswordViewController.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 23/08/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLResetPasswordViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import "UIButton+Indicator.h"
#import "UIAlertController+Blocks.h"

#import "ZLErrorHandler.h"


@interface ZLResetPasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendResetEmailButton;

@end


@implementation ZLResetPasswordViewController

- (void)dealloc
{
    // For debug purpose.
}

- (void)viewDidLoad
{
    NSParameterAssert(self.viewModel);
    [self bindViewModel];
}

#pragma mark - Private -

#pragma mark Actions

- (IBAction)closeBarButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Helpers

- (void)bindViewModel
{
    @weakify(self)

    self.emailTextField.text = self.viewModel.email;
    RAC(self.viewModel, email) = self.emailTextField.rac_textSignal;
    
    RAC(self.emailTextField, backgroundColor) =
    [self.viewModel.validEmailSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    
    self.sendResetEmailButton.rac_command = self.viewModel.sendResetEmailCommand;
    
    RAC([UIApplication sharedApplication], networkActivityIndicatorVisible) =
    self.viewModel.sendResetEmailCommand.executing;
    
    
    [self.viewModel.sendResetEmailCommand.executing
    subscribeNext:^(id x) {
        @strongify(self)
        BOOL isStarted = [(NSNumber *)x boolValue];
        BOOL isButtonEnabled = self.sendResetEmailButton.enabled;
        isStarted ? [self.sendResetEmailButton showIndicator] : [self.sendResetEmailButton hideIndicator];
        self.sendResetEmailButton.enabled = isButtonEnabled;
    }];
    
    RACSignal *resetPasswordEmailSentSource = [[self.viewModel.sendResetEmailCommand.executionSignals
                                                doNext:^(id x) {
                                                    @strongify(self)
                                                    [self.emailTextField resignFirstResponder];
                                                }]
                                               flattenMap:^RACStream *(RACSignal *subscribeSignal) {
                                                   return subscribeSignal;
                                               }];
    
    [resetPasswordEmailSentSource subscribeNext:^(NSNumber *success) {
        @strongify(self)
        if ([success boolValue]) {
            [self showSendResetEmailSuccessMessage];
        }
    }];
    
    [self.viewModel.sendResetEmailCommand.errors subscribeNext:^(NSError *error) {
        [[ZLErrorHandler sharedErrorHandler] handleError:error];
    }];
}

- (void)showSendResetEmailSuccessMessage
{
    NSString *message = [NSString stringWithFormat:@"An email has been sent to %@.", self.viewModel.email];
    
    [UIAlertController showAlertInViewController:self
                                       withTitle:@"Success!"
                                         message:message
                               cancelButtonTitle:@"OK"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                            [self dismissViewControllerAnimated:YES completion:nil];
                                        }];
}

@end
