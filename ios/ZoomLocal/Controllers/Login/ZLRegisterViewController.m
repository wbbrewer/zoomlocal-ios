//
//  ZLRegisterViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 08/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLRegisterViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import "NSString+KMKValidation.h"
#import "NSString+KMKFormat.h"
#import "UIButton+Indicator.h"
#import "UIAlertController+Blocks.h"
#import "IQKeyboardManager.h"

#import "ZLWPApiClient.h"
#import "ZLErrorHandler.h"


@interface ZLRegisterViewController ()

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

// email for credentials when invoking delegate. User can change text fields while request is being performed.
@property (strong, nonatomic) NSString *email;
// password for credentials when invoking delegate. User can change text fields while request is being performed.
@property (strong, nonatomic) NSString *password;

@end


@implementation ZLRegisterViewController {
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByPosition];
    
#if defined(yklishevich)
    self.firstNameTextField.text = @"Eugne";
    self.lastNameTextField.text = @"Klishevich";
    self.emailTextField.text = @"eklishevich@gmail.com";
    self.zipTextField.text = @"11111";
    self.passwordTextField.text = @"qwaszx";
#endif
    
    RACSignal *validFirstNameSignal =
    [self.firstNameTextField.rac_textSignal
     map:^id(NSString *text) {
         return @(text.length > 0);
     }];
    
    RAC(self.firstNameTextField, backgroundColor) =
    [validFirstNameSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    RACSignal *validLastNameSignal =
    [self.lastNameTextField.rac_textSignal
     map:^id(NSString *text) {
         return @(text.length > 0);
     }];
    
    RAC(self.lastNameTextField, backgroundColor) =
    [validLastNameSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    RACSignal *validEmailSignal =
    [self.emailTextField.rac_textSignal
     map:^id(NSString *text) {
         return @([text isValidEmail]);
     }];
    
    RAC(self.emailTextField, backgroundColor) =
    [validEmailSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    RACSignal *validZipSignal =
    [self.zipTextField.rac_textSignal
     map:^id(NSString *text) {
         BOOL valid = [text isValid_USA_ZipCode];
         return @(valid);
     }];
    
    RAC(self.zipTextField, backgroundColor) =
    [validZipSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    RACSignal *validPasswordSignal =
    [self.passwordTextField.rac_textSignal
     map:^id(NSString *text) {
         return @(text.length > 0);
     }];
    
    RAC(self.passwordTextField, backgroundColor) =
    [validPasswordSignal
     map:^id(NSNumber *textValid) {
         return [textValid boolValue] ? [UIColor whiteColor] : [UIColor yellowColor];
     }];
    
    RACSignal *submitButtonActiveSignal =
    [RACSignal combineLatest:@[validFirstNameSignal, validLastNameSignal, validZipSignal, validEmailSignal, validPasswordSignal]
                      reduce:^id(NSNumber *firstNameValid, NSNumber *lastNameValid, NSNumber *zipValid, NSNumber *emailValid, NSNumber *passwordValid) {
                          return @(
                          [firstNameValid boolValue] &&
                          [lastNameValid boolValue] &&
                          [zipValid boolValue] &&
                          [emailValid boolValue] &&
                          [passwordValid boolValue]
                          );
                      }];
    
    @weakify(self)
    [submitButtonActiveSignal subscribeNext:^(NSNumber *submitButtonActive) {
        @strongify(self)
        self.submitButton.enabled = [submitButtonActive boolValue];
    }];
    
    [[[[self.submitButton
        rac_signalForControlEvents:UIControlEventTouchUpInside]
       doNext:^(id x) {
           [(UIButton *)x showIndicator];
       } ]
      flattenMap:^RACStream *(id value){
          @strongify(self)
          return [self signUpSignal];
      }]
     subscribeNext:^(id x) {
         @strongify(self)
         [self.submitButton hideIndicator];
         
         NSString *logMsg = [NSString stringWithFormat:@"Sign in result: %@", x];
         DDLogVerbose([logMsg unescapeNewLine]);
         
         OVCResponse *response = x[@"response"];
         NSError *error = x[@"error"];
         
         if (error == nil) {
             NSDictionary *credentialsDict = @{ @"email" : self.email, @"password" : self.password };
             [self.delegate registerViewController:self didRegisterUserWithCredentials:credentialsDict];
         }
         else {
             DDLogError(@"Error: '%@'", error);
             if (response != nil) {
                 NSString *errorMsg = response.result[@"message"];
                 [[ZLErrorHandler sharedErrorHandler] showErrorAlertWithMessage:errorMsg];
             }
             else {
                 [[ZLErrorHandler sharedErrorHandler] handleError:error];
             }
         }
     }];
}

#pragma mark - Private -

-(RACSignal *)signUpSignal
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        self.email = self.emailTextField.text;
        self.password = self.passwordTextField.text;
        
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.firstNameTextField.text, @"first_name",
                                    self.lastNameTextField.text, @"last_name",
                                    self.email, @"user_email",
                                    self.password, @"user_zip",
                                    self.passwordTextField.text, @"user_pass",
                                    nil];
        
        [[ZLWPApiClient sharedClient] POST:@"wp-json/rest/v2/users"
                                parameters:parameters
                                completion:^(OVCResponse *response, NSError *error) {
                                    
                                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                                    [dict setValue:response forKey:@"response"];
                                    [dict setValue:error forKey:@"error"];
                                    
                                    [subscriber sendNext:dict];
                                    [subscriber sendCompleted];
                                }];
        return nil;
    }];
}

#pragma mark Actions

- (IBAction)formTapped:(UITapGestureRecognizer *)sender
{
    // Hide keyboard
    [self.view endEditing:YES];
}

@end
