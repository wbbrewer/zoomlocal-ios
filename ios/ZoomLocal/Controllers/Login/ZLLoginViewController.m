//
//  ZLLoginViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLLoginViewController.h"

#import <MZFormSheetPresentationViewController.h>
#import "UIButton+Indicator.h"
#import "NSString+KMKValidation.h"
#import <UIAlertController+Blocks.h>

#import "ZLUserSessionManager.h"
#import "ZLErrorHandler.h"
#import "ZLRegisterViewController.h"
#import "ZLResetPasswordViewController.h"
#import "ZLResetPasswordViewModel.h"


@interface ZLLoginViewController ()<UITextFieldDelegate, ZLRegisterViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@property (strong, nonatomic) ZLResetPasswordViewModel *resetPasswordViewModel;

@end


@implementation ZLLoginViewController

#pragma mark - Public -

+ (void)presentLoginScreenWithDidDismissHandler:(void (^)())dismissHandler
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"LoginNavigationController"];
    MZFormSheetPresentationViewController *formSheetController = [[MZFormSheetPresentationViewController alloc] initWithContentViewController:navigationController];
    formSheetController.didDismissContentViewControllerHandler = ^(UIViewController * __nonnull contentViewController) {
        dismissHandler();
    };
    
    //formSheetController.presentationController.contentViewSize = CGSizeMake(vc.view.bounds.size.width * .8, vc.view.bounds.size.height * .8);
    UITabBarController *tabBarController = (UITabBarController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    NSParameterAssert([tabBarController isKindOfClass:[UITabBarController class]]);
    
    if (tabBarController.presentedViewController == nil) {
        // if there is modally presented view controller, then attempt to present some controller on tab bar controller results in only
        // message to console: 'Attempt to present controller on tab bar controller whose view is not in the window hierarchy!'
        [tabBarController presentViewController:formSheetController animated:YES completion:nil];
    }
    else {
        [tabBarController.presentedViewController presentViewController:formSheetController animated:YES completion:nil];
    }
}

#pragma mark - Ptotected -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if defined(yklishevich)
    _emailTextField.text = @"eklishevich@gmail.com"; _passwordTextField.text = @"qwaszx";
    self.loginButton.enabled = YES;
#endif

    CGSize loginViewControllerSize = CGSizeMake(0.8 * self.view.bounds.size.width, 0.8 * self.view.bounds.size.height);
    self.mz_formSheetPresentingPresentationController.presentationController.contentViewSize = loginViewControllerSize;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showRegisterVC"]) {
        ZLRegisterViewController *registerVC = (ZLRegisterViewController *)segue.destinationViewController;
        registerVC.delegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"showResetPasswordViewController"]) {
        UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
        
        // When using `UIModalPresentationCurrentContext` for presentation style of `ZLResetPasswordViewController`
        // as user dismisses `ZLResetPasswordViewController` view of navigation controller of login screen is inserted as sibling of
        // `MZFormSheetPresentationViewControllerCustomView` (that is of the view of `MZFormSheetPresentationViewController`)
        // insted of as a child of `MZFormSheetPresentationViewControllerCustomView`. This leads login screen to being presented
        // over full screen and to the crash of app after second dismissing the `MZFormSheetPresentationViewControllerCustomView`.
        // So `definesPresentationContext` of `ZLResetPasswordViewController`'s navigation controller is set to `NO` so that
        // `MZFormSheetPresentationViewControllerCustomView` itself would be replaced with login navigation controller's view.
        self.navigationController.definesPresentationContext = NO;
        ZLResetPasswordViewController *resetVC = (ZLResetPasswordViewController *)nc.topViewController;
        self.resetPasswordViewModel = [[ZLResetPasswordViewModel alloc] init];
        resetVC.viewModel = self.resetPasswordViewModel;
    }
}

#pragma mark <UITextFieldDelegate>

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    else if (textField == _passwordTextField) {
        [self startLoginWithEmail:_emailTextField.text password:_passwordTextField.text];
    }
    
    return YES;
}

#pragma mark - Actions

- (IBAction)leftBarButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginButtonPressed:(id)sender
{
    [self startLoginWithEmail:_emailTextField.text password:_passwordTextField.text];
}

- (IBAction)registerButtonPressed:(UIButton *)sender
{
    
}

- (IBAction)scrollViewTapped:(id)sender
{
    // Hide keyboard
    [self.view endEditing:YES];
}

- (IBAction)credentialsValueChangedEvent:(UITextField *)sender
{
    BOOL isLoginEnabled = [_emailTextField.text isValidEmail] && [self isPasswordValid:_passwordTextField.text];
    self.loginButton.enabled = isLoginEnabled;
}

- (IBAction)forgotPasswordButtonPressed:(id)sender
{
    
}

#pragma mark <ZLRegisterViewControllerDelegate>

- (void)registerViewController:(ZLRegisterViewController *)registerViewController
didRegisterUserWithCredentials:(NSDictionary *)credentials
{
    _emailTextField.text = credentials[@"email"];
    _passwordTextField.text = credentials[@"password"];
    [self startLoginWithEmail:credentials[@"email"] password:credentials[@"password"]];
    [self.navigationController popToViewController:self animated:YES];
}

#pragma mark - Helpers -

- (BOOL)isPasswordValid:(NSString *)password
{
    return (password.length >= 1);
}

- (void)startLoginWithEmail:(NSString *)email password:(NSString *)password
{
    if ([email isValidEmail]) {
        [self.loginButton showIndicator];
        self.signUpButton.enabled = NO;
        [self.view endEditing:YES];
        
        [[ZLUserSessionManager sharedManager] loginWithEmail:email
                                                    password:password
                                                  completion:^(NSError *error) {
                                                      
                                                      [self.loginButton hideIndicator];
                                                      self.signUpButton.enabled = YES;
                                                      
                                                      if (error == nil) {
                                                          [self dismissViewControllerAnimated:YES completion:nil];
                                                      }
                                                      else {
                                                          DDLogError(@"Error: '%@'", error);
                                                          if ([error.domain isEqualToString:ZLUserSessionManagerErrorDomain] &&
                                                              error.code == ZLUserSessionManagerErrorInvalidCredentials) {
                                                              [[ZLErrorHandler sharedErrorHandler] showErrorAlertWithMessage:error.localizedDescription];
                                                          }
                                                          [[ZLErrorHandler sharedErrorHandler] handleError:error];
                                                      }
                                                  }];
    }
    else {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@"Warning"
                                             message:@"Invalid E-mail"
                                   cancelButtonTitle:@"OK"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:nil
                                            tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                                ;
                                            }];
    }
}

@end
