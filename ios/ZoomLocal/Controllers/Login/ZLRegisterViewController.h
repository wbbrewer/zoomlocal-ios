//
//  ZLRegisterViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 08/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZLRegisterViewControllerDelegate;


@interface ZLRegisterViewController : UIViewController

@property (weak, nonatomic) id<ZLRegisterViewControllerDelegate> delegate;

@end


@protocol ZLRegisterViewControllerDelegate <NSObject>

- (void)registerViewController:(ZLRegisterViewController *)registerViewController
didRegisterUserWithCredentials:(NSDictionary *)credentials;

@end