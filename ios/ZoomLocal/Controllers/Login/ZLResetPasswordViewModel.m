//
//  ZLResetPasswordViewModel.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 23/08/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLResetPasswordViewModel.h"

#import "NSString+KMKValidation.h"
#import <ReactiveCocoa/RACEXTScope.h>

#import "ZLWPApiClient.h"


@implementation ZLResetPasswordViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    @weakify(self)

    self.email = @"";
#if defined(yklishevich)
    self.email = @"eklishevich@gmail.com";
#endif
    
    self.validEmailSignal =
    [[RACObserve(self, email)
      map:^id(NSString *text) {
          return @([text isValidEmail]);
      }]
     distinctUntilChanged];
    
    self.sendResetEmailCommand =
    [[RACCommand alloc] initWithEnabled:self.validEmailSignal
                            signalBlock:^RACSignal *(id input) {
                                @strongify(self)
                                return [self sendResetEmailSignal];
                            }];
}

/**
 Returns `YES` value if request was performed successfully. And `NO` otherwise.
 */
- (RACSignal *)sendResetEmailSignal
{
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.email, @"email",
                                    nil];
        
        [[ZLWPApiClient sharedClient] POST:@"wp-json/rest/v2/password_reset"
                                parameters:parameters
                                completion:^(OVCResponse *response, NSError *error) {
                                    
                                    if (error == nil) {
                                        [subscriber sendNext:@(YES)];
                                        [subscriber sendCompleted];
                                    }
                                    else {
                                        [subscriber sendError:error];
                                    }
                                }];
        return nil;
    }];
    
    return signal;
}

@end
