//
//  ZLResetPasswordViewModel.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 23/08/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <ReactiveCocoa/ReactiveCocoa.h>


@interface ZLResetPasswordViewModel : NSObject

@property (strong, nonatomic) NSString *email;

@property (strong, nonatomic) RACSignal *validEmailSignal;
@property (strong, nonatomic) RACCommand *sendResetEmailCommand;

@end
