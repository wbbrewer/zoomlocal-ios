//
//  ZLForgotPasswordViewController.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 23/08/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZLResetPasswordViewModel.h"


@interface ZLResetPasswordViewController : UIViewController

@property (weak, nonatomic) ZLResetPasswordViewModel *viewModel;

@end
