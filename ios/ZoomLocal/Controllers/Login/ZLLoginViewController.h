//
//  ZLLoginViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/24/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZLLoginViewController : UIViewController

/**
 Used to present screen modally.
 */
+ (void)presentLoginScreenWithDidDismissHandler:(void (^)())dismissHandler;

@end
