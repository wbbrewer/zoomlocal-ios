//
//  ZLEventsViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEventsViewController.h"

#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>
#import <libextobjc/extobjc.h>
#import "TagDataLayer.h"
#import "TAGManager.h"

#import "ZLWPApiClient.h"
#import "FilterByDistanceTableViewController.h"
#import "ZLConstants.h"
#import "ProductModel.h"
#import "ZLSettingsManager.h"
#import "LocationService.h"
#import "FilterByDistanceContainerViewController.h"
#import "ZLProductCollectionViewCell.h"
#import "ZLItemDetailsViewController.h"
#import "ZLListingsViewController.h"
#import "ZLItemsCollectionView.h"


static NSString *const kListingsCellReuseIdentifier = @"ListingCell";
static NSString *const kStoryboardSegueShowItemDetails = @"showItemDetails";

static const NSInteger kInitialListPage = 1; // paging start from 1, depends on your api

enum {
    ZLEventsTableSectionEvents = 0
};


@interface ZLEventsViewController () <ZLItemsCollectionViewDataSource,
                                    ZLItemsCollectionViewDelegate,
                                    UITraitEnvironment,
                                    FilterByDistanceTableViewControllerDelegate,
                                    LocationServiceDelegate>

@property (nonatomic, strong) NSMutableArray<ProductModel *> *events;

// to keep track of what is the next page to load
@property (nonatomic, assign) int currentPage;
@property (weak, nonatomic) IBOutlet ZLItemsCollectionView *itemsCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterByDistanceHeightConstraint;

@end


@implementation ZLEventsViewController {
    FilterByDistanceContainerViewController *_filterByDistanceContainerVC;
    float _radiusOfViewingInMi;
    BOOL _isDataSynchronizedWithLocation;
    NSString *_itemId;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [[LocationService sharedInstance] addDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[LocationService sharedInstance] removeDelegate:self];
}

#pragma mark - Public -

- (void)goToItemDetailsScreenWithItemId:(NSString *)itemId
{
    _itemId = itemId;
    [self performSegueWithIdentifier:kStoryboardSegueShowItemDetails sender:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    self.itemsCollectionView.underlyingScrollView.emptyDataSetSource = self; // imlemented in base class
    self.itemsCollectionView.underlyingScrollView.emptyDataSetDelegate = self; // implemented in base class
    
    [self.itemsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ZLProductCollectionViewCell class]) bundle:nil]
               forCellWithReuseIdentifier:kListingsCellReuseIdentifier];

    
    // initialize
    _events = [NSMutableArray array];
    _currentPage = kInitialListPage;
    
    // This is required to get pull-to-refresh.
    self.itemsCollectionView.underlyingScrollView.alwaysBounceVertical = YES;
    
    __weak typeof(self) weakSelf = self;
    
    // refresh new data when pull the table list
    [self.itemsCollectionView.underlyingScrollView addPullToRefreshWithActionHandler:^{
        weakSelf.currentPage = kInitialListPage; // reset the page
        [weakSelf.events removeAllObjects]; // remove all data
        [weakSelf.itemsCollectionView reloadData]; // before load new content, clear the existing table list
        [weakSelf fetch]; // load new data
        [weakSelf.itemsCollectionView.underlyingScrollView.pullToRefreshView stopAnimating]; // clear the animation
        
        // once refresh, allow the infinite scroll again
        weakSelf.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = YES;
    }];
    
    // load more content when scroll to the bottom most
    [self.itemsCollectionView.underlyingScrollView addInfiniteScrollingWithActionHandler:^{
        // WORDAROUND: if number of items is not sufficient to cover the whole screen then this block is
        // invoked when user pulls scroll view down (not up as must be) so that to refresh list. This is bug in library.
        if (self.itemsCollectionView.underlyingScrollView.contentOffset.y > 0.0) {
            [weakSelf fetch];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];    
    [self updateListIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Events"}];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Update the view.
    [self.itemsCollectionView reloadData];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection
              withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    //DDLogDebug(@"Trait collection = %@", newCollection);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kStoryboardSegueShowItemDetails]) { // cell of table was tapped
        if ([sender isKindOfClass:[ZLProductCollectionViewCell class]]) {
            ZLProductCollectionViewCell *cell = (ZLProductCollectionViewCell *)sender;
            ProductModel *productModel = cell.productModel;
            ZLItemDetailsViewController *controller = (ZLItemDetailsViewController *)[segue destinationViewController];
            controller.productModel = productModel;
        }
        else if ([sender isKindOfClass:[self class]]) { // handling deep link
            ZLItemDetailsViewController *controller = (ZLItemDetailsViewController *)[segue destinationViewController];
            controller.productId = _itemId;
        }
    }
    else if ([[segue identifier] isEqualToString:@"EmbedFilterByDistance"]) {
        _filterByDistanceContainerVC = (FilterByDistanceContainerViewController *)[segue destinationViewController];
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
}

#pragma mark <ZLItemsCollectionViewDataSource>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView
{
    return _events.count;
}

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row
{
    UICollectionViewCell *cell = nil;
    ZLProductCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:kListingsCellReuseIdentifier
                                                                                       forRow:row];
    aCell.productModel = _events[row];
    cell = aCell;
    
    return cell;
}

#pragma mark <ZLItemsCollectionViewDelegate>

- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row
{
    UICollectionViewCell *cell = [collectionView cellForItemAtRow:row];
    [self performSegueWithIdentifier:kStoryboardSegueShowItemDetails sender:cell];
}

#pragma mark <FilterByDistanceTableViewControllerDelegate>

- (void)filterByDistanceTableViewControllerDidSaveSettings:(FilterByDistanceTableViewController *)viewController
{
    if (_radiusOfViewingInMi != viewController.radiusInMiles) {
        _radiusOfViewingInMi = viewController.radiusInMiles;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = viewController.radiusInMiles;
        [ZLSettingsManager sharedManager].radiusOfViewingInMi = viewController.radiusInMiles;
        
        [self updateList];
    }
}

#pragma mark <LocationServiceDelegate>

- (void)locationService:(LocationService *)locationService didUpdateLocation:(CLLocation *)location
{
    _isDataSynchronizedWithLocation = NO;
}

#pragma mark Actions

- (IBAction)filterByDistancePressed:(UIStoryboardSegue *)unwindSegue
{
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterByDistanceNavigationCntr"];
    FilterByDistanceTableViewController *filteringByDistanceVC = (FilterByDistanceTableViewController *)navigationController.topViewController;
    filteringByDistanceVC.delegate = self;
    FilterByDistanceContainerViewController *containerVC = (FilterByDistanceContainerViewController *)unwindSegue.sourceViewController;
    filteringByDistanceVC.radiusInMiles = containerVC.radiusOfSearchInMiles;
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark - Helpers -

- (void)updateListIfNeeded
{
    BOOL isDataSynchronizedWithRadiusOfViewing = YES;
    
    if (_radiusOfViewingInMi != [ZLSettingsManager sharedManager].radiusOfViewingInMi) {
        isDataSynchronizedWithRadiusOfViewing = NO;
        _radiusOfViewingInMi = [ZLSettingsManager sharedManager].radiusOfViewingInMi;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
    
    if (!_isDataSynchronizedWithLocation || !isDataSynchronizedWithRadiusOfViewing) {
        [self updateList];
    }
}


- (void)updateList
{
    self.currentPage = kInitialListPage;
    [self.events removeAllObjects];
    [self fetch];
}

- (void)showFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         self.filterByDistanceHeightConstraint.constant = ZLFilterByDistanceHeight;
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         // Bottom constraint has priority 999 to avoid warning when setting height of the whole view to 0.0.
                         self.filterByDistanceHeightConstraint.constant = 0.0;
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark Downloading

- (void)fetch
{
    //Fetch the events
    self.theResponse = nil;
    self.theError = nil;
    self.busy = YES;
    
    [self.itemsCollectionView reloadData];
    [self hideFilterByDistanceAnimated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:@"yes" forKey:@"featured"];
    [parameters setObject:@(_currentPage) forKey:@"page"];
    [parameters setObject:@(ZLNumberOfItemsPerPageInServerResponse) forKey:@"per_page"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.latitude) forKey:@"latitude"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.longitude) forKey:@"longitude"];
    [parameters setObject:@(_radiusOfViewingInMi) forKey:@"radius"];
    
     _isDataSynchronizedWithLocation = YES;
    
    //https://listings-dev.zoomlocal.com/wp-json/rest/v1/event?featured=yes&page=1
    [[ZLWPApiClient sharedClient] GET:@"wp-json/rest/v2/event"
                         parameters:parameters
                         completion:^(OVCResponse *response, NSError *error) {
                             
                             // ALWAYS clear the animation!
                             [self.itemsCollectionView.underlyingScrollView.infiniteScrollingView stopAnimating];
                             
                             // disable infinite scroll when we reach the end.
                             if ([response.result count] < ZLNumberOfItemsPerPageInServerResponse) {
                                 self.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                             }
                             
                             self.theResponse = response;
                             self.theError = error;
                             self.busy = NO;
                             
                             if (!error) {
                                 [self showFilterByDistanceAnimated:YES];
                                 _currentPage++; // increase the page number
                                 //            int currentRow = [_events count]; // keep the the index of last row before add new items into the list
                                 
                                 // store the items into the existing list
                                 for (id obj in response.result) {
                                     [_events addObject:obj];
                                 }
                             } else {
                                 DDLogError(error.localizedDescription);
                                 self.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                             }
                             
                             // Update the view.
                             [self.itemsCollectionView reloadData];
                         }];
}

@end
