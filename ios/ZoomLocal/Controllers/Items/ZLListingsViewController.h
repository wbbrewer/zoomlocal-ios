//
//  ZLListingsViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/14/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"

@class TermModel;

extern const CGFloat ZLFilterByDistanceHeight;


@interface ZLListingsViewController : ZLEmptyDataSetViewController

@property (strong, nonatomic) TermModel *term;

// Used for tab "Followed", where only items of vendors being followed by user should be displayed.
// Default `NO`.
@property (assign, nonatomic) BOOL showOnlyItemsOfFollowedVendors;

/**
 Allows to show programatically screen with details concerning specific item.
 Used mainly for deep linking.
 */
- (void)goToItemDetailsScreenWithItemId:(NSString *)itemId;

@end
