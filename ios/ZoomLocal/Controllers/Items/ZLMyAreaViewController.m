//
//  ZLMyAreaViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLMyAreaViewController.h"

#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <libextobjc/extobjc.h>
#import "TagDataLayer.h"
#import "TAGManager.h"

#import "ZLWPApiClient.h"
#import "FilterByDistanceTableViewController.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"
#import "ZLSettingsManager.h"
#import "LocationService.h"
#import "FilterByDistanceContainerViewController.h"
#import "TermModel.h"
#import "VendorCollectionViewCell.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"
#import "ZLVendorsByMapViewController.h"
#import "ZLConstants.h"
#import "ZLListingsViewController.h"


static int kInitialListPage = 1; // paging start from 1, depends on your api
static NSString *const kFilterByDistanceCellReuseIdentifier = @"FilterByDistanceCell";
static NSString *const reuseIdentifier = @"VendorCell";

static NSString *const kStoryboardSegueShowVendorDetails = @"showVendor";

static double kResponsiveCellHeightCompact = 128.0;
static double kResponsiveCellHeightRegular = 156.0;

enum {
    ZLMyAreaCollectionSectionVendors = 0
};


@interface ZLMyAreaViewController () <FilterByDistanceTableViewControllerDelegate,
                                    UICollectionViewDataSource,
                                    UICollectionViewDelegate,
                                    UICollectionViewDelegateFlowLayout,
                                    UITraitEnvironment,
                                    LocationServiceDelegate>

@property (nonatomic, strong) NSMutableArray<TermModel *> *vendors;
// To keep track of what is the next page to load
@property (nonatomic, assign) int currentPage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterByDistanceHeightConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

// TODO: (YKL) replace UICollectionView with ZLIemsCollectionView
@implementation ZLMyAreaViewController
{
    FilterByDistanceContainerViewController *_filterByDistanceContainerVC;
    float _radiusOfViewingInMi;
    BOOL _isDataSynchronizedWithLocation;
    NSString *_vendorId;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [[LocationService sharedInstance] addDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[LocationService sharedInstance] removeDelegate:self];
}

#pragma mark - Public -

- (void)goToVendorDetailsScreenWithId:(NSString *)vendorId
{
    _vendorId = vendorId;
    [self performSegueWithIdentifier:kStoryboardSegueShowVendorDetails sender:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
        self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular)
    {
        self.collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4); // top, left, bottom, right
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 4;
        self.collectionView.collectionViewLayout = flow;
    }
    else {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 1;
        self.collectionView.collectionViewLayout = flow;
    }
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:@"VendorCollectionViewCell" bundle:nil]
          forCellWithReuseIdentifier:reuseIdentifier];

    // initialize
    _vendors = [NSMutableArray array];
    _currentPage = kInitialListPage;
    
    // This is required to get pull-to-refresh.
    self.collectionView.alwaysBounceVertical = YES;
    
    __weak typeof(self) weakSelf = self;
    
    // refresh new data when pull the table list
    [self.collectionView addPullToRefreshWithActionHandler:^{
        weakSelf.currentPage = kInitialListPage; // reset the page
        [weakSelf.vendors removeAllObjects]; // remove all data
        [weakSelf.collectionView reloadData]; // before load new content, clear the existing table list
        [weakSelf fetch]; // load new data
        [weakSelf.collectionView.pullToRefreshView stopAnimating]; // clear the animation
        
        // once refresh, allow the infinite scroll again
        weakSelf.collectionView.showsInfiniteScrolling = YES;
    }];
    
    // load more content when scroll to the bottom most
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        // WORDAROUND: if number of items is not sufficient to cover the whole screen then this block is
        // invoked when user pulls scroll view down (not up as must be) so that to refresh list. This is bug in library.
        if (self.collectionView.contentOffset.y > 0.0) {
            [weakSelf fetch];
        }
    }];
    
    if (self.productsCategory != nil) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close"
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(onCloseBarButtonPressed:)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateListIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"My Area"}];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Update the view.
    [self.collectionView reloadData];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection
              withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    DDLogDebug(@"Trait collection = %@", newCollection);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:kStoryboardSegueShowVendorDetails]) {
        if ([sender isKindOfClass:[VendorCollectionViewCell class]]) {
            VendorCollectionViewCell *cell = (VendorCollectionViewCell *)sender;
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
            TermModel *termModel = _vendors[indexPath.row];
            
            ZLRetailerDetailsTopTabBarViewController *controller = (ZLRetailerDetailsTopTabBarViewController *)[segue destinationViewController];
            controller.vendorTermId = termModel.termId;
        }
    }
    else if ([[segue identifier] isEqualToString:@"EmbedFilterByDistance"]) {
        _filterByDistanceContainerVC = (FilterByDistanceContainerViewController *)[segue destinationViewController];
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case ZLMyAreaCollectionSectionVendors:
            return _vendors.count;
            
        default:
            NSAssert(NO, @"Unknown section!");
            return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    
    switch (indexPath.section) {
        case ZLMyAreaCollectionSectionVendors:
        {
            VendorCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                                        forIndexPath:indexPath];
            
            // Configure the cell
            TermModel *model = self.vendors[indexPath.row];
            
            [aCell.vendorImageView sd_setImageWithURL:[NSURL URLWithString:[model.vendor.images.medium firstObject]]
                                    placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
            [aCell.vendorNameLabel setText: model.name];
            [aCell.vendorAddressLabel setText: model.vendor.address1];
            [aCell.vendorLocationLabel setText:[NSString stringWithFormat:@"%@, %@", model.vendor.city, model.vendor.state]];
            CLLocationDistance distance = [[LocationService sharedInstance].currentLocation distanceFromLocation:model.vendor.location] * 0.000621371192;
            [aCell.vendorDistanceLabel setText:[NSString stringWithFormat:@"%0.1f %@", distance, @"miles"]];
            cell = aCell;
            break;
        }
        default:
            NSAssert(NO, @"Unknown section!");
            break;
    }
    
    return cell;
}


#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = CGSizeZero;
    
    switch (indexPath.section) {
        case ZLMyAreaCollectionSectionVendors:
        {
            // iPad
            if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular)
            {
                cellSize.height = kResponsiveCellHeightRegular;
                cellSize.width = (self.view.bounds.size.width/2) - 6;
            }
            else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact)
            {
                cellSize.height = kResponsiveCellHeightRegular;
                cellSize.width = (self.view.bounds.size.width/2) - 0.5;
            }
            else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact)
            {
                cellSize.height = kResponsiveCellHeightCompact;
                cellSize.width = self.view.bounds.size.width;
            }
            else
            {
                cellSize.height = kResponsiveCellHeightCompact;
                cellSize.width = self.view.bounds.size.width;
            }
            
            break;
        }
        default:
            NSAssert(NO, @"Unknown section!");
            break;
    }
    
    return cellSize;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case ZLMyAreaCollectionSectionVendors:
        {
            UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
            [self performSegueWithIdentifier:kStoryboardSegueShowVendorDetails sender:cell];
            break;
        }
        default:
            NSAssert(NO, @"Unknown section!");
            break;
    }
}

#pragma mark <FilterByDistanceTableViewControllerDelegate>

- (void)filterByDistanceTableViewControllerDidSaveSettings:(FilterByDistanceTableViewController *)viewController
{
    if (_radiusOfViewingInMi != viewController.radiusInMiles) {
        _radiusOfViewingInMi = viewController.radiusInMiles;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = viewController.radiusInMiles;
        [ZLSettingsManager sharedManager].radiusOfViewingInMi = viewController.radiusInMiles;
        
        [self updateList];
    }
}

#pragma mark <LocationServiceDelegate>

- (void)locationService:(LocationService *)locationService didUpdateLocation:(CLLocation *)location
{
    _isDataSynchronizedWithLocation = NO;
}

#pragma mark Actions

- (IBAction)filterByDistancePressed:(UIStoryboardSegue *)unwindSegue
{
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterByDistanceNavigationCntr"];
    FilterByDistanceTableViewController *filteringByDistanceVC = (FilterByDistanceTableViewController *)navigationController.topViewController;
    filteringByDistanceVC.delegate = self;
    FilterByDistanceContainerViewController *containerVC = (FilterByDistanceContainerViewController *)unwindSegue.sourceViewController;
    filteringByDistanceVC.radiusInMiles = containerVC.radiusOfSearchInMiles;
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (void)onCloseBarButtonPressed:(UIBarButtonItem *)item
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Helpers -

- (void)updateListIfNeeded
{
    BOOL isDataSynchronizedWithRadiusOfViewing = YES;
    
    if (_radiusOfViewingInMi != [ZLSettingsManager sharedManager].radiusOfViewingInMi) {
        isDataSynchronizedWithRadiusOfViewing = NO;
        _radiusOfViewingInMi = [ZLSettingsManager sharedManager].radiusOfViewingInMi;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
    
    if (!_isDataSynchronizedWithLocation || !isDataSynchronizedWithRadiusOfViewing) {
        [self updateList];
    }
}

- (void)updateList
{
    self.currentPage = kInitialListPage;
    [self.vendors removeAllObjects];
    self.collectionView.showsInfiniteScrolling = YES;
    [self fetch];
}

- (void)showFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         self.filterByDistanceHeightConstraint.constant = ZLFilterByDistanceHeight;
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         // Bottom constraint has priority 999 to avoid warning when setting height of the whole view to 0.0.
                         self.filterByDistanceHeightConstraint.constant = 0.0;
                         [self.view layoutIfNeeded];
                     }];
}


#pragma mark Downloading

- (void)fetch
{
    //Fetch the categories
    self.theResponse = nil;
    self.theError = nil;
    self.busy = YES;
    
    [self.collectionView reloadData];
    [self hideFilterByDistanceAnimated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:@"true" forKey:@"hide_empty"];
    [parameters setObject:@(_currentPage) forKey:@"page"];
    [parameters setObject:@(ZLNumberOfItemsPerPageInServerResponse) forKey:@"per_page"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.latitude) forKey:@"latitude"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.longitude) forKey:@"longitude"];
    [parameters setObject:@(_radiusOfViewingInMi) forKey:@"radius"];
    
    if (self.productsCategory != nil) {
        [parameters setObject:self.productsCategory forKey:@"products_cat"];
    }
    
    _isDataSynchronizedWithLocation = YES;
    
    // Request example: https://listings-dev.zoomlocal.com/wp-json/rest/v2/vendor?hide_empty=true&latitude=30.401667&longitude=-89.076111&page=1&per_page=10&radius=50
    [[ZLWPApiClient sharedClient] GET:@"wp-json/rest/v2/vendor"
                           parameters:parameters
                           completion:^(OVCResponse *response, NSError *error)
     {
         
         // ALWAYS clear the animation!
         [self.collectionView.infiniteScrollingView stopAnimating];
         
         // disable infinite scroll when we reach the end.
         if ([response.result count] < ZLNumberOfItemsPerPageInServerResponse) {
             self.collectionView.showsInfiniteScrolling = NO;
         }
         
         self.theResponse = response;
         self.theError = error;
         self.busy = NO;
         
         if (!error) {
             [self showFilterByDistanceAnimated:YES];
             _currentPage++; // increase the page number
             //             int currentRow = [_vendors count]; // keep the the index of last row before add new items into the list
             
             // store the items into the existing list
             for (id obj in response.result) {
                 [_vendors addObject:obj];
             }
         } else {
             DDLogError(@"Error: '%@'", error);
             self.collectionView.showsInfiniteScrolling = NO;
         }
         
         // Update the view.
         [self.collectionView reloadData];
     }];
}

@end
