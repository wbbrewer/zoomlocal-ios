//
//  ZLDealsViewController.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 24/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLDealsViewController.h"

#import <UIScrollView+SVInfiniteScrolling.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>

#import "ZLItemsCollectionView.h"
#import "ZLDealsCollectionViewCell.h"

#import "FilterByDistanceTableViewController.h"
#import "FilterByDistanceContainerViewController.h"
#import "ZLSettingsManager.h"
#import "LocationService.h"
#import "ZLDealDetailsViewController.h"
#import "ZLLoginViewController.h"
#import "ZLUserSessionManager.h"
#import "ZLConstants.h"
#import "ZLWPApiClient.h"
#import "ZLUserSessionManager.h"


static NSString *const kDealsCellReuseIdentifier = @"DealsCell";
static const NSInteger kInitialListPage = 1; // paging start from 1, depends on your api


@interface ZLDealsViewController () <
                                    FilterByDistanceTableViewControllerDelegate,
                                    LocationServiceDelegate,
                                    ZLUserSessionManagerObserver,
                                    ZLDealDetailsViewControllerDelegate
                                    >

@property (strong, nonatomic) NSMutableArray *deals;
// To keep track of what is the next page to load
@property (nonatomic, assign) int currentPage;

@property (weak, nonatomic) IBOutlet ZLItemsCollectionView *dealsCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterByDistanceHeightConstraint;

@end


@implementation ZLDealsViewController {
    FilterByDistanceContainerViewController *_filterByDistanceContainerVC;
    float _radiusOfViewingInMi;
    BOOL _isDataSynchronizedWithLocation;
    ZLDealDetailsViewController *_dealDetailsViewController;
    BOOL _userHasLoggedIn;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [[LocationService sharedInstance] addDelegate:self];
        _deals = [NSMutableArray array];
        [[ZLUserSessionManager sharedManager] addUserSessionObserver:self];
    }
    return self;
}

- (void)dealloc
{
    [[LocationService sharedInstance] removeDelegate:self];
    [[ZLUserSessionManager sharedManager] removeUserSessionObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dealsCollectionView.underlyingScrollView.emptyDataSetSource = self; // imlemented in base class
    self.dealsCollectionView.underlyingScrollView.emptyDataSetDelegate = self; // implemented in base class
    
    UINib *dealsCellNib = [UINib nibWithNibName:NSStringFromClass([ZLDealsCollectionViewCell class]) bundle:nil];
    [self.dealsCollectionView registerNib:dealsCellNib
               forCellWithReuseIdentifier:kDealsCellReuseIdentifier];
    
    // This is required to get pull-to-refresh.
    self.dealsCollectionView.underlyingScrollView.alwaysBounceVertical = YES;
    
    @weakify(self)
    
    // refresh new data when pull the table list
    [self.dealsCollectionView.underlyingScrollView addPullToRefreshWithActionHandler:^{
        @strongify(self)
        [self reloadDeals];
    }];
    
    // load more content when scroll to the bottom most
    
    [self.dealsCollectionView.underlyingScrollView addInfiniteScrollingWithActionHandler:^{
        // WORDAROUND: if number of items is not sufficient to cover the whole screen then this block is
        // invoked when user pulls scroll view down (not up as must be) so that to refresh list. This is bug in library.
        if (self.dealsCollectionView.underlyingScrollView.contentOffset.y > 0.0) {
            @strongify(self)
            [self downloadDeals];
        }
    }];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateListIfNeeded];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"EmbedFilterByDistance"]) {
        _filterByDistanceContainerVC = (FilterByDistanceContainerViewController *)[segue destinationViewController];
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
}

#pragma mark <ZLItemsCollectionViewDataSource>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView
{
    return _deals.count;
}

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row
{
    UICollectionViewCell *cell = nil;
    
    ZLDealsCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:kDealsCellReuseIdentifier
                                                                                       forRow:row];
    aCell.coupon = _deals[row];
    cell = aCell;
    
    return cell;
}

#pragma mark <ZLItemsCollectionViewDelegate>

- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row
{
    ZLCoupon *coupon = self.deals[row];
    
    _dealDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DealDetailsViewController"];
    [_dealDetailsViewController showCoupon:coupon withParentViewController:self delegate:self];
}

#pragma mark <FilterByDistanceTableViewControllerDelegate>

- (void)filterByDistanceTableViewControllerDidSaveSettings:(FilterByDistanceTableViewController *)viewController
{
    if (_radiusOfViewingInMi != viewController.radiusInMiles) {
        _radiusOfViewingInMi = viewController.radiusInMiles;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = viewController.radiusInMiles;
        [ZLSettingsManager sharedManager].radiusOfViewingInMi = viewController.radiusInMiles;
        
        [self updateList];
    }
}

#pragma mark <LocationServiceDelegate>

- (void)locationService:(LocationService *)locationService didUpdateLocation:(CLLocation *)location
{
    _isDataSynchronizedWithLocation = NO;
}

#pragma mark <ZLUserSessionManagerObserver>

- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus
{
    if (loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        _userHasLoggedIn = YES;
    }
}

#pragma mark <ZLDealDetailsViewControllerDelegate>

- (void)dealDetailsViewControllerDidPressCloseButton:(ZLDealDetailsViewController *)vc
{
    // It is possible that user has logged in, then displayed data should be updated in cells
    [self updateListIfNeeded];
}

- (void)dealDetailsViewController:(ZLDealDetailsViewController *)vc didRedeemCoupon:(ZLCoupon *)coupon
{
    NSUInteger indexOfCoupon = NSNotFound;
    for (indexOfCoupon = 0; indexOfCoupon < [_deals count]; indexOfCoupon++) {
        if ([[_deals[indexOfCoupon] listingId] isEqualToNumber:coupon.listingId]) {
            break;
        }
    }
    
    [self.deals replaceObjectAtIndex:indexOfCoupon withObject:coupon];
    
    for (ZLDealsCollectionViewCell *cell in [self.dealsCollectionView visibleCells]) {
        if ([cell.coupon.listingId isEqualToNumber:coupon.listingId]) {
            cell.coupon = coupon;
        }
    }    
}

#pragma mark Actions

- (IBAction)filterByDistancePressed:(UIStoryboardSegue *)unwindSegue
{
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterByDistanceNavigationCntr"];
    FilterByDistanceTableViewController *filteringByDistanceVC = (FilterByDistanceTableViewController *)navigationController.topViewController;
    filteringByDistanceVC.delegate = self;
    FilterByDistanceContainerViewController *containerVC = (FilterByDistanceContainerViewController *)unwindSegue.sourceViewController;
    filteringByDistanceVC.radiusInMiles = containerVC.radiusOfSearchInMiles;
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - Helpers -

- (void)updateListIfNeeded
{
    BOOL isDataSynchronizedWithRadiusOfViewing = YES;
    
    if (_radiusOfViewingInMi != [ZLSettingsManager sharedManager].radiusOfViewingInMi) {
        isDataSynchronizedWithRadiusOfViewing = NO;
        _radiusOfViewingInMi = [ZLSettingsManager sharedManager].radiusOfViewingInMi;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
    
    if (!_isDataSynchronizedWithLocation || !isDataSynchronizedWithRadiusOfViewing || _userHasLoggedIn) {
        [self updateList];
    }
}

- (void)updateList
{
    self.currentPage = kInitialListPage;
    [self.deals removeAllObjects];
    [self downloadDeals];
}

- (void)showFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         self.filterByDistanceHeightConstraint.constant = ZLFilterByDistanceHeight;
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         // Bottom constraint has priority 999 to avoid warning when setting height of the whole view to 0.0.
                         self.filterByDistanceHeightConstraint.constant = 0.0;
                         [self.view layoutIfNeeded];
                     }];
}

// Used when user want to reload screen completly rather than download the next package of pages.
- (void)reloadDeals
{
    self.currentPage = kInitialListPage; // reset the page
    [self.deals removeAllObjects]; // remove all data
    [self.dealsCollectionView reloadData]; // before load new content, clear the existing table list
    [self downloadDeals]; // load new data
    [self.dealsCollectionView.underlyingScrollView.pullToRefreshView stopAnimating]; // clear the animation
    
    // once refresh, allow the infinite scroll again
    self.dealsCollectionView.underlyingScrollView.showsInfiniteScrolling = YES;
}


#pragma mark Actions

- (void)loginButtonOnCouponPressed
{
    [ZLLoginViewController presentLoginScreenWithDidDismissHandler:^{

    }];
}

#pragma mark Downloading

- (void)downloadDeals
{
    _isDataSynchronizedWithLocation = YES;
    _userHasLoggedIn = NO;

    [self moveToBusyState];

    [self.dealsCollectionView reloadData];
    [self hideFilterByDistanceAnimated:YES];

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"yes" forKey:@"featured"];
    [parameters setObject:@(_currentPage) forKey:@"page"];
    [parameters setObject:@(ZLNumberOfItemsPerPageInServerResponse) forKey:@"per_page"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.latitude) forKey:@"latitude"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.longitude) forKey:@"longitude"];
    [parameters setObject:@(_radiusOfViewingInMi) forKey:@"radius"];
//
//    
    // Example: https://listings-dev.zoomlocal.com/wp-json/rest/v2/coupons
    [[ZLWPApiClient sharedClient] authorizedGET:@"wp-json/rest/v2/coupons"
                                     parameters:parameters
                                     completion:^(OVCResponse *response, NSError *error) {

                                         // ALWAYS clear the animation!
                                         [self.dealsCollectionView.underlyingScrollView.infiniteScrollingView stopAnimating];

                                         // disable infinite scroll when we reach the end.
                                         if ([response.result count] < ZLNumberOfItemsPerPageInServerResponse) {
                                             self.dealsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                                         }

                                         if (error == nil) {

                                             [self showFilterByDistanceAnimated:YES];
                                             _currentPage++; // increase the page number
                                             
                                             // store the items into the existing list
                                             for (id obj in response.result) {
                                                 [_deals addObject:obj];
                                             }
                                             
                                             [self moveToNormalStateWithEmptySetFlag:(_deals.count == 0)];

                                         }
                                         else {
                                             DDLogError(@"Error: '%@'", error);
                                             [self moveToErrorStateWithError:error];
                                             self.dealsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                                         }

                                         // Update the view.
                                         [self.dealsCollectionView reloadData];
                                     }];
}

@end
