//
//  ZLListingsTabViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 23/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLListingsTabViewController.h"

#import <UIAlertController+Blocks.h>

#import "ZLConstants.h"
#import "ZLListingsViewController.h"
#import "ZLUserSessionManager.h"


@interface ZLListingsTabViewController () <ZLUserSessionManagerObserver>

@end


@implementation ZLListingsTabViewController

- (void)dealloc
{
    [[ZLUserSessionManager sharedManager] removeUserSessionObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupSegmentedPager];
    [[ZLUserSessionManager sharedManager] addUserSessionObserver:self];
    [self configureLogoutButton];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"mx_page_1"]) {
        ZLListingsViewController *followedVC = (ZLListingsViewController *)segue.destinationViewController;
        followedVC.showOnlyItemsOfFollowedVendors = YES;
    }
}

#pragma - mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager
{
    return 2;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index
{
    return [@[@"All", @"Followed"] objectAtIndex:index];
}

#pragma -mark <MXSegmentedPagerDelegate>

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title
{
    DDLogVerbose(@"%@ page selected.", title);
}

#pragma mark <ZLUserSessionManagerObserver>

- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus
{
    [self configureLogoutButton];
}


#pragma mark - Private -

#pragma mark Actions

- (IBAction)logoutBarButtonPressed:(UIBarButtonItem *)sender
{
    [UIAlertController showAlertInViewController:self
                                       withTitle:@"Logout of ZoomLocal"
                                         message:@"Are you sure you want to logout of ZoomLocal?"
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@[@"OK"]
                                        tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                                            
                                            if (buttonIndex != controller.cancelButtonIndex) {
                                                [[ZLUserSessionManager sharedManager] logout];
                                            }
                                            
                                        }];
}


#pragma mark Helpers

- (void)setupSegmentedPager
{
    UIColor *tintColor = [UIColor blueTintColor];
    self.segmentedPager.pager.transitionStyle = MXPagerViewTransitionStyleTab;
    self.segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor grayTextColor]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : tintColor};
    
    self.segmentedPager.segmentedControl.selectionIndicatorColor = tintColor;
}

- (void)configureLogoutButton
{
    if ([ZLUserSessionManager sharedManager].loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"LogoutIcon"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(logoutBarButtonPressed:)];
    }
    else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

@end
