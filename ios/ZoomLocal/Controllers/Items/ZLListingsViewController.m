//
//  ZLListingsViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/14/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLListingsViewController.h"

#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIScrollView+VGParallaxHeader.h>
#import "TagDataLayer.h"
#import "TAGManager.h"
#import <ReactiveCocoa/RACEXTScope.h>

#import "FilterByDistanceTableViewController.h"
#import "LocationService.h"
#import "ZLConstants.h"
#import "ZLWPApiClient.h"
#import "ProductModel.h"
#import "ZLProductCollectionViewCell.h"
#import "ZLItemDetailsViewController.h"
#import "ZLSettingsManager.h"
#import "FilterByDistanceContainerViewController.h"
#import "ZLItemsCollectionView.h"
#import "ZLUserSessionManager.h"
#import "ZLLoginViewController.h"


static const NSInteger kInitialListPage = 1; // paging start from 1, depends on your api
static NSString *const kListingsCellReuseIdentifier = @"ListingCell";

static NSString *const kStoryboardSegueShowListingDetails = @"showListingDetails";

enum {
    ZLListingsTableSectionProducts = 0
};


@interface ZLListingsViewController () <ZLItemsCollectionViewDataSource,
                                        ZLItemsCollectionViewDelegate,
                                        UICollectionViewDelegateFlowLayout,
                                        UITraitEnvironment,
                                        FilterByDistanceTableViewControllerDelegate,
                                        LocationServiceDelegate>

// to keep track of what is the next page to load
@property (nonatomic, assign) int currentPage;

@property (nonatomic, strong) NSMutableArray<ProductModel *> *listingsItems;
@property (weak, nonatomic) IBOutlet ZLItemsCollectionView *itemsCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterByDistanceHeightConstraint;

@end


@implementation ZLListingsViewController {
    FilterByDistanceContainerViewController *_filterByDistanceContainerVC;
    float _radiusOfViewingInMi;
    BOOL _isDataSynchronizedWithLocation;
    NSString *_itemId;
    BOOL _showLoginButton;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [[LocationService sharedInstance] addDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[LocationService sharedInstance] removeDelegate:self];
}

#pragma mark - Public -

- (void)goToItemDetailsScreenWithItemId:(NSString *)itemId
{
    _itemId = itemId;
    [self performSegueWithIdentifier:kStoryboardSegueShowListingDetails sender:self];
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    self.itemsCollectionView.underlyingScrollView.emptyDataSetSource = self; // imlemented in base class
    self.itemsCollectionView.underlyingScrollView.emptyDataSetDelegate = self; // implemented in base class
    
    [self.itemsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ZLProductCollectionViewCell class]) bundle:nil]
               forCellWithReuseIdentifier:kListingsCellReuseIdentifier];
    
    // initialize
    _listingsItems = [NSMutableArray array];
    _currentPage = kInitialListPage;
    
    // This is required to get pull-to-refresh.
    self.itemsCollectionView.underlyingScrollView.alwaysBounceVertical = YES;
    
    @weakify(self)
    
    // refresh new data when pull the table list
    [self.itemsCollectionView.underlyingScrollView addPullToRefreshWithActionHandler:^{
        @strongify(self)
        [self reloadListings];
    }];
    
    // load more content when scroll to the bottom most
    
    [self.itemsCollectionView.underlyingScrollView addInfiniteScrollingWithActionHandler:^{
        // WORDAROUND: if number of items is not sufficient to cover the whole screen then this block is
        // invoked when user pulls scroll view down (not up as must be) so that to refresh list. This is bug in library.
        if (self.itemsCollectionView.underlyingScrollView.contentOffset.y > 0.0) {
            @strongify(self)
            [self fetch];
        }
    }];
}

// WARNING: this method is not invoked for "All" tab (the first tab)
// this is because of error in MSPagetView implementation. Maybe because view (or superview) have zero
// size when invoking `addSubview:` method.
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self updateListIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateListIfNeeded];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Listings"}];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Update the view.
    [self.itemsCollectionView reloadData];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection
              withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    //DDLogDebug(@"Trait collection = %@", newCollection);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kStoryboardSegueShowListingDetails]) {
        if ([sender isKindOfClass:[ZLProductCollectionViewCell class]]) {
            ZLProductCollectionViewCell *cell = (ZLProductCollectionViewCell *)sender;
            ProductModel *productModel = cell.productModel;
            ZLItemDetailsViewController *controller = (ZLItemDetailsViewController *)[segue destinationViewController];
            controller.productId = [productModel.listingId stringValue];
        }
        else if ([sender isKindOfClass:[self class]]) { // handling deep link
            ZLItemDetailsViewController *controller = (ZLItemDetailsViewController *)[segue destinationViewController];
            controller.productId = _itemId;
        }
    }
    else if ([[segue identifier] isEqualToString:@"EmbedFilterByDistance"]) {
        _filterByDistanceContainerVC = (FilterByDistanceContainerViewController *)[segue destinationViewController];
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
}

#pragma mark EmptyDataSetViewController override

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    if (_showLoginButton) {
        return YES;
    }
    else {
        return [super emptyDataSetShouldAllowTouch:scrollView];
    }
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    if (_showLoginButton) {
        NSDictionary *attributes = @{
                                     NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:19.0],
                                     NSForegroundColorAttributeName : [UIColor blueTintColor],
                                     };
        return [[NSAttributedString alloc] initWithString:@"Log In" attributes:attributes];
    }
    else {
        return [super buttonTitleForEmptyDataSet:scrollView forState:state];
    }
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    if (_showLoginButton) {
        @weakify(self)
        [ZLLoginViewController presentLoginScreenWithDidDismissHandler:^{
            @strongify(self)
            [self reloadListings];
        }];
    }
    else {
        [super emptyDataSet:scrollView didTapButton:button];
    }
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    if (_showLoginButton) {
        return nil;
    }
    else {
        return [super titleForEmptyDataSet:scrollView];
    }
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    if (_showLoginButton) {
        return nil;
    }
    else {
        return [super descriptionForEmptyDataSet:scrollView];
    }
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    if (_showLoginButton) {
        return nil;
    }
    else {
        return [super imageForEmptyDataSet:scrollView];
    }
}

#pragma mark <ZLItemsCollectionViewDataSource>

- (NSInteger)numberOfRowsInItemsCollectionView:(ZLItemsCollectionView *)collectionView
{
    return _listingsItems.count;
}

- (UICollectionViewCell *)itemsCollectionView:(ZLItemsCollectionView *)collectionView
                             cellForItemInRow:(NSInteger)row
{
    UICollectionViewCell *cell = nil;
    
    ZLProductCollectionViewCell *aCell = [collectionView dequeueReusableCellWithReuseIdentifier:kListingsCellReuseIdentifier
                                                                                       forRow:row];
    aCell.productModel = _listingsItems[row];
    cell = aCell;
    
    return cell;
}

#pragma mark <ZLItemsCollectionViewDelegate>

- (void)itemsCollectionView:(ZLItemsCollectionView *)collectionView didSelectItemAtRow:(NSInteger)row
{
    UICollectionViewCell *cell = [collectionView cellForItemAtRow:row];
    [self performSegueWithIdentifier:kStoryboardSegueShowListingDetails sender:cell];
}

#pragma mark <FilterByDistanceTableViewControllerDelegate>

- (void)filterByDistanceTableViewControllerDidSaveSettings:(FilterByDistanceTableViewController *)viewController
{
    if (_radiusOfViewingInMi != viewController.radiusInMiles) {
        _radiusOfViewingInMi = viewController.radiusInMiles;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = viewController.radiusInMiles;
        [ZLSettingsManager sharedManager].radiusOfViewingInMi = viewController.radiusInMiles;
        
        [self updateList];
    }
}

#pragma mark <LocationServiceDelegate>

- (void)locationService:(LocationService *)locationService didUpdateLocation:(CLLocation *)location
{
    _isDataSynchronizedWithLocation = NO;
}

#pragma mark Actions

- (IBAction)filterByDistancePressed:(UIStoryboardSegue *)unwindSegue
{
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterByDistanceNavigationCntr"];
    FilterByDistanceTableViewController *filteringByDistanceVC = (FilterByDistanceTableViewController *)navigationController.topViewController;
    filteringByDistanceVC.delegate = self;
    FilterByDistanceContainerViewController *containerVC = (FilterByDistanceContainerViewController *)unwindSegue.sourceViewController;
    filteringByDistanceVC.radiusInMiles = containerVC.radiusOfSearchInMiles;
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}


#pragma mark - Helpers -

- (void)updateListIfNeeded
{
    BOOL isDataSynchronizedWithRadiusOfViewing = YES;
    
    if (_radiusOfViewingInMi != [ZLSettingsManager sharedManager].radiusOfViewingInMi) {
        isDataSynchronizedWithRadiusOfViewing = NO;
        _radiusOfViewingInMi = [ZLSettingsManager sharedManager].radiusOfViewingInMi;
        _filterByDistanceContainerVC.radiusOfSearchInMiles = _radiusOfViewingInMi;
    }
    
    if (!_isDataSynchronizedWithLocation || !isDataSynchronizedWithRadiusOfViewing) {
        [self updateList];
    }
}

- (void)updateList
{
    self.currentPage = kInitialListPage;
    [self.listingsItems removeAllObjects];
    [self fetch];
}

- (void)showFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         self.filterByDistanceHeightConstraint.constant = ZLFilterByDistanceHeight;
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideFilterByDistanceAnimated:(BOOL)animated
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:(animated ? ZLAnimationDurationNormal : 0.0)
                     animations:^{
                         // Bottom constraint has priority 999 to avoid warning when setting height of the whole view to 0.0.
                         self.filterByDistanceHeightConstraint.constant = 0.0;
                         [self.view layoutIfNeeded];
                     }];
}

// Used when user want to reload screen completly rather than download the next package of pages.
- (void)reloadListings
{
    self.currentPage = kInitialListPage; // reset the page
    [self.listingsItems removeAllObjects]; // remove all data
    [self.itemsCollectionView reloadData]; // before load new content, clear the existing table list
    [self fetch]; // load new data
    [self.itemsCollectionView.underlyingScrollView.pullToRefreshView stopAnimating]; // clear the animation
    
    // once refresh, allow the infinite scroll again
    self.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = YES;
}

#pragma mark Downloading

- (void)fetch
{
    _showLoginButton = NO;
    self.theResponse = nil;
    self.theError = nil;
    self.busy = YES;
    
    [self.itemsCollectionView reloadData];
    [self hideFilterByDistanceAnimated:YES];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"yes" forKey:@"featured"];
    [parameters setObject:@(_currentPage) forKey:@"page"];
    [parameters setObject:@(ZLNumberOfItemsPerPageInServerResponse) forKey:@"per_page"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.latitude) forKey:@"latitude"];
    [parameters setObject:@([LocationService sharedInstance].currentLocation.coordinate.longitude) forKey:@"longitude"];
    [parameters setObject:@(_radiusOfViewingInMi) forKey:@"radius"];
    
    if (self.term.slug != nil) {
        [parameters setObject:self.term.slug forKey:@"filter[product_cat]"];
    }
    
    if (self.showOnlyItemsOfFollowedVendors) {
        // Only listings of being followed by current user (if he is authenticated) vendors
        [parameters setObject:@"yes" forKey:@"followed"];
    }
    
    _isDataSynchronizedWithLocation = YES;
    
    // Example: https://listings-dev.zoomlocal.com/wp-json/rest/v2/listing?featured=yes&latitude=30.401667&longitude=-89.076111&page=1&per_page=10&radius=50
    // 'authorized' so that to add "access_token" to params
    [[ZLWPApiClient sharedClient] authorizedGET:@"wp-json/rest/v2/listing"
                                     parameters:parameters
                                     completion:^(OVCResponse *response, NSError *error) {
                                         
                                       // ALWAYS clear the animation!
                                       [self.itemsCollectionView.underlyingScrollView.infiniteScrollingView stopAnimating];
                                       
                                       // disable infinite scroll when we reach the end.
                                       if ([response.result count] < ZLNumberOfItemsPerPageInServerResponse) {
                                           self.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                                       }
                                       
                                       self.theResponse = response;
                                       self.theError = error;
                                       self.busy = NO;
                                       
                                       if (error == nil) {
                                           [self showFilterByDistanceAnimated:YES];
                                           _currentPage++; // increase the page number
                                           
                                           // store the items into the existing list
                                           for (id obj in response.result) {
                                               [_listingsItems addObject:obj];
                                           }
                                       }
                                       else {
                                           DDLogError(@"Error: '%@'", error);
                                           self.itemsCollectionView.underlyingScrollView.showsInfiniteScrolling = NO;
                                           BOOL isUserNotAuthenticated = (response.HTTPResponse.statusCode == 403);
                                           _showLoginButton = (_showOnlyItemsOfFollowedVendors && isUserNotAuthenticated); // only for "Followed" tab
                                       }
                                       
                                       // Update the view.
                                       [self.itemsCollectionView reloadData];
                                   }];
}

@end

