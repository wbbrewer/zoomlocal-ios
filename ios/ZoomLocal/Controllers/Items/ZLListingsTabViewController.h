//
//  ZLListingsTabViewController.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 23/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MXSegmentedPagerController.h>


@interface ZLListingsTabViewController : MXSegmentedPagerController

@end
