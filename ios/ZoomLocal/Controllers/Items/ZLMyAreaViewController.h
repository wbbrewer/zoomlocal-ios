//
//  ZLMyAreaViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"


@interface ZLMyAreaViewController : ZLEmptyDataSetViewController

/**
 When setting only vendors with products of given category are shown.
 */
@property (strong, nonatomic) NSString *productsCategory;

- (void)goToVendorDetailsScreenWithId:(NSString *)vendorId;

@end
