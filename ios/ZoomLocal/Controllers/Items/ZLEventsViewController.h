//
//  ZLEventsViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/4/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"


@interface ZLEventsViewController : ZLEmptyDataSetViewController

/**
 Allows to show programatically screen with details concerning specific item.
 Used mainly for deep linking.
 */
- (void)goToItemDetailsScreenWithItemId:(NSString *)eventId;

@end
