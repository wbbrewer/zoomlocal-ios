//
//  CategoriesViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/14/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "CategoriesViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "TagDataLayer.h"
#import "TAGManager.h"
#import "ZLListingsViewController.h"
#import "ZLWPApiClient.h"
#import "TermModel.h"
#import "CategoryWithImageCell.h"


static NSString *const reuseIdentifier = @"CategoryCell";
static const double kResponsiveCellHeightCompact = 60;
static const double kResponsiveCellHeightRegular = 120.0;


@interface CategoriesViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *results;

@end

@implementation CategoriesViewController

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:@"CategoryWithImageCell" bundle:nil]
          forCellWithReuseIdentifier:reuseIdentifier];
    
    if (self.results == nil) {
        self.results = [[NSMutableArray alloc] init];
        [self fetch];
    }
    else {
        // Update the view.
        [self.collectionView reloadData];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Categories"}];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // Update the view.
    [self.collectionView reloadData];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection
              withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    DDLogDebug(@"Trait collection = %@", newCollection);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showListings"]) {
        ZLListingsViewController *controller = (ZLListingsViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
        TermModel *term = self.results[indexPath.row];
        controller.term = term;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.results.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryWithImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    TermModel *term = self.results[indexPath.row];
    
    [cell.image sd_setImageWithURL:[NSURL URLWithString:[term.images.thumbnail firstObject]]
                  placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [cell.label setText:term.name];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TermModel *term = self.results[indexPath.row];
    [self performSegueWithIdentifier:@"showListings"
                              sender:term];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular &&
        self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular) {
        cellSize.height = kResponsiveCellHeightRegular;
        cellSize.width = (self.view.bounds.size.width/2) - 7.5;
    }
    else {
        cellSize.height = kResponsiveCellHeightCompact;
        cellSize.width = (self.view.bounds.size.width) - 10;
    }
    
    return cellSize;
}

#pragma mark - Helpers -

#pragma mark Downloading

- (void)fetch
{
    //Fetch the categories
    // :parent - parent category, 0 - the root category
    // :per_page 0 - receive all items
    //https://listings.zoomlocal.com/wp-json/wp/v2/product_cat/?hide_empty=true&orderby=slug&parent=0&per_page=50
    [[ZLWPApiClient sharedClient] GET:@"wp-json/wp/v2/product_cat"
                         parameters:@{@"hide_empty" : @"true",
                                      @"orderby" : @"slug",
                                      @"parent" : @"0",
                                      @"per_page" : @"100"}
                         completion:^(OVCResponse *response, NSError *error) {
        
        self.theResponse = response;
        self.theError = error;
        
        if (!error) {
            // Filter for top level categories.
            for (TermModel *term in response.result) {
                [self.results addObject:term];
            }
        }
        else {
            DDLogError(@"Error: '%@'", error);
        }
        
        // Update the view.
        [self.collectionView reloadData];
    }];
}

@end
