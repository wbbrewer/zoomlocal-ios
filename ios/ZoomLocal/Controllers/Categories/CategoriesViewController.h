//
//  CategoriesViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/14/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"


@interface CategoriesViewController : ZLEmptyDataSetViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITraitEnvironment>

@end
