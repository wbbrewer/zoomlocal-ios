//
//  FilterByDistanceTableViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 14/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "FilterByDistanceTableViewController.h"

@import MapKit;

#import "TagDataLayer.h"
#import "TAGManager.h"

#import "ZLConstants.h"
#import "LocationService.h"


const double ZLFilterByDistanceDefaultRadiusOfViewing = 50.0;


@interface FilterByDistanceTableViewController () <MKMapViewDelegate,
                                                    UIPickerViewDataSource,
                                                    UIPickerViewDelegate>

@property (readonly, nonatomic) NSArray *possibleRadiuses;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *radiusOptionValueLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *radiusPickerView;

@end


@implementation FilterByDistanceTableViewController {
        BOOL _radiusPickerVisible;
        NSNumber *_radiusInMilesWrapped;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        _possibleRadiuses = @[@(1.0), @(5.0), @(10.0), @(25.0), @(50.0), @(100.0), @(250.0), @(500.0), @(1000.0)];
        NSParameterAssert([_possibleRadiuses containsObject:@(ZLFilterByDistanceDefaultRadiusOfViewing)]);
        _radiusInMilesWrapped = @(ZLFilterByDistanceDefaultRadiusOfViewing);
    }
    return self;
}

#pragma mark - Properties -

- (void)setRadiusInMiles:(double)radiusInMiles
{
    NSParameterAssert([self.possibleRadiuses containsObject:@(radiusInMiles)]);
    _radiusInMilesWrapped = @(radiusInMiles);
}

- (double)radiusInMiles
{
    return _radiusInMilesWrapped.doubleValue;
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    [self showRegionInRadius:[self metersInMiles:_radiusInMilesWrapped.doubleValue] location:[LocationService sharedInstance].currentLocation];
    _radiusOptionValueLabel.text = [self formatRadius:_radiusInMilesWrapped];
    
    [self.mapView setShowsUserLocation:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Filter by distance"}];
}

#pragma mark - Table view

- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // This makes the "Add Photo" row taller when the user picked a photo.
    if (indexPath.section == 0) {
        return 220.0;
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        const CGFloat kTopSeparatorHeight = 1.0;
        const CGFloat kPickerHeight = 216.0;
        return _radiusPickerVisible ? (kTopSeparatorHeight + kPickerHeight) : 0.0f;
    }
    else {
        return 60.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            if (!_radiusPickerVisible) {
                [self showRadiusPicker];
            } else {
                [self hideRadiusPicker];
            }
            return;
        }
    }
    
    // Also hide the picker when tapped on any other row.
    [self hideRadiusPicker];
}

#pragma mark <MKMapViewDelegate>

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithOverlay:overlay];
    circleView.fillColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.1];
    
    return circleView;
}

#pragma mark <UIPickerViewDataSource>

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.possibleRadiuses.count;
}

#pragma mark <UIPickerViewDelegate>

- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView
                      attributedTitleForRow:(NSInteger)row
                               forComponent:(NSInteger)component {
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:[[NSNumberFormatter radiusFormatter] stringFromNumber:self.possibleRadiuses[row]]
                                                                attributes:@{
                                                                             NSForegroundColorAttributeName : [UIColor blackColor]
                                                                             // NSFontAttributeName is not taken into account by picker, so it is useless to set it
                                                                             }];
    return title;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _radiusOptionValueLabel.text = [self formatRadius:self.possibleRadiuses[row]];
    _radiusInMilesWrapped = self.possibleRadiuses[row];
    [self showRegionInRadius:[self metersInMiles:_radiusInMilesWrapped.doubleValue] location:[LocationService sharedInstance].currentLocation];
}

#pragma mark Action

- (IBAction)cancelBarButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveBarButtonPressed:(id)sender {
    [self.delegate filterByDistanceTableViewControllerDidSaveSettings:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Helpers

- (void)showRegionInRadius:(double)radiusInMeters location:(CLLocation *)location {
    
    MKCircle *circleOverlay = [MKCircle circleWithCenterCoordinate:location.coordinate radius:radiusInMeters];
    
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(circleOverlay.boundingMapRect);
    region.span.latitudeDelta *= 1.1;
    region.span.longitudeDelta *= 1.1;
    
    [_mapView setRegion:region animated:YES];
    
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView addOverlay:circleOverlay];
}

- (void)showRadiusPicker
{
    NSIndexPath *radiusRowIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:radiusRowIndexPath];
    cell.detailTextLabel.textColor = cell.detailTextLabel.tintColor;
    
    NSInteger rowToSelect = [self.possibleRadiuses indexOfObject:_radiusInMilesWrapped];
    [self.radiusPickerView selectRow:rowToSelect inComponent:0 animated:NO];
    
    _radiusPickerVisible = YES;
    // To update height in animated manner
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    // 4
    self.radiusPickerView.hidden = NO;
    self.radiusPickerView.alpha = 0.0f;
    [UIView animateWithDuration:0.25 animations:^{
        self.radiusPickerView.alpha = 1.0f;
    }];
    
    NSIndexPath *pickerIndexPath = [NSIndexPath indexPathForRow:radiusRowIndexPath.row + 1 inSection:radiusRowIndexPath.section];
    [self.tableView scrollToRowAtIndexPath:pickerIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)hideRadiusPicker
{
    if (_radiusPickerVisible) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        
        _radiusPickerVisible = NO;
        // To update height in animated manner
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        
        [UIView animateWithDuration:0.25 animations:^{
            self.radiusPickerView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            self.radiusPickerView.hidden = YES;
        }];
    }
}

- (double)metersInMiles:(double)miles {
    return 1609 * miles;
}

- (NSString *)formatRadius:(NSNumber *)radius {
    NSString *formattedRadius = [[NSNumberFormatter radiusFormatter] stringFromNumber:radius];
    return [formattedRadius stringByAppendingString:@" mile(s)"];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    DDLogDebug(@"mapView didUpdateUserLocation: %@", userLocation);

    [self showRegionInRadius:[self metersInMiles:_radiusInMilesWrapped.doubleValue] location:[LocationService sharedInstance].currentLocation];
}

@end
