//
//  EmptyDataSetViewController.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/7/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"
#import <Overcoat/Overcoat.h>


@interface ZLEmptyDataSetViewController : UIViewController <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

// TODO: remove all properties and migrate to using methods.
@property BOOL busy;
@property OVCResponse *theResponse;
@property NSError *theError;

/// Used for example to indicate that data is being donwloaded.
- (void)moveToBusyState;

/// Typically used when network error occured.
- (void)moveToErrorStateWithError:(NSError *)error;

/// Used when data was received successfully and state just should be reset to some neutral state.
/// If there is no items in data set (`isEmpty` is set to YES) then appropriate message is displayed.
- (void)moveToNormalStateWithEmptySetFlag:(BOOL)isEmpty;

@end
