//
//  ZLZoomImageViewController.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 02/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLZoomImageViewController.h"


@interface ZLZoomImageViewController () <UIScrollViewDelegate>

@property (strong, nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end


@implementation ZLZoomImageViewController

- (instancetype)initWithImage:(UIImage *)image
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        _image = image;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    UIImageView *imageView = [[UIImageView alloc] initWithImage:_image];
    [self.scrollView addSubview:imageView];
    self.imageView = imageView;
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 100.0;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGRect imageViewFrame = CGRectZero;
    imageViewFrame.size = [self rectangleSizeFittedIntoRectangleWithSize:self.scrollView.bounds.size
                                                            originalSize:self.imageView.image.size];
    self.imageView.frame = imageViewFrame;
    
    self.scrollView.contentSize = self.imageView.frame.size;
    
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(self.scrollView.bounds),
                                      CGRectGetMidY(self.scrollView.bounds));
    [self view:self.imageView setCenter:centerPoint];
}

#pragma mark <UIScrollViewDelegate>

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

/**
 Content offset is set to zero in animated manner after finishing zooming. Along with scaling including center of being zoomed view this leads to
 shift of being zoomed view. Here we center zoomed view so that is was in the center of scroll view.
 */
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *zoomView = [scrollView.delegate viewForZoomingInScrollView:scrollView];
    CGRect zoomViewFrame = zoomView.frame;
    if (zoomViewFrame.size.width < scrollView.bounds.size.width) {
        zoomViewFrame.origin.x = (scrollView.bounds.size.width - zoomViewFrame.size.width) / 2.0;
    }
    else {
        zoomViewFrame.origin.x = 0.0;
    }
    
    if (zoomViewFrame.size.height < scrollView.bounds.size.height) {
        zoomViewFrame.origin.y = (scrollView.bounds.size.height - zoomViewFrame.size.height) / 2.0;
    }
    else {
        zoomViewFrame.origin.y = 0.0;
    }
    zoomView.frame = zoomViewFrame;
}

#pragma mark - Private -

/**
 Prevent view from having negative origin (x or y). If locationg view leads to negative origin coordinates without 
 adjusting content offset of scroll view then content offset is adjusted so that there were no netagive coordinates.
 The visible portion of image is left the same as it were if we do not adjust content offset.
 */
- (void)view:(UIView *)view setCenter:(CGPoint)centerPoint
{
    CGRect viewFrame = view.frame;
    CGPoint contentOffset = self.scrollView.contentOffset;
    
    CGFloat x = centerPoint.x - viewFrame.size.width / 2.0;
    CGFloat y = centerPoint.y - viewFrame.size.height / 2.0;
    
    if (x < 0) {
        contentOffset.x = -x;
        viewFrame.origin.x = 0.0;
    }
    else {
        viewFrame.origin.x = x;
    }
    
    if (y < 0) {
        contentOffset.y = -y;
        viewFrame.origin.y = 0.0;
    }
    else {
        viewFrame.origin.y = y;
    }
    
    view.frame = viewFrame;
    self.scrollView.contentOffset = contentOffset;
}

/**
 Finds frame's size of rectangle that is inscribed into another rectangle with `size` size. 
 If being inscribed rectangle fits into inscribing rectangle then returns `originalSize`.
 @param `originalSize` size of rectangle that must be inscribed into rectangle with `size` size.
 */
- (CGSize)rectangleSizeFittedIntoRectangleWithSize:(CGSize)size originalSize:(CGSize)originalSize
{
    CGSize fittedSize = size;

    if (originalSize.width > size.width || originalSize.height > size.height) {
        if ((originalSize.width / originalSize.height) > (size.width / size.height)) {
            fittedSize.width = size.width;
            fittedSize.height = (originalSize.height / originalSize.width) * size.width;
        }
        else {
            fittedSize.height = size.height;
            fittedSize.width = (originalSize.width / originalSize.height) * size.height;
        }
    }
    else {
        // If image is fitted into screen bounds
        fittedSize = originalSize;
    }

    return fittedSize;
}

@end
