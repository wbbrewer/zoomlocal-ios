//
//  ZLZoomImageViewController.h
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 02/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZLZoomImageViewController : UIViewController

- (instancetype)initWithImage:(UIImage *)image;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end
