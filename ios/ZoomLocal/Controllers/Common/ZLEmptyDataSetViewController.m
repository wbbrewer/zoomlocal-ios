//
//  EmptyDataSetViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/7/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLEmptyDataSetViewController.h"

#import <libextobjc/extobjc.h>

#import "LocationService.h"


typedef NS_ENUM(NSInteger, EmptyDataSetState) {
    EmptyDataSetStateNormal         = 0,
    EmptyDataSetStateBusy            = 1,
    EmptyDataSetStateEmpty           = 2,
    EmptyDataSetStateNetworkError    = 3,
};


@implementation ZLEmptyDataSetViewController {
    EmptyDataSetState _state;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        [self common_init];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {
        [self common_init];
    }
    return self;
}

- (void)common_init
{
    _state = EmptyDataSetStateNormal;
}


#pragma mark - Public -

- (void)moveToBusyState
{
    _state = EmptyDataSetStateBusy;
}

- (void)moveToErrorStateWithError:(NSError *)error
{
    _state = EmptyDataSetStateNetworkError;
    _theError = error;
}

- (void)moveToNormalStateWithEmptySetFlag:(BOOL)areThereItemsInDataSet
{
    if (areThereItemsInDataSet) {
        _state = EmptyDataSetStateEmpty;
    }
    else {
        _state = EmptyDataSetStateNormal;
    }
}

#pragma mark <DZNEmptyDataSetSource>

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    switch ([self currentState]) {
        case EmptyDataSetStateNormal:
        {
            text = @"ZoomLocal";
            break;
        }
        case EmptyDataSetStateBusy:
        {
            text = @"Please wait...";
            break;
        }
        case EmptyDataSetStateEmpty:
        {
            text = @"No Results";
            break;
        }
        case EmptyDataSetStateNetworkError:
        {
            text = @"Error";
            break;
        }
    }
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:21.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:146/255.0 green:146/255.0 blue:146/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    switch ([self currentState]) {
        case EmptyDataSetStateNormal:
        {
            text = @"Find What You Want, When You Want It.";
            break;
        }
        case EmptyDataSetStateBusy:
        {
            text = @"We are looking for items in your area.";
            break;
        }
        case EmptyDataSetStateEmpty:
        {
            text = @"Sorry, we could not find any items.";
            break;
        }
        case EmptyDataSetStateNetworkError:
        {
            text = _theError.localizedDescription;
            break;
        }
    }
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    switch ([self currentState]) {
        case EmptyDataSetStateNormal:
        {
            return nil;
        }
        case EmptyDataSetStateBusy:
        {
            return nil;
        }
        case EmptyDataSetStateEmpty:
        {
            return nil;
        }
        case EmptyDataSetStateNetworkError:
        {
            return [UIImage imageNamed:@"network_error"];
        }
    }
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark <DZNEmptyDataSetDelegate>

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return NO;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view
{
    DDLogDebug(@"");
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    DDLogDebug(@"");
}

#pragma mark - Helpers -

- (enum EmptyDataSetState)currentState
{
    if (_state != EmptyDataSetStateNormal) {
        return _state;
    }
    else {
        if (_busy && !_theResponse.result && !_theError) {
            return EmptyDataSetStateBusy;
        }
        else if (!_busy && _theResponse.result && !_theError) {
            return EmptyDataSetStateEmpty;
        }
        else if (_theError) {
            return EmptyDataSetStateNetworkError;
        }
    }
    return EmptyDataSetStateNormal;
}

@end
