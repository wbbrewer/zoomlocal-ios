//
//  ZLHomeCollectionViewFlowLayout.h
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/6/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

@import UIKit;

extern const CGFloat ZLHomeMenuCollectionViewMinInteritemSpacing;


@interface ZLHomeCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
