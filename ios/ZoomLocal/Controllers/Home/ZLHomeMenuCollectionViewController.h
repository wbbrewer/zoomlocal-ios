//
//  ZLHomeMenuCollectionViewController.h
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/13/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

@import UIKit;

#import "ZLHomeMenuCollectionViewControllerDelegate.h"


@interface ZLHomeMenuCollectionViewController : UIViewController

@property (assign, nonatomic) NSObject<ZLHomeMenuCollectionViewControllerDelegate> *delegate;

@end
