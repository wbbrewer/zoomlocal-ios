//
//  ZLHomeMenuCollectionViewController.m
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/13/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

#import "ZLHomeMenuCollectionViewController.h"

#import "ZLHomeCollectionViewCell.h"
#import "ZLHomeCollectionViewFlowLayout.h"
#import "ZLNavigationManager.h"

const CGFloat kItemHeight = 88.0;
const CGFloat kMinLineSpacing = 13.0;


static NSString *const kHomeCollectionViewCellUID = @"HomeCollectionViewCell";


@interface ZLHomeMenuCollectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
{
    UICollectionView *_view;
}

@property (nonatomic, readonly) NSArray *fetchedObjects;

@end


@implementation ZLHomeMenuCollectionViewController


#pragma mark -
#pragma mark UIViewController override

- (void)loadView
{
    ZLHomeCollectionViewFlowLayout *collectionLayout = [ZLHomeCollectionViewFlowLayout new];
    _view = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:collectionLayout];
    _view.delegate = self;
    _view.dataSource = self;
    _view.showsHorizontalScrollIndicator = NO;
    _view.showsVerticalScrollIndicator = NO;
    _view.backgroundColor = [UIColor clearColor];
    _view.allowsSelection = YES;
    
    [_view registerClass:[ZLHomeCollectionViewCell class] forCellWithReuseIdentifier:kHomeCollectionViewCellUID];
    
    [self setView:_view];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        // Empty
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [[(UICollectionView *)self.view collectionViewLayout] invalidateLayout];
    }];
}

#pragma mark -
#pragma mark UICollectionViewDataSource callbacks

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if (section == 1) {
        return 6;
    }
    else if (section == 2) {
        return 2;
    }
    else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    
    if (indexPath.section == 0) {
        ZLHomeCollectionViewCell *homeCell = [collectionView dequeueReusableCellWithReuseIdentifier:kHomeCollectionViewCellUID
                                                                                       forIndexPath:indexPath];
        homeCell.titleLabel.text = @"COUPONS";
        homeCell.itemImageView.image = [UIImage imageNamed:@"home_deals_icon"];
        cell = homeCell;
    }
    else if (indexPath.section == 1) {
        
        ZLHomeCollectionViewCell *homeCell = [collectionView dequeueReusableCellWithReuseIdentifier:kHomeCollectionViewCellUID
                                                                                       forIndexPath:indexPath];
        homeCell.titleLabel.text = [self categoryTitleForIndex:indexPath.row];
        homeCell.itemImageView.image = [self categoryImageForIndex:indexPath.row];
        
        cell = homeCell;
    }
    else if (indexPath.section == 2) {
        ZLHomeCollectionViewCell *homeCell = [collectionView dequeueReusableCellWithReuseIdentifier:kHomeCollectionViewCellUID
                                                                                       forIndexPath:indexPath];
        [self fillCellOfQuickNavigationSection:homeCell forRow:indexPath.row];
        cell = homeCell;
    }
    
    return cell;
}

#pragma mark -
#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        [self.delegate homeMenuCollectionViewController:self didSelectButtonWithTag:ZLHomeMenuNavigationButtonTagCoupons];
    }
    else if (indexPath.section == 1) {
        NSString *category = [self categoryTitleForIndex:indexPath.row];
        [self.delegate homeMenuCollectionViewController:self didSelectCategory:category];
    }
    else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [self.delegate homeMenuCollectionViewController:self didSelectButtonWithTag:ZLHomeMenuNavigationButtonTagEvents];
        }
        else if (indexPath.row == 1) {
            [self.delegate homeMenuCollectionViewController:self didSelectButtonWithTag:ZLHomeMenuNavigationButtonTagSearch];
        }
    }
}

#pragma mark -
#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(self.view.bounds.size.width, kItemHeight);
    }
    else if (indexPath.section == 1) {
        return CGSizeMake((self.view.bounds.size.width - ZLHomeMenuCollectionViewMinInteritemSpacing) / 2.0, kItemHeight);
    }
    else if (indexPath.section == 2) {
        return CGSizeMake((self.view.bounds.size.width - ZLHomeMenuCollectionViewMinInteritemSpacing) / 2.0, kItemHeight);
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return kMinLineSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return ZLHomeMenuCollectionViewMinInteritemSpacing;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    if (section != 0) {
        return UIEdgeInsetsMake(kMinLineSpacing, 0.0, 0.0, 0.0);
    }
    else {
        return UIEdgeInsetsZero;
    }
}


#pragma mark -
#pragma mark Private

- (NSString *)categoryTitleForIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return @"SHOPPING";
        case 1:
            return @"DINING";
        case 2:
            return @"SERVICES";
        case 3:
            return @"RECREATION";
        case 4:
            return @"NIGHTLIFE";
        case 5:
            return @"ENTERTAINMENT";
        default:
            NSAssert(NO, @"Invalid param!");
            return nil;
    }
}

- (UIImage *)categoryImageForIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return [UIImage imageNamed:@"shopping_category"];
        case 1:
            return [UIImage imageNamed:@"dining_category"];
        case 2:
            return [UIImage imageNamed:@"services_category"];
        case 3:
            return [UIImage imageNamed:@"recreation_category"];
        case 4:
            return [UIImage imageNamed:@"nightlife_category"];
        case 5:
            return [UIImage imageNamed:@"entertainment_category"];
        default:
            NSAssert(NO, @"Invalid param!");
            return nil;
    }
}

- (void)fillCellOfQuickNavigationSection:(ZLHomeCollectionViewCell *)cell forRow:(NSUInteger)row
{
    cell.titleLabel.text = [self buttonTitleForQuickNavigationSectionWithRow:row];
    
    if (row == 0) {
        cell.itemImageView.image = [UIImage imageNamed:@"home_event_icon"];
    }
    else if (row == 1) {
        cell.itemImageView.image = [UIImage imageNamed:@"home_search_icon"];
    }
}

- (NSString *)buttonTitleForQuickNavigationSectionWithRow:(NSUInteger)row
{
    switch (row) {
        case 0:
            return @"EVENTS";
        case 1:
            return @"SEARCH";
        default:
            NSAssert(NO, @"Invalid param!");
            return @"";
    }
}

@end
