//
//  ZLHomeCollectionViewCell.h
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/6/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

@import UIKit;

@interface ZLHomeCollectionViewCell : UICollectionViewCell

@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UIImageView *itemImageView;

@end
