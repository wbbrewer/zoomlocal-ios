//
//  ZLHomeCollectionReusableView.m
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/7/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

#import "ZLHomeCollectionReusableView.h"


@implementation ZLHomeCollectionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (nil != self) {
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5f];
    }
    return self;
}

@end
