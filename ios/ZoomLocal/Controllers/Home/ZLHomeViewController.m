//
//  ZLHomeViewController.m
//  ZoomLocal
//
//  Created by Klishevich, Yauheni on 07/06/2016.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLHomeViewController.h"
#import "ZLMyAreaViewController.h"

#import "UIImageView+WebCache.h"
#import "KMKOneRowCollectionView.h"
#import "KMKUniversalCollectionViewCellWithBorder.h"
#import "KDCycleBannerView.h"

#import "ZLHomeMenuCollectionViewController.h"
#import "ZLNavigationManager.h"
#import "ZLWPApiClient.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"
#import "ZLConstants.h"
#import "ZLVendorDetailsModel.h"
#import "ZLErrorHandler.h"

NSString *kCollectionViewBannerCellID = @"BannerCellID";


static NSString *const kStoryboardSegueShowBannerVendor = @"showBannerVendor";


@interface ZLHomeViewController () <ZLHomeMenuCollectionViewControllerDelegate,
                                    KMKOneRowCollectionViewDatasource,
                                    KMKOneRowCollectionViewDelegate>

@property (strong, nonatomic) NSArray *bannerVendors;

@property (weak, nonatomic) IBOutlet KDCycleBannerView *bannerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *bannerActivityIndicator;
@property (weak, nonatomic) IBOutlet KMKOneRowCollectionView *bannerOneRowCollectionView;

@end


@implementation ZLHomeViewController {
    NSString *_pressedCategory;
    TermModel *_tappedVendorOnBanner;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    ZLHomeMenuCollectionViewController *homeMenuVC = (ZLHomeMenuCollectionViewController *)self.childViewControllers[0];
    NSParameterAssert([homeMenuVC isKindOfClass:[ZLHomeMenuCollectionViewController class]]);
    homeMenuVC.delegate = self;
    
    _bannerView.autoPlayTimeInterval = 5;

    [self downloadBannerVendor];
    
    [_bannerOneRowCollectionView registerClass:[KMKUniversalCollectionViewCellWithBorder class]
                    forCellWithReuseIdentifier:kCollectionViewBannerCellID];
    _bannerOneRowCollectionView.pagingEnabled = YES;
}

- (void)viewDidLayoutSubviews
{
//    self.bannerView.frame = self.bannerContainerView.bounds;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kStoryboardSegueShowBannerVendor]) {
        UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
        ZLRetailerDetailsTopTabBarViewController *rtbvc = (ZLRetailerDetailsTopTabBarViewController *)nc.topViewController;
        rtbvc.vendorTermId = _tappedVendorOnBanner.termId;
        rtbvc.navigationItem.leftBarButtonItem =
        [[UIBarButtonItem alloc] initWithTitle:@"Close"
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(closeButtonOnRetailerViewControllerPressed)];
    }
}

#pragma mark <ZLHomeMenuCollectionViewControllerDelegate>

- (void)homeMenuCollectionViewController:(ZLHomeMenuCollectionViewController *)viewController
                       didSelectCategory:(NSString *)category
{
    _pressedCategory = category;
//    [self performSegueWithIdentifier:@"showMyArea" sender:self];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ZLMyAreaViewController *vendorListVC = [storyboard instantiateViewControllerWithIdentifier:@"MyAreaViewController"];
    vendorListVC.productsCategory = category;
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vendorListVC];
    nc.navigationBar.barTintColor = [UIColor tealColor];
    nc.navigationBar.tintColor = [UIColor whiteColor];
    [self presentViewController:nc animated:YES completion:^{
        ;
    }];
}

- (void)homeMenuCollectionViewController:(ZLHomeMenuCollectionViewController *)viewController
                         didSelectButtonWithTag:(ZLHomeMenuNavigationButtonTag)buttonTag;
{
    if (buttonTag == ZLHomeMenuNavigationButtonTagEvents) { // events
        [[ZLNavigationManager sharedManager] showEventsPartition];
    }
    else if (buttonTag == ZLHomeMenuNavigationButtonTagCoupons) {
        [[ZLNavigationManager sharedManager] showDealsPartition];
    }
    else if (buttonTag == ZLHomeMenuNavigationButtonTagSearch) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"SearchNavigationCntr"];
        [self presentViewController:navigationController animated:YES completion:NULL];
    }
}

#pragma mark - KDCycleBannerViewDataSource

- (NSArray *)numberOfKDCycleBannerView:(KDCycleBannerView *)bannerView
{
    NSMutableArray *bannerImageURLs = [NSMutableArray arrayWithCapacity:self.bannerVendors.count];
    for (TermModel *vendorTermModel in self.bannerVendors) {
        [bannerImageURLs addObject:[vendorTermModel.vendor.bannerImages.medium firstObject]];
    }
    
    return bannerImageURLs;
}

- (UIViewContentMode)contentModeForImageIndex:(NSUInteger)index
{
    return UIViewContentModeScaleAspectFit;
}

#pragma mark - KDCycleBannerViewDelegate

- (void)cycleBannerView:(KDCycleBannerView *)bannerView didSelectedAtIndex:(NSUInteger)index
{
    [self bannerTappedWithVendorTermModel:_bannerVendors[index]];
}

#pragma mark Actions

- (void)closeButtonOnRetailerViewControllerPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private -

- (void)downloadBannerVendor
{
    [_bannerActivityIndicator startAnimating];
    _bannerActivityIndicator.hidden = NO;
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"yes" forKey:@"banner"];

    [[ZLWPApiClient sharedClient] GET:@"wp-json/rest/v2/vendor"
                           parameters:parameters
                           completion:^(OVCResponse *response, NSError *error)
     {
         [_bannerActivityIndicator stopAnimating];
          _bannerActivityIndicator.hidden = YES;
         
         if (error == nil) {
             _bannerVendors = (NSArray *)response.result;
             [_bannerView reloadDataWithCompleteBlock:nil];
         }
         else {
             DDLogError(@"Error: '%@'", error);
             [[ZLErrorHandler sharedErrorHandler] handleError:error];
         }
     }];
}

- (void)bannerTappedWithVendorTermModel:(TermModel *)vendorTermModel
{
    _tappedVendorOnBanner = vendorTermModel;
    [self performSegueWithIdentifier:kStoryboardSegueShowBannerVendor sender:self];
}

@end
