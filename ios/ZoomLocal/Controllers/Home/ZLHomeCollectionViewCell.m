//
//  ZLHomeCollectionViewCell.m
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/6/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

#import "ZLHomeCollectionViewCell.h"

static CGFloat const kLabelHeight = 18.0;
static CGFloat const kMargin = 8.0;
static CGSize const kImageViewSize = {40.0, 40.0};


@interface ZLHomeCollectionViewCell () {
    NSString *_imageName;
}

@end


@implementation ZLHomeCollectionViewCell

@synthesize titleLabel = _titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (nil != self)
    {
        _itemImageView = [UIImageView new];
        _itemImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
        
        [self.contentView addSubview:_itemImageView];
        [self.contentView addSubview:_titleLabel];
    }
    
    return self;
}


#pragma mark -
#pragma mark UIView overload

- (void)layoutSubviews
{
    const CGFloat width = CGRectGetWidth(self.bounds);
    const CGFloat height = CGRectGetHeight(self.bounds);
    const CGFloat topMargin = (height - (kImageViewSize.height + kMargin + kLabelHeight)) / 2;
    
    _itemImageView.frame = CGRectMake((width - kImageViewSize.width) / 2,
                                  topMargin,
                                  kImageViewSize.width,
                                  kImageViewSize.height);
    
    _titleLabel.frame = CGRectMake(0.0, height - (kLabelHeight + topMargin), width, kLabelHeight);
}


#pragma mark -
#pragma mark UICollectionViewCell overload

- (void)setHighlighted:(BOOL)highlighted
{
    self.backgroundColor = highlighted ? [UIColor colorWithWhite:1.0 alpha:0.5f] : [UIColor clearColor];
    
    [super setHighlighted:highlighted];
}

@end
