//
//  ZLHomeCollectionReusableView.h
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/7/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZLHomeCollectionReusableView : UICollectionReusableView

@end
