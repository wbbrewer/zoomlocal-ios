//
//  ZLHomeCollectionViewFlowLayout.m
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/6/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

#import "ZLHomeCollectionViewFlowLayout.h"
#import "ZLHomeCollectionReusableView.h"

const CGFloat ZLHomeMenuCollectionViewMinInteritemSpacing = 13.0;

static NSString *const kDecorationViewKindVertical = @"Vertical";
static NSString *const kDecorationViewKindHorizontal = @"Horizontal";


@interface ZLHomeCollectionViewFlowLayout()

- (BOOL)indexPathIsNotLastInLine:(NSIndexPath *)indexPath;
- (BOOL)indexPathIsNotInLastLine:(NSIndexPath *)indexPath;

@end


@implementation ZLHomeCollectionViewFlowLayout


#pragma mark -
#pragma mark UICollectionViewLayout overload

- (void)prepareLayout
{
    [self registerClass:[ZLHomeCollectionReusableView class] forDecorationViewOfKind:kDecorationViewKindVertical];
    [self registerClass:[ZLHomeCollectionReusableView class] forDecorationViewOfKind:kDecorationViewKindHorizontal];
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind
                                                                  atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes *layoutAttributes =
    [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind
                                                                withIndexPath:indexPath];
    
    const CGRect baseFrame = cellAttributes.frame;
    const CGFloat lineWeight = 1.0;

    CGRect layoutFrame = CGRectZero;
    
    if ([decorationViewKind isEqualToString:kDecorationViewKindVertical]) {
        const CGFloat spaceBetweenCells = ZLHomeMenuCollectionViewMinInteritemSpacing;
        
        layoutFrame = CGRectMake(baseFrame.origin.x + baseFrame.size.width + (spaceBetweenCells - lineWeight) / 2,
                                 baseFrame.origin.y,
                                 lineWeight,
                                 baseFrame.size.height);
    }
    else if ([decorationViewKind isEqualToString:kDecorationViewKindHorizontal]) {
        const CGFloat spaceBetweenCells = self.minimumLineSpacing;
        
        layoutFrame = CGRectMake(baseFrame.origin.x,
                                 baseFrame.origin.y + baseFrame.size.height + (spaceBetweenCells - lineWeight) / 2,
                                 baseFrame.size.width,
                                 lineWeight);
    }
    
    layoutAttributes.frame = layoutFrame;
    layoutAttributes.zIndex = -1;
    
    return layoutAttributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *baseLayoutAttributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray *layoutAttributes = [baseLayoutAttributes mutableCopy];
    
    for (UICollectionViewLayoutAttributes *thisLayoutItem in baseLayoutAttributes)
    {
        if (thisLayoutItem.representedElementCategory == UICollectionElementCategoryCell) {
            if ([self indexPathIsNotLastInLine:thisLayoutItem.indexPath]) {
                UICollectionViewLayoutAttributes *newVerticalLayoutItem =
                [self layoutAttributesForDecorationViewOfKind:kDecorationViewKindVertical
                                                  atIndexPath:thisLayoutItem.indexPath];
                
                [layoutAttributes addObject:newVerticalLayoutItem];
            }

            if ([self indexPathIsNotInLastLine:thisLayoutItem.indexPath]) {
                UICollectionViewLayoutAttributes *newHorizontalLayoutItem =
                [self layoutAttributesForDecorationViewOfKind:kDecorationViewKindHorizontal
                                                  atIndexPath:thisLayoutItem.indexPath];
                
                [layoutAttributes addObject:newHorizontalLayoutItem];
            }
        }
    }
    
    return layoutAttributes;
}


#pragma mark -
#pragma mark private

- (BOOL)indexPathIsNotLastInLine:(NSIndexPath *)indexPath
{
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:(indexPath.row + 1) inSection:indexPath.section];
    
    UICollectionViewLayoutAttributes *cellAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
    UICollectionViewLayoutAttributes *nextCellAttributes = [self layoutAttributesForItemAtIndexPath:nextIndexPath];
    
    return cellAttributes.frame.origin.y == nextCellAttributes.frame.origin.y;
}

- (BOOL)indexPathIsNotInLastLine:(NSIndexPath *)indexPath
{
    NSInteger numOfSections = [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView];
    
    if (indexPath.section != numOfSections - 1) {
        return YES;
    }
    else {
        NSInteger lastItemRow = [self.collectionView.dataSource collectionView:self.collectionView
                                                        numberOfItemsInSection:indexPath.section] - 1;
        
        NSIndexPath *lastItem = [NSIndexPath indexPathForItem:lastItemRow inSection:indexPath.section];
        
        UICollectionViewLayoutAttributes *lastItemAttributes = [self layoutAttributesForItemAtIndexPath:lastItem];
        UICollectionViewLayoutAttributes *thisItemAttributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        
        return lastItemAttributes.frame.origin.y != thisItemAttributes.frame.origin.y;
    }
}

@end
