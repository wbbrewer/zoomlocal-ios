//
//  ZLHomeMenuCollectionViewControllerDelegate.h
//  Catalog Navigator
//
//  Created by Donov, Eugene on 12/14/14.
//  Copyright (c) 2015 Mobile Guide, LLC. All rights reserved.
//

@class ZLCategory;
@class ZLHomeMenuCollectionViewController;

typedef NS_ENUM(NSInteger, ZLHomeMenuNavigationButtonTag) {
    ZLHomeMenuNavigationButtonTagEvents = 0,
    ZLHomeMenuNavigationButtonTagCoupons = 1,
    ZLHomeMenuNavigationButtonTagSearch = 2
};


@protocol ZLHomeMenuCollectionViewControllerDelegate <NSObject>

@required
- (void)homeMenuCollectionViewController:(ZLHomeMenuCollectionViewController *)viewController
                       didSelectCategory:(NSString *)category;
- (void)homeMenuCollectionViewController:(ZLHomeMenuCollectionViewController *)viewController
                  didSelectButtonWithTag:(ZLHomeMenuNavigationButtonTag)buttonRow;

@end
