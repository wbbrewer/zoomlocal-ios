//
//  SearchViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 10/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "SearchViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "TagDataLayer.h"
#import "TAGManager.h"

#import "ProductModel.h"
#import "TermModel.h"

#import "ZLProductCollectionViewCell.h"
#import "VendorCollectionViewCell.h"

#import "ZLWPApiClient.h"

#import "LocationService.h"
#import "ListingDetailsViewController.h"
#import "EventDetailsViewController.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"


static NSString *const kListingCellReuseIdentifier = @"ListingCell";
static NSString *const kEventCellReuseIdentifier = @"EventCell";
static NSString *const kVendorCellReuseIdentifier = @"VendorCell";

static double kResponsiveCellHeightCompact = 128.0;
static double kResponsiveCellHeightRegular = 156.0;


@interface SearchViewController () <UICollectionViewDataSource,
                                    UICollectionViewDelegateFlowLayout,
                                    UISearchBarDelegate>

@property (strong, nonatomic) NSMutableArray *results;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSArray *scope;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end


@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular)
    {
        self.collectionView.contentInset = UIEdgeInsetsMake(4, 4, 4, 4); // top, left, bottom, right
        
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 4;
        self.collectionView.collectionViewLayout = flow;
    } else {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        //flow.itemSize = CGSizeMake(cellWidth, cellHeight);
        //flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flow.minimumInteritemSpacing = 1;
        flow.minimumLineSpacing = 1;
        self.collectionView.collectionViewLayout = flow;
    }
    
    // Register cell classes
    UINib *productCellNib = [UINib nibWithNibName:NSStringFromClass([ZLProductCollectionViewCell class]) bundle:nil];
    [self.collectionView registerNib:productCellNib
          forCellWithReuseIdentifier:kListingCellReuseIdentifier];

    
//    [self.collectionView registerNib: [UINib nibWithNibName:@"EventCollectionViewCell" bundle:nil]
//          forCellWithReuseIdentifier: kEventCellReuseIdentifier];
    
    [self.collectionView registerNib: [UINib nibWithNibName:@"VendorCollectionViewCell" bundle:nil]
          forCellWithReuseIdentifier: kVendorCellReuseIdentifier];
    
    [[LocationService sharedInstance] addObserver:self forKeyPath:@"currentLocation" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _scope = [NSArray arrayWithObjects:@"listing", @"event", @"vendor", nil];
    [_searchBar sizeToFit];
    [_searchBar setShowsScopeBar:YES];
    [_searchBar setScopeButtonTitles:[NSArray arrayWithObjects:@"Listings", @"Events", @"Retailers", nil]];
    [_searchBar setScopeBarBackgroundImage:[UIImage new]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Search"}];
}

#pragma mark - <UISearchBarDelegate>

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self fetch];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    DDLogDebug(@"%ld", (long)selectedScope);
    [self fetch];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - UICollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _results.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch([_searchBar selectedScopeButtonIndex]) {
        case 0:
        {
            // Listing
            ZLProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: kListingCellReuseIdentifier forIndexPath: indexPath];
            cell.productModel = _results[indexPath.row];
            return cell;
        }
            
        case 1:
        {
            // Event
            ZLProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: kListingCellReuseIdentifier forIndexPath: indexPath];
            cell.productModel = _results[indexPath.row];
            return cell;
        }
            
        case 2:
        {
            //Vendor
            VendorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: kVendorCellReuseIdentifier forIndexPath: indexPath];
            
            // Configure the cell
            TermModel *model = _results[indexPath.row];
            
            [cell.vendorImageView sd_setImageWithURL:[NSURL URLWithString:[model.vendor.images.thumbnail firstObject]]
                                    placeholderImage:[UIImage imageNamed:@"shop_placeholder"]];
            [cell.vendorNameLabel setText: model.name];
            [cell.vendorAddressLabel setText: model.vendor.address1];
            [cell.vendorLocationLabel setText:[NSString stringWithFormat:@"%@, %@", model.vendor.city, model.vendor.state]];
            CLLocationDistance distance = [[LocationService sharedInstance].currentLocation distanceFromLocation:model.vendor.location] * 0.000621371192;
            [cell.vendorDistanceLabel setText:[NSString stringWithFormat:@"%0.1f %@", distance, @"miles"]];
            
            return cell;
            
            break;
        }
            
        default:
            return nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    
    // iPad
    if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular)
    {
        cellSize.height = kResponsiveCellHeightRegular;
        cellSize.width = (self.view.bounds.size.width/2) - 6;
    } else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact)
    {
        cellSize.height = kResponsiveCellHeightRegular;
        cellSize.width = (self.view.bounds.size.width/2) - 0.5;
    }
    else if (self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact && self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact)
    {
        cellSize.height = kResponsiveCellHeightCompact;
        cellSize.width = self.view.bounds.size.width;
    } else {
        cellSize.height = kResponsiveCellHeightCompact;
        cellSize.width = self.view.bounds.size.width;
    }
    
    return cellSize;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    // Update the view.
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    switch([_searchBar selectedScopeButtonIndex]) {
        case 0:
        {
            ProductModel *product = self.results[indexPath.row];
            [self performSegueWithIdentifier:@"showListingDetails"
                                      sender:product];
            
            break;
        }
        case 1:
        {
            ProductModel *product = self.results[indexPath.row];
            [self performSegueWithIdentifier:@"showEventDetails"
                                      sender:product];
            break;
        }
        case 2:
        {
            TermModel *model = self.results[indexPath.row];
            [self performSegueWithIdentifier:@"showVendor" sender:model];
            break;
        }
    }
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showListingDetails"]) {
        ListingDetailsViewController *controller = (ListingDetailsViewController *)[segue destinationViewController];
        controller.productModel = sender;
    }
    else if ([[segue identifier] isEqualToString:@"showEventDetails"]) {
        EventDetailsViewController *controller = (EventDetailsViewController *)[segue destinationViewController];
        controller.productModel = sender;
    }
    else  if ([[segue identifier] isEqualToString:@"showVendor"]) {
        ZLRetailerDetailsTopTabBarViewController *controller = (ZLRetailerDetailsTopTabBarViewController *)[segue destinationViewController];
        TermModel *vendorTerm = (TermModel *)sender;
        controller.vendorTermId = vendorTerm.termId;
    }
}

#pragma mark Actions

- (IBAction)closeBarButtonPressed:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Downloading

- (void)fetch {
    if (self.searchBar.text.length >= 3) {
        
        self.theResponse = nil;
        self.theError = nil;
        self.busy = YES;
        [_results removeAllObjects];
        
        // Update the view.
        [self.collectionView reloadData];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        if (_searchBar.selectedScopeButtonIndex == 2 ) {
            // vendor
            [parameters setObject:self.searchBar.text forKey:@"search"];
        } else {
            [parameters setObject:self.searchBar.text forKey:@"filter[s]"];
        }
        [parameters setObject:@"0" forKey:@"per_page"];
        
        DDLogDebug(@"fetch: %@", self.searchBar.text);
        [[ZLWPApiClient sharedClient] GET: [NSString stringWithFormat:@"wp-json/rest/v2/%@", _scope[[_searchBar selectedScopeButtonIndex]]]
                             parameters: parameters
                             completion: ^(OVCResponse *response, NSError *error) {
                                 
                                 self.busy = NO;
                                 self.theResponse = response;
                                 self.theError = error;
                                 
                                 if (error == nil) {
                                     _results = (NSMutableArray *)response.result;
                                 } else {
                                     DDLogError(error.localizedDescription);
                                 }
                                 
                                 
                                 // Update the view.
                                 [self.collectionView reloadData];
                             }];
    }
}

#pragma mark -

- (void)observeValueForKeyPath:(NSString*)keyPath
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    if ([keyPath isEqualToString:@"currentLocation"]) {
        // Update the view.
        [self.collectionView reloadData];
    }
}

@end
