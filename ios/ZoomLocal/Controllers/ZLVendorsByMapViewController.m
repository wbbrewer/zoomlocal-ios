//
//  VendorsByMapViewController.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/7/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLVendorsByMapViewController.h"

@import MapKit;
#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "TagDataLayer.h"
#import "TAGManager.h"

#import "ZLConstants.h"
#import "LocationService.h"
#import "ZLWPApiClient.h"
#import "TermModel.h"
#import "ZLRetailerDetailsTopTabBarViewController.h"


@interface ZLVendorsByMapViewController ()<MKMapViewDelegate>

@property (nonatomic, strong) NSMutableArray *results;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation ZLVendorsByMapViewController

#pragma mark UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitleView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zl_logo"]]];
    
    // Do any additional setup after loading the view.
    [[LocationService sharedInstance] addObserver:self
                                       forKeyPath:@"currentLocation"
                                          options:NSKeyValueObservingOptionNew
                                          context:nil];
    
    //
    [self.mapView setShowsUserLocation:YES];
    [self.mapView setShowsPointsOfInterest:NO];
    
    [_mapView setRegion:ZLDefaultMapRegion animated:YES];
    
    [self fetch];
}

- (void)dealloc
{
    [[LocationService sharedInstance] removeObserver:self forKeyPath:@"currentLocation"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showVendor"]) {
        ZLRetailerDetailsTopTabBarViewController *controller = (ZLRetailerDetailsTopTabBarViewController *)[segue destinationViewController];
        TermModel *vendorTerm = (TermModel *)sender;
        controller.vendorTermId = vendorTerm.termId;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // The container should have already been opened, otherwise events pushed to
    // the data layer will not fire tags in that container.
    TAGDataLayer *dataLayer = [TAGManager instance].dataLayer;
    [dataLayer push:@{@"event": @"openScreen", @"screenName": @"Vendors by map"}];
}

#pragma mark <MKMapViewDelegate>

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *identifier = @"MyAnnotationView";
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    DDLogDebug(@"annotation: %@",annotation);
    
    MKPinAnnotationView *view = (id)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view) {
        view.annotation = annotation;
    } else {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        view.canShowCallout = true;
        view.animatesDrop = false;
        view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

    }
    
    return view;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (TermModel *model in _results) {
        if ([model.vendor.name isEqualToString:view.annotation.title]) {
            //
            DDLogDebug(@"view: %@",view.annotation.title);
            [self performSegueWithIdentifier:@"showVendor" sender:model];
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    // TODO: Requery data when we have more vendors.
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    for (UIView *view in views) {
        [self addBounceAnnimationToView:view];
    }
}

- (void)addBounceAnnimationToView:(UIView *)view
{
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    DDLogDebug(@"mapView didUpdateUserLocation: %@", userLocation);
        //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 5000, 5000);
        //[self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}


#pragma mark NSObject(NSKeyValueObserving)

- (void)observeValueForKeyPath:(NSString*)keyPath
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context
{
    if ([keyPath isEqualToString:@"currentLocation"]) {
        
        //DDLogDebug(@"currentLocation: %@", self.currentLocation);
    }
}


#pragma mark - Private -

#pragma mark Helpers

- (void)fetch {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //Fetch all the vendors with items to show them in a MapView
    [[ZLWPApiClient sharedClient] GET:@"wp-json/rest/v2/vendor"
                           parameters:@{@"hide_empty" : @"true", @"orderby" : @"slug", @"per_page" : @"0"}
                           completion:^(OVCResponse *response, NSError *error) {
                               
                               if (!error) {
                                   // initialize
                                   _results = (NSMutableArray *)response.result;
                                   
                                   // store the items into the existing list
                                   for (TermModel *model in _results) {
                                       
                                       if (model.vendor.location.coordinate.latitude != 0 && model.vendor.location.coordinate.longitude != 0) {
                                           
                                           // Add an annotation.
                                           MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                                           
                                           point.coordinate = model.vendor.location.coordinate;
                                           
                                           point.title = model.vendor.name;
                                           
                                           point.subtitle = model.vendor.displayAddress;
                                           
                                           [_mapView addAnnotation:point];
                                       }
                                   }
                                   
                                   [self mapView:self.mapView regionDidChangeAnimated:NO];
                                   
                                   MKMapRect zoomRect = MKMapRectNull;
                                   for (id <MKAnnotation> annotation in _mapView.annotations)
                                   {
                                       MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
                                       MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
                                       zoomRect = MKMapRectUnion(zoomRect, pointRect);
                                   }
                                   [_mapView setVisibleMapRect:zoomRect animated:YES];
                                   
                               } else {
                                   DDLogError(error.localizedDescription);
                               }
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                               });
                           }];
}

- (BOOL)isShowingHUD:(UIView *)view {
    
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    for (UIView *subview in subviews) {
        if([subview isKindOfClass:[MBProgressHUD class]]) {
            return YES;
        }
        // Do what you want to do with the subview
        //DDLogDebug(@"%@", subview);
    }
    return NO;
}

@end
