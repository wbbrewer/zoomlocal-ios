//
//  FilterByDistanceContainerViewController.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 13/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "FilterByDistanceContainerViewController.h"

#import "ZLConstants.h"

const CGFloat ZLFilterByDistanceHeight = 54.0;


@interface FilterByDistanceContainerViewController ()

@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;

@end


@implementation FilterByDistanceContainerViewController

#pragma mark - Public -

- (void)setRadiusOfSearchInMiles:(float)radiusOfSearchInMiles
{
    _radiusOfSearchInMiles = radiusOfSearchInMiles;
    if (self.isViewLoaded) {
        _radiusLabel.attributedText = [self descriptionForRadius:_radiusOfSearchInMiles andLocation:_locationByName];
        [_radiusLabel.superview setNeedsLayout];
    }
}

#pragma mark - Override -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _radiusLabel.attributedText = [self descriptionForRadius:_radiusOfSearchInMiles andLocation:_locationByName];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

#pragma mark - Helpers -

- (NSAttributedString *)descriptionForRadius:(float)radius andLocation:(NSString *)locationByName
{
    NSMutableAttributedString *description = [[NSMutableAttributedString alloc] init];
    
    [description appendAttributedString:[[NSAttributedString alloc] initWithString:@"Viewing "
                                                                        attributes:@{
                                                                                     NSForegroundColorAttributeName : [UIColor blueTintColor],
                                                                                     NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0]
                                                                                     }]];
    
    [description appendAttributedString:[[NSAttributedString alloc] initWithString:[[[NSNumberFormatter radiusFormatter] stringFromNumber:@(radius)] stringByAppendingString:@"mi"]
                                                                        attributes:@{
                                                                                     NSForegroundColorAttributeName : [UIColor grayTextColor],
                                                                                     NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0]
                                                                                     }]];
    
    return description;
}

@end