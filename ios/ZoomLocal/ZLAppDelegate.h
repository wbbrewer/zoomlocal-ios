//
//  AppDelegate.h
//  ZoomLocal
//
//  Created by Aric Brown on 11/19/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TAGManager;
@class TAGContainer;


@interface ZLAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) TAGManager *tagManager;
/// Google Tag Manager container.
@property (nonatomic, strong) TAGContainer *GTMContainer;

@property (strong, nonatomic) UIWindow *window;

@end

