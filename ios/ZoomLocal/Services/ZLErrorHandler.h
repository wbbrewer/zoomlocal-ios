//
//  ZLErrorHandler.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 05/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 Used to handle any error that needs to be handled in more than one place in project.
 
 Some module may do not interprete some underlying error as error from its domain and pass this error to invoking module. 
 When using this approach we end up handling error from different domain in one place. At the same time many error should
 be treated in some typical way irrespective of place where they were received. This class is used for providing such typical ways.
 
 If some error should be treated in some specific way then handle it in place where is occurs and use this class for handling
 all other errors.
 */
@interface ZLErrorHandler : NSObject

#pragma mark - Singleton -

/// @return Returns the singleton instance.
+ (instancetype)sharedErrorHandler;

/// Only for use by test frameworks!
+ (void)destroyAndRecreateSingleton;

// Clue for improper use (produces compile time error)
+ (instancetype) alloc  NS_UNAVAILABLE;
- (instancetype) init   NS_UNAVAILABLE;
+ (instancetype) new    NS_UNAVAILABLE;

#pragma mark -

/**
 Notify user in some way about error.
 
 Passed error is not logged. This is the resposibility of a client code (as during logging method name is also logged).
 */
- (void)handleError:(NSError *)error;

/**
 Shows alert with title "Error",given message and button "OK".
 Can be used for handling errors that are handled by client code in a specific way.
 */
- (void)showErrorAlertWithMessage:(NSString *)message;

@end
