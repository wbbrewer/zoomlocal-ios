//
//  LocationService.m
//  ZoomLocal
//
//  Created by Aric Brown on 12/11/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "LocationService.h"

#import <GCDMulticastDelegate.h>

#import "ZLConstants.h"


@interface LocationService ()<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (assign, nonatomic) BOOL busy;

@end


@implementation LocationService
{
    id _multicastDelegate;
}

+(LocationService *)sharedInstance
{
    static LocationService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    return instance;
}

- (id)init
{
    self = [super init];
    if(self != nil) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = 5000; // meters
        self.locationManager.delegate = self;
        
        const CLLocationCoordinate2D kDefaultLocation = ZLDefaultMapRegion.center;
        _currentLocation = [[CLLocation alloc] initWithLatitude:kDefaultLocation.latitude longitude:kDefaultLocation.longitude];
        
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        _multicastDelegate = [[GCDMulticastDelegate alloc] init];
    }
    return self;
}


#pragma mark - Public

- (void)addDelegate:(id<LocationServiceDelegate>)delegate
{
    [_multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
}

- (void)removeDelegate:(id<LocationServiceDelegate>)delegate
{
    [_multicastDelegate removeDelegate:delegate];
}

- (CLLocationDistance)distanceToLocation:(CLLocation *)location
{
    return [self.currentLocation distanceFromLocation:location];
}


#pragma mark <CLLocationManagerDelegate>

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DDLogDebug(@"Location service failed with error %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
        //[self performSegueWithIdentifier:@"startSegue" sender:self];
    }
    else if (status == kCLAuthorizationStatusDenied) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location services not authorized"
        //                                                        message:@"This app needs you to authorize locations services to work."
        //                                                       delegate:nil
        //                                              cancelButtonTitle:@"Ok"
        //                                              otherButtonTitles:nil];
        //        [alert show];
    }
    else {
        DDLogDebug(@"Wrong location status");
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray*)locations
{
    CLLocation *location = [locations lastObject];
    if (location) {
        if (location.verticalAccuracy < 5000) {
            self.currentLocation = location;
            [_multicastDelegate locationService:self didUpdateLocation:location];
            //DDLogDebug(@"currentLocation: %@", self.currentLocation);
            
            if (!_busy) {
                [self reverseGeocode:location];
            }
        }
    }
}


#pragma mark -

- (void)reverseGeocode:(CLLocation *)location
{
    _busy = YES;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        DDLogDebug(@"Finding address");
        if (error) {
            _busy = NO;
            DDLogDebug(@"Error %@", error.description);
        } else {
            _busy = NO;
            CLPlacemark *placemark = [placemarks lastObject];
            self.currentPlacemark = placemark;
            //DDLogDebug(@"placemark: %@", placemark.addressDictionary);
        }
    }];
}

@end
