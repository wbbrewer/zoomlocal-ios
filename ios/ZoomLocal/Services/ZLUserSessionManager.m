//
//  ZLUserSessionManager.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 02/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLUserSessionManager.h"

#import <GCDMulticastDelegate.h>
#import <Valet/Valet.h>

#import "UserModel.h"
#import "ZLWPApiClient.h"
#import "ZLSettingsManager.h"

NSString *const ZLUserSessionManagerErrorDomain = @"com.zoomlocal.ZoomLocal.UserSessionManager";

static NSString *const kClientId = @"UGJEdVFb0PEJzyt0LEkprzFQM5GFI3";
static NSString *const kClientSecret = @"OCKyhXtW9uLNdllGl7kGwMgbC3hjI0";

static NSString *const kUsernameStorageKey = @"com.zoomlocal.ZoomLocal.keychain.username";
static NSString *const kPasswordStorageKey = @"com.zoomlocal.ZoomLocal.keychain.password";

static NSString *const kTokenStorageKey = @"com.zoomlocal.ZoomLocal.keychain.token";


#ifdef DEBUG
    #undef LOG_LEVEL_DEF
    #define LOG_LEVEL_DEF OverridenLogLevel
#warning change to debug
    static const int OverridenLogLevel = LOG_LEVEL_VERBOSE; // log level for current m-file.
#endif


@interface NSError (UserSessionManager)

+ (NSError *)unknownErrorWithUnderlyingError:(NSError *)underlyingError;
+ (NSError *)invalidCredentialsErrorWithDescription:(NSString *)description underlyingError:(NSError *)underlyingError;

@end


@interface ZLUserSessionManager ()

@property (strong, nonatomic) UserModel *user;
@property (strong, nonatomic) TokenModel *token;
@property (strong, nonatomic) NSString *loginEmail;
@property (strong, nonatomic) NSString *password;

@property (copy, nonatomic) void (^loginCompletionHandler)(NSError *);

@end


@implementation ZLUserSessionManager {
    id _multicastObserver;
}

static id sSharedSessionManager = nil;

+ (instancetype)sharedManager
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sSharedSessionManager = [[super alloc] initUniqueInstance];
    });
    return sSharedSessionManager;
}

- (instancetype)initUniqueInstance
{
    self = [super init];
    if (self != nil) {
        _multicastObserver = [[GCDMulticastDelegate alloc] init];
    }
    return self;
}

#pragma mark - Properties

- (TokenModel *)token
{
    NSData *tokenData = [[NSUserDefaults standardUserDefaults] dataForKey:kTokenStorageKey];
    TokenModel *token = [NSKeyedUnarchiver unarchiveObjectWithData:tokenData];
    return token;
}

- (void)setToken:(TokenModel *)token
{
    NSData *tokenData = [NSKeyedArchiver archivedDataWithRootObject:token];
    [[NSUserDefaults standardUserDefaults] setObject:tokenData forKey:kTokenStorageKey];
}

- (void)setLoginEmail:(NSString *)email
{
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:kUsernameStorageKey];
}

- (NSString *)loginEmail
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kUsernameStorageKey];
}

- (void)setPassword:(NSString *)password
{
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPasswordStorageKey];
}

- (NSString *)password
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kPasswordStorageKey];
}

- (ZLUserSessionManagerLoginStatus)loginStatus
{
    if (self.token != nil) {
        return ZLUserSessionManagerLoginStatusLoggedIn;
    }
    else {
        return ZLUserSessionManagerLoginStatusLoggedOut;
    }
}

#pragma mark - Public -

- (void)loginWithEmail:(NSString *)email password:(NSString *)password completion:(void (^)(NSError *))completion
{
    NSParameterAssert(email);
    NSParameterAssert(password);
    
    if (self.loginCompletionHandler == nil) {
        self.loginCompletionHandler = completion;
        
        NSDictionary *params = @{@"grant_type" : @"password",
                                 @"client_id" : kClientId,
                                 @"client_secret" : kClientSecret,
                                 @"username" : email,
                                 @"password" : password};
        
        [[ZLWPApiClient sharedClient] POST:@"oauth/token"
                              parameters:params
                              completion:^(OVCResponse *response, NSError *error) {

                                  if (error == nil) {
                                      self.loginEmail = email;
                                      self.password = password;
                                      
                                      self.token = response.result;
                                      DDLogVerbose(@"Access token: '%@'", self.token.accessToken);
                                      [self downloadUserId];
                                  }
                                  else {
                                      NSError *errorToPass = [self tryToTranslateReceivingTokenError:error];
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          self.loginCompletionHandler(errorToPass);
                                          self.loginCompletionHandler = nil;
                                      });
                                  }
                              }];
    }
    else {
        NSAssert(NO, @"App error!");
    }
}

- (void)logout
{
    self.loginEmail = nil;
    self.password = nil;

    [self sendLogoutRequest];
}

- (void)refreshAccessToken:(void (^)(NSError *error))completion
{
    if (self.token.refreshToken == nil) {
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey : @"Attempt to update access token when there is no refresh token."
                                   };
        NSError *error = [NSError errorWithDomain:ZLUserSessionManagerErrorDomain
                                             code:ZLUserSessionManagerErrorNoRefreshToken
                                         userInfo:userInfo];
        completion(error);
    }
    else {
        NSDictionary *params = @{@"grant_type" : @"refresh_token",
                                 @"client_id" : kClientId,
                                 @"client_secret" : kClientSecret,
                                 @"refresh_token" : self.token.refreshToken};
        
        [[ZLWPApiClient sharedClient] POST:@"oauth/token"
                              parameters:params
                              completion:^(OVCResponse *response, NSError *error) {
                                  
                                  if (error == nil) {
                                      self.token = response.result;
                                      DDLogVerbose(@"result: %@", response.result);                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          completion(nil);
                                      });
                                  }
                                  else {
                                      DDLogError(@"Error: '%@'", error);
                                      
                                      // if refresh token has expired error with "status code: 400" occurs and JSON with content:
                                      // { error = "invalid_grant"; "error_description" = "Refresh token has expired";}
                                      BOOL isRefreshTokenExpiredError = [response.result[@"error_description"] isEqualToString:@"Refresh token has expired"];
                                      
                                      if (isRefreshTokenExpiredError) {
                                          [self loginWithEmail:self.loginEmail
                                                      password:self.password
                                                    completion:^(NSError *error) {
                                                        completion(error);
                                                    }];
                                      }
                                      else {
                                          completion(error);
                                      }
                                  }
                              }];
    }
}

- (void)addUserSessionObserver:(id<ZLUserSessionManagerObserver>)observer
{
    [_multicastObserver addDelegate:observer delegateQueue:dispatch_get_main_queue()];
}

- (void)removeUserSessionObserver:(id<ZLUserSessionManagerObserver>)observer
{
    [_multicastObserver removeDelegate:observer];
}

#pragma mark - Helpers -

- (void)downloadUserId
{
    [[ZLWPApiClient sharedClient] GET:@"oauth/me"
                         parameters:@{@"access_token" : self.token.accessToken}
                         completion:^(OVCResponse *response, NSError *error) {
                             
                             if (error == nil) {
                                 [self downloadUserByUserId:[response.result valueForKeyPath:@"ID"]];
                             }
                             else {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     self.loginCompletionHandler(error);
                                     self.loginCompletionHandler = nil;
                                 });
                             }
                         }];
}

- (void)downloadUserByUserId:(NSNumber *)userId
{
    [[ZLWPApiClient sharedClient] GET:[NSString stringWithFormat:@"wp-json/wp/v2/users/%@", userId]
                         parameters:@{@"access_token" : self.token.accessToken}
                         completion:^(OVCResponse *response, NSError *error) {
        
        if (error == nil) {
            self.user = response.result;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.loginCompletionHandler(nil);
                self.loginCompletionHandler = nil;
                [_multicastObserver userSessionManager:self didChangeLoginStatusTo:ZLUserSessionManagerLoginStatusLoggedIn];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.loginCompletionHandler(error);
                self.loginCompletionHandler = nil;
            });
        }
    }];
}

- (NSError *)tryToTranslateReceivingTokenError:(NSError *)error
{
    NSError *resultingError = error;
    
    NSError *loginError = nil;
    
    // TODO: error from `AFURLResponseSerializationErrorDomain` should be treaded by service for making request to server.
    // This service must receive errorJSON from indicated service and interprete it in needed way.
    if ([error.domain isEqualToString:AFURLResponseSerializationErrorDomain] &&
        error.code == NSURLErrorBadServerResponse) {
        
        NSError *serializationError = nil;
        NSData *responseData = [error.userInfo valueForKey:AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *errorJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                  options:0
                                                                    error:&serializationError];
        
        if ([errorJSON[@"error"] isEqualToString:@"invalid_grant"]) { // "error_description" = "Invalid username and password combination";
            loginError = [NSError invalidCredentialsErrorWithDescription: errorJSON[@"error_description"]
                                                         underlyingError: error];
        }
        else if (serializationError != nil) {
            loginError = [NSError unknownErrorWithUnderlyingError:error];
        }
        else {
            loginError = [NSError unknownErrorWithUnderlyingError:error];
        }
    }
    
    if (loginError != nil) {
        resultingError = loginError;
    }
    
    return resultingError;
}

// Even if request fails we simply forget all tokens and when user logs in again we will request new tokens.
- (void)sendLogoutRequest
{
    NSDictionary *params = @{@"access_token" : self.token.accessToken,
                             @"refresh_token" : self.token.refreshToken};
    
    [[ZLWPApiClient sharedClient] GET:@"oauth/destroy"
                           parameters:params
                           completion:^(OVCResponse *response, NSError *error) {
                               
                               // This is fire and forget. If the call fails continue.
                               if (error == nil) {
                                   DDLogVerbose(@"result: %@", response.result);
                               }
                               else {
                                   DDLogError(@"Error: '%@'", error);
                               }
                           }];
    self.token = nil;
    [_multicastObserver userSessionManager:self didChangeLoginStatusTo:ZLUserSessionManagerLoginStatusLoggedOut];
}

@end


@implementation NSError (UserSessionManager)

+ (NSError *)unknownErrorWithUnderlyingError:(NSError *)underlyingError
{
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey : @"Unknown error.",
                               NSUnderlyingErrorKey : underlyingError
                               };
    NSError *error = [NSError errorWithDomain:ZLUserSessionManagerErrorDomain
                                         code:ZLUserSessionManagerErrorUnknownError
                                     userInfo:userInfo];
    return error;
}

+ (NSError *)invalidCredentialsErrorWithDescription:(NSString *)description underlyingError:(NSError *)underlyingError
{
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey : description,
                               NSUnderlyingErrorKey : underlyingError
                               };
    NSError *error = [NSError errorWithDomain:ZLUserSessionManagerErrorDomain
                                         code:ZLUserSessionManagerErrorInvalidCredentials
                                     userInfo:userInfo];
    return error;
}

@end


