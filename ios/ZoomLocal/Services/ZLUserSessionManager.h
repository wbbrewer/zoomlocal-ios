//
//  ZLUserSessionManager.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 02/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TokenModel.h"

@protocol ZLUserSessionManagerObserver;


extern NSString *const ZLUserSessionManagerErrorDomain;

/**
 Module do not translate all errors occuring during its work into its domain and simply passes them to invoking module.
 Use `ZLErrorHandler` class for handling the most typical errors.
 */
typedef NS_ENUM(NSInteger, ZLUserSessionManagerError) {
    ZLUserSessionManagerErrorUnknownError = -1000,
    ZLUserSessionManagerErrorInvalidCredentials = -1001,
    ZLUserSessionManagerErrorNoRefreshToken = -1002
};


typedef NS_ENUM(NSInteger, ZLUserSessionManagerLoginStatus) {
    ZLUserSessionManagerLoginStatusLoggedOut = 0,
    ZLUserSessionManagerLoginStatusLoggedIn
};


@interface ZLUserSessionManager : NSObject

#pragma mark - Singleton -

/// @return Returns the singleton instance.
+ (instancetype)sharedManager;

// Clue for improper use (produces compile time error)
+ (instancetype) alloc  NS_UNAVAILABLE;
- (instancetype) init   NS_UNAVAILABLE;
+ (instancetype) new    NS_UNAVAILABLE;

@property (readonly, strong, nonatomic) TokenModel *token;

@property (readonly, nonatomic) ZLUserSessionManagerLoginStatus loginStatus;

/**
 The second attempt to login is ignored if the first one was not completed (in progress).
 */
- (void)loginWithEmail:(NSString *)email password:(NSString *)password completion:(void (^)(NSError *))completion;
- (void)logout;

/**
 Refresh access token if there is refresh token (that is if it has been received ealier just for oncee.
 If there is no refresh token received earlier than error.
 If refresh token has been expired and there was at least one successfull login then new refresh token is requested using remembered credentials.
 */
- (void)refreshAccessToken:(void (^)(NSError *))completion;

- (void)addUserSessionObserver:(id<ZLUserSessionManagerObserver>)observer;
- (void)removeUserSessionObserver:(id<ZLUserSessionManagerObserver>)observer;

@end


@protocol ZLUserSessionManagerObserver <NSObject>

@optional
- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus;

@end
