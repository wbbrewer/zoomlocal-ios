#import "ZLNavigationManager.h"

#import "SynthesizeSingleton.h"

#import "ZLAppDelegate.h"

enum {
    ZLTabBarCotrollerIndexHome = 0,
    ZLTabBarCotrollerIndexDeals,
    ZLTabBarCotrollerIndexListings,
    ZLTabBarCotrollerIndexEvents,
    ZLTabBarControllerIndexMyArea,
    ZLTabBarCotrollerIndexSearch
};


@interface ZLNavigationManager ()

@property (readonly, nonatomic) UITabBarController *tabBarController;

@end


@implementation ZLNavigationManager

SYNTHESIZE_SINGLETON_FOR_CLASS(ZLNavigationManager, sharedManager)

- (void)showEventsPartition
{
    [self.tabBarController setSelectedIndex:ZLTabBarCotrollerIndexEvents];
}

- (void)showDealsPartition
{
    [self.tabBarController setSelectedIndex:ZLTabBarCotrollerIndexDeals];
}

- (void)goToSearchScreen
{
    
}

#pragma mark -
#pragma mark Private

/**
 @return Tab bar controller or `nil` if tab bar controller is not on screen.
 */
- (UITabBarController *)tabBarController
{
    ZLAppDelegate *appDelegate = (ZLAppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *rootVC = appDelegate.window.rootViewController;
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        return (UITabBarController *)rootVC;
    }
    else {
        return nil;
    }
}

@end
