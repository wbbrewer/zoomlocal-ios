//
//  SettingsManager.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 13/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


// Used as a rule for storing persitent settings.
@interface ZLSettingsManager : NSObject

#pragma mark - Singleton -
/**
 Singleton instance.
 */
+ (ZLSettingsManager *)sharedManager;
+(instancetype) alloc NS_UNAVAILABLE;
-(instancetype) init NS_UNAVAILABLE;
+(instancetype) new NS_UNAVAILABLE;

#pragma mark -

/// Radius of view in miles.
/// Intended for listings, events and vendors ("My Area")
@property (assign, nonatomic) float radiusOfViewingInMi;

@end
