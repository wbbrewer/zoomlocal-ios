//
//  ZLErrorHandler.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 05/02/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLErrorHandler.h"

#import "UIAlertController+Blocks.h"
#import <OVCResponse.h>
#import <HTTPStatusCodes.h>

#import "ZLAppDelegate.h"


@implementation ZLErrorHandler

static id sSharedInstance = nil;

+ (instancetype)sharedErrorHandler
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sSharedInstance = [[super alloc] initUniqueInstance];
    });
    return sSharedInstance;
}

- (instancetype)initUniqueInstance
{
    self = [super init];
    if (self != nil) {
    }
    return self;
}

+ (void)destroyAndRecreateSingleton
{
    sSharedInstance = [[super alloc] initUniqueInstance];
}

#pragma mark -

- (void)handleError:(NSError *)error
{
    if ([error.domain isEqualToString:NSURLErrorDomain]) {
        [self handleErrorFromURLErrorDomainWithError:error];
    }
    else if (error.userInfo[@"OVCResponse"] != nil) {
        OVCResponse *ovcResponse = error.userInfo[@"OVCResponse"];
        NSHTTPURLResponse *HTTPResponse = ovcResponse.HTTPResponse;
        if (HTTPResponse.statusCode == kHTTPStatusCodeUnprocessableEntity) {
            NSDictionary *errorDict = ovcResponse.result;
            [self showErrorAlertWithMessage:errorDict[@"message"]];
        }
    }
    else {
        [self showErrorAlertWithMessage:@"Unknown Error.\r\n"
         @"If this error continues to occur please contact ZoomLocal Technical Support at support@zoomlocal.com."];
    }
}

#pragma mark - Helpers -

- (void)handleErrorFromURLErrorDomainWithError:(NSError *)error
{
    if (error.code == NSURLErrorNotConnectedToInternet) {
        [self showErrorAlertWithMessage:@"No Internet connection."];
    }
    else {
        [self showErrorAlertWithMessage:error.localizedDescription];
    }
}

- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIViewController *presentingViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    while (presentingViewController.presentedViewController != nil) {
        // When trying to present on controller that is already presenting other view controller nothing happens.
        presentingViewController = presentingViewController.presentedViewController;
    }
    
    [UIAlertController showAlertInViewController:presentingViewController
                                       withTitle:@"Error"
                                         message:message
                               cancelButtonTitle:@"OK"
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil
                                        tapBlock:nil];
}

@end
