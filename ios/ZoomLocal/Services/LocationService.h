//
//  LocationService.h
//  ZoomLocal
//
//  Created by Aric Brown on 12/11/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class LocationService;


// All methods in `LocationServiceDelegate` must be required. (`GCDMulticastDelegate` limitation)
@protocol LocationServiceDelegate <NSObject>

- (void)locationService:(LocationService *)locationService didUpdateLocation:(CLLocation *)location;

@end


@interface LocationService : NSObject

+ (LocationService *)sharedInstance;

/**
 Repeated invocation of this method leads to notifying the same delegate several times.
 Be sure to invoke  @c removeObserver: before @c observer is deallocated.
 */
- (void)addDelegate:(id<LocationServiceDelegate>)delegate;
- (void)removeDelegate:(id<LocationServiceDelegate>)delegate;

// If coordinates were not received then default value is returned (Gulfport, Mississippi)
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) CLPlacemark *currentPlacemark;

- (CLLocationDistance)distanceToLocation:(CLLocation *)location;     

@end
