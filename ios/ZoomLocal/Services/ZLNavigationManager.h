#import <Foundation/Foundation.h>


@interface ZLNavigationManager : NSObject

// Here follows major interface of class.

#pragma mark Singleton
/**
 Singleton instance.
 */
+ (ZLNavigationManager *)sharedManager;

// Clue for improper use (produces compile time error)
+ (instancetype) alloc NS_UNAVAILABLE;
- (instancetype) init NS_UNAVAILABLE;
+ (instancetype) new NS_UNAVAILABLE;

- (void)showEventsPartition;
- (void)showDealsPartition;

@end
