//
//  SettingsManager.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 13/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLSettingsManager.h"

#import "NSUserDefaults+KMKUtils.h"

#import "FilterByDistanceTableViewController.h"


static NSString *const kUserDefaultsKeyRadiusForFilteringByDistance = @"RadiusForFilteringByDistance";


@implementation ZLSettingsManager {
    float _radiusOfViewingInMi;
    NSUserDefaults *_userDefaults;
}

#pragma mark - Singleton -

static id sSharedInstance = nil;

+ (instancetype)sharedManager
{
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sSharedInstance = [[super alloc] initUniqueInstance];
    });
    return sSharedInstance;
}

- (instancetype)initUniqueInstance
{
    self = [super init];
    if (self != nil) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

/**
 Motive of implementatioin:
    When moving from debug mode to release server is changed and an old credentials are invalid. But app is trying to
 use the old credentials. To avoid this we must remove old credentials.
 */
+ (void)load
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *const kDebugModeKey = @"debug_mode";
#if DEBUG
    if ([userDefaults boolForKey:kDebugModeKey] == NO) {
        [NSUserDefaults resetDefaults];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kDebugModeKey];
    }
#else
    if ([userDefaults boolForKey:kDebugModeKey]) {
        [NSUserDefaults resetDefaults];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kDebugModeKey];
    }
#endif
}

#pragma mark -

- (void)setRadiusOfViewingInMi:(float)radiusOfViewingInMi
{
    _radiusOfViewingInMi = radiusOfViewingInMi;
    [_userDefaults setDouble:radiusOfViewingInMi forKey:kUserDefaultsKeyRadiusForFilteringByDistance];
    [_userDefaults synchronize];    
}

- (float)radiusOfViewingInMi
{
    NSNumber *savedRadiusNumber = [_userDefaults objectForKey:kUserDefaultsKeyRadiusForFilteringByDistance];
    if (savedRadiusNumber != nil) {
        _radiusOfViewingInMi = savedRadiusNumber.doubleValue;
    }
    else {
        _radiusOfViewingInMi = ZLFilterByDistanceDefaultRadiusOfViewing;
        [_userDefaults setDouble:_radiusOfViewingInMi forKey:kUserDefaultsKeyRadiusForFilteringByDistance];
        [_userDefaults synchronize];
    }
    return _radiusOfViewingInMi;
}


@end
