//
//  WPApiClient.h
//  Zoom Local
//
//  Created by Aric Brown on 9/12/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import <Overcoat/Overcoat.h>

@interface ZLWPApiClient : OVCHTTPSessionManager

// Init
+ (instancetype)sharedClient;

/**
 Used for requests that impose authorized access.
 Login screen is not shown as in case of `authorizedPOST:...` method in case if user is not logged in. But if there is
 saved credentials then login procedure is performed automatically.
 If access token has been expired and server can handle request without valid access token then result it the same as in case of 
 simple `GET:...` method.
 "Login" screen is never shown for this method in current implementation.
 */
- (void)authorizedGET:(NSString *)URLString
           parameters:(id)parameters
           completion:(void (^)(OVCResponse *, NSError *))completion;

/**
 Method make authorized request to server. If request fails and `showLoginScreen` is `YES` then user is requested to login ("Login" screen is shown) 
 or, if there are credentials from previous login, is automatically logged in. After successful login the initial request is repeated again.
 
 If user did not successfully log in or dismisses "Login" screen then client receives error that was received when making the very
 first request (that is "Authentication fails" error).
 
 @param showLoginScreen Define whether "Login" screen should be shown in case if there is not saved user's credentials
 because authorized request has not been done before. If autherized request has been done before then there should be accessible saved
 user's credentials and the new access token is requested automatically.
 */
- (void)authorizedPOST:(NSString *)URLString
            parameters:(id)parameters
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:(BOOL)showLoginScreen
            completion:(void (^)(OVCResponse *, NSError *))completion;

@end
