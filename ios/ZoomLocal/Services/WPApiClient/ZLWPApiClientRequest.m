//
//  ZLWPApiClientRequest.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 04/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLWPApiClientRequest.h"

NSString *const ZLWPApiClientRequestMethodPost = @"POST";


@implementation ZLWPApiClientRequest

+ (instancetype)WPApiClientRequestWithMethod:(NSString *)method
                                   URLString:(NSString *)URLString
                                      params:(NSDictionary *)params
                                  completion:(void (^)(OVCResponse *, NSError *))completion
{
    return [[self alloc] initWithMethod:method URLString:URLString params:params completion:completion];
}

- (instancetype)initWithMethod:(NSString *)method
                     URLString:(NSString *)URLString
                        params:(NSDictionary *)params
                    completion:(void (^)(OVCResponse *, NSError *))completion
{
    self  = [super init];
    if (self) {
        _method = method;
        _URLString = URLString;
        _params = params;
        _completion = completion;
    }
    return self;    
}

@end
