//
//  ZLWPApiClientRequest.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 04/04/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Overcoat/Overcoat.h>

extern NSString *const ZLWPApiClientRequestMethodPost;


@interface ZLWPApiClientRequest : NSObject

+ (instancetype)WPApiClientRequestWithMethod:(NSString *)method
                                   URLString:(NSString *)URLString
                                      params:(NSDictionary *)params
                                  completion:(void (^)(OVCResponse *, NSError *))completion;

- (instancetype)initWithMethod:(NSString *)method
                     URLString:(NSString *)URLString
                        params:(NSDictionary *)params
                    completion:(void (^)(OVCResponse *, NSError *))completion;

@property (readonly) NSString *method;
@property (readonly) NSString *URLString;
@property (readonly) NSDictionary *params;
@property (readonly) void (^completion)(OVCResponse *, NSError *);

@end
