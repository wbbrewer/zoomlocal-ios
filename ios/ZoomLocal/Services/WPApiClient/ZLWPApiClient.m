//
//  WPApiClient.m
//  Zoom Local
//
//  Created by Aric Brown on 9/12/15.
//  Copyright (c) 2015 EquatorApps. All rights reserved.
//

#import "ZLWPApiClient.h"

#import <MZFormSheetPresentationViewController.h>

#import "TermModel.h"
#import "ProductModel.h"
#import "ListingItemModel.h"
#import "EventModel.h"
#import "TokenModel.h"
#import "ZLUserSessionManager.h"
#import "ZLWPApiClientRequest.h"
#import "ZLLoginViewController.h"
#import "ZLCoupon.h"

#ifdef yklishevich
NSString *const ZLWPApiClientEndpoint = @"https://listings-dev.zoomlocal.com/";
//NSString *const ZLWPApiClientEndpoint = @"http://localhost/~yklishevich/listings-dev/";
#else
NSString *const ZLWPApiClientEndpoint = @"https://listings.zoomlocal.com/";
#endif


@interface ZLWPApiClient ()<ZLUserSessionManagerObserver>

@end


@implementation ZLWPApiClient {
    NSMutableSet *_waitingAuthorizedRequests; // for storing request when user perfom login precedure
}

#pragma mark - Lifecycle

+ (instancetype)sharedClient {
    static ZLWPApiClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ZLWPApiClient alloc] init];
    });
    
    return _sharedClient;
}

- (instancetype)init {
    self = [super initWithBaseURL:[NSURL URLWithString:ZLWPApiClientEndpoint]];
    if (self) {
        _waitingAuthorizedRequests = [NSMutableSet set];
    }
    return self;
}

#pragma mark - Overcoat

// https://listings-dev.zoomlocal.com/wp-json/wp/v2/terms/product_cat?per_page=100

// wp-json/wp/v2/products

+ (NSDictionary *)modelClassesByResourcePath
{
    return @{
             @"wp-json/wp/v2/product_cat": [TermModel class],
             @"wp-json/rest/v2/all": [ProductModel class],
             @"wp-json/rest/v2/listing": [ListingItemModel class],
             @"wp-json/rest/v2/listing/*": [ListingItemModel class],
             @"wp-json/rest/v2/event": [EventModel class],
             @"wp-json/wp/v2/terms/yith_shop_vendor": [TermModel class],
             @"wp-json/rest/v2/vendor": [TermModel class],
             @"wp-json/rest/v2/vendor/*": [TermModel class],
             @"oauth/token": [TokenModel class],
             @"wp-json/rest/v2/coupons": [ZLCoupon class],
             @"wp-json/rest/v2/coupons/*": [ZLCoupon class],
             };
}

#pragma mark <ZLUserSessionManagerObserver>

- (void)userSessionManager:(ZLUserSessionManager *)userSessionManager
    didChangeLoginStatusTo:(ZLUserSessionManagerLoginStatus)loginStatus
{
    if (loginStatus == ZLUserSessionManagerLoginStatusLoggedIn) {
        
        for (ZLWPApiClientRequest *request in _waitingAuthorizedRequests) {
            if ([request.method isEqualToString:ZLWPApiClientRequestMethodPost]) {
                
                NSMutableDictionary *paramsWithAccessToken = [NSMutableDictionary dictionaryWithDictionary:request.params];
                [paramsWithAccessToken setValue:[ZLUserSessionManager sharedManager].token.accessToken forKey:@"access_token"];
                
                [self POST:request.URLString parameters:paramsWithAccessToken completion:request.completion];
            }
            else {
                NSAssert(NO, @"Not implemented!");
            }
        }
        [_waitingAuthorizedRequests removeAllObjects];
    }
}

#pragma mark - Requests

- (void)authorizedGET:(NSString *)URLString
           parameters:(id)parameters
           completion:(void (^)(OVCResponse *, NSError *))completion {
    
    NSAssert(parameters[@"access_token"] == nil, @"");
    NSParameterAssert(parameters == nil || [parameters isKindOfClass:[NSDictionary class]]);
    
    NSMutableDictionary *paramsWithAccessToken = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([ZLUserSessionManager sharedManager].token.accessToken != nil) {
        [paramsWithAccessToken setValue:[ZLUserSessionManager sharedManager].token.accessToken forKey:@"access_token"];
    }
    
    [self GET:URLString
    parameters:paramsWithAccessToken
    completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
        
        BOOL isAccessTokenExpired = (response.HTTPResponse.statusCode == 403);
        
        if (error == nil) {
            completion(response, nil);
        }
        else {
            
            BOOL isAccessTokenExpired = (response.HTTPResponse.statusCode == 403);
            if (isAccessTokenExpired) {
                
                [[ZLUserSessionManager sharedManager] refreshAccessToken:^(NSError *error) {
                    
                    if (error == nil) {
                        NSString *accessToken = [ZLUserSessionManager sharedManager].token.accessToken;
                        if (accessToken != nil) [paramsWithAccessToken setValue:accessToken forKey:@"access_token"];
                        [self GET:URLString parameters:paramsWithAccessToken completion:completion];
                    }
                    else {
                        [self GET:URLString parameters:parameters completion:completion];
                    }
                    
                }];
            }
            else {
                completion(response, error);
            }
        }
    }];
}

/**
 void is returned because of complicated logic including showing Login screen.
 */
- (void)authorizedPOST:(NSString *)URLString
            parameters:(id)parameters
showLoginScreenIfThereIsNoCredentialsForAutomaticLogin:(BOOL)showLoginScreen
            completion:(void (^)(OVCResponse *, NSError *))completion
{
    
    NSAssert(parameters == nil || [parameters isKindOfClass:[NSDictionary class]], @"");
    
    NSString *accessToken = [ZLUserSessionManager sharedManager].token.accessToken;
    
    if (accessToken == nil && showLoginScreen) {
        // This code is performed once while app is running

        ZLWPApiClientRequest *requestObject = [ZLWPApiClientRequest WPApiClientRequestWithMethod:ZLWPApiClientRequestMethodPost
                                                                                       URLString:URLString
                                                                                          params:parameters
                                                                                      completion:completion];
        [_waitingAuthorizedRequests addObject:requestObject];
        
        [self goToLoginScreen];
        [[ZLUserSessionManager sharedManager] addUserSessionObserver:self];
        
    }
    else if (accessToken == nil) {
        
        [self POST:URLString
        parameters:parameters
        completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
            completion(response, error);
        }];
        
    }
    else {
        
        NSMutableDictionary *paramsWithAccessToken = [NSMutableDictionary dictionaryWithDictionary:parameters];
        if (accessToken != nil) {
            [paramsWithAccessToken setValue:accessToken forKey:@"access_token"];
        }
        
        [self POST:URLString
        parameters:paramsWithAccessToken
        completion:^(OVCResponse * _Nullable response, NSError * _Nullable error) {
            
            BOOL isAccessTokenExpired = (response.HTTPResponse.statusCode == 403);
            
            if (error == nil) {
                completion(response, nil);
            }
            else if (isAccessTokenExpired) {
                
                [[ZLUserSessionManager sharedManager] refreshAccessToken:^(NSError *error) {
                    
                    if (error == nil) {
                        NSString *accessToken = [ZLUserSessionManager sharedManager].token.accessToken;
                        if (accessToken != nil) [paramsWithAccessToken setValue:accessToken forKey:@"access_token"];
                        [self POST:URLString parameters:paramsWithAccessToken completion:completion];
                    }
                    else {
                        [self POST:URLString parameters:parameters completion:completion];
                    }                    
                }];
            }
            else {
                completion(response, error);
            }
        }];
    }
}

#pragma mark - Private -

- (void)goToLoginScreen
{
    [ZLLoginViewController presentLoginScreenWithDidDismissHandler:^{
        // If user dismisses "Login" screen then `_waitingAuthorizedRequests` will contain unsutisfied requests.
        // We should satisfy them. Clients must get error "Unauthorized request" from server.
        // If controller is dismissed
        for (ZLWPApiClientRequest *request in _waitingAuthorizedRequests) {
            [self POST:request.URLString parameters:request.params completion:request.completion];
        }
        // User can dissmiss "Login" screen before receiving response from server. Cleaning will prevent clients from sending subsequent requests after authentication
        // in `userSessionManager:didChangeLoginStatusTo:`
        [_waitingAuthorizedRequests removeAllObjects];
    }];
}

@end
