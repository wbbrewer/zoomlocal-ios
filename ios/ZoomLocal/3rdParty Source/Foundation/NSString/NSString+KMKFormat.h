//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>


@interface NSString (KMKFormat)

/**
 @abstract Allows to output strings contained in objects and containg new line ('\n) taking into account new line rather than
 just outputting '\n' when for example logged into console.
 
 @details If string with new line escape sequence '\n' is put into container (array, dictionary or class which `description` method
 output it when invoked) then NSLog output description of container with new line untouched. That is the code
 
 @code
 NSString *s = @"1\n2";
 NSArray *a = @[s];
 NSLog(@"%@", a);

 @endcode
 
 will output:
     (
        "1\n2"
     )
 
 Sometimes it is desired to output internal strings with new line ('\n') replacing new line with actual "new line". 
 For example if internal sting represents some formatted text (for example JSON, which is logged into console) and 
 developer wants to log this formatted text preserving new line in it.
 */
- (NSString *)unescapeNewLine;

@end
