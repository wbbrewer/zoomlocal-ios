//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>


FOUNDATION_EXTERN NSString* NSStringFromCLLocationCoordinate2D(CLLocationCoordinate2D coordinate);


@interface NSString (KMKStringFrom)



@end
