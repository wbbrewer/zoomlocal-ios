//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "NSString+KMKFormat.h"


@implementation NSString (KMKFormat)

- (NSString *)unescapeNewLine
{
    return [self stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
}

@end
