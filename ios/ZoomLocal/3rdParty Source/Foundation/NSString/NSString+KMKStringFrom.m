//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "NSString+KMKStringFrom.h"


NSString* NSStringFromCLLocationCoordinate2D(CLLocationCoordinate2D coordinate)
{
    NSString *string = [NSString stringWithFormat:@"Latitude: '%f', longitude: '%f'", coordinate.latitude, coordinate.longitude];
    return string;
}

@implementation NSString (KMKStringFrom)

@end
