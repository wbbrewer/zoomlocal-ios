//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>


@interface NSString (KMKValidation)

/**
 @return YES if email is valid.
 */
- (BOOL)isValidEmail;

/**
 Check whether sting is valid USA postal code.
 Leading and trailing whitespace characters are ignored.
 @return 
    `YES` - if string is valid five-digit (plain) zip code (example "32123") or valid nine-digit "ZIP+4" code (example "32123-1234").
    `NO`  - otherwise
 */
- (BOOL)isValid_USA_ZipCode;

@end
