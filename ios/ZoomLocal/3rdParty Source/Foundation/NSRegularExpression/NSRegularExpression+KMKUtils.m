//  Copyright (c) 2014, Yauheni Klishevich.
//  Released under the MIT license.

#import "NSRegularExpression+KMKUtils.h"


@implementation NSRegularExpression (KMKUtils)

+ (BOOL)validateValue:(NSString *)value regExpr:(NSString *)regExpr 
{
    NSParameterAssert(value);
    NSParameterAssert(regExpr);
    
    // Validating regExpr
    NSError *error = nil;
    NSRegularExpression *regularEx = [NSRegularExpression regularExpressionWithPattern:regExpr options:0 error:&error];
    if (error != nil) {
        NSAssert(NO, @"Not valid regular expression: '%@'", regExpr);
        regularEx = regularEx; // to avoid compile warning
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regExpr];
    BOOL isValueValid = [predicate evaluateWithObject:value];
    return isValueValid;
}

@end
