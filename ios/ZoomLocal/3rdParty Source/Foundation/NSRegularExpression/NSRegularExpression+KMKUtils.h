//  Copyright (c) 2014, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>


@interface NSRegularExpression (KMKUtils)

/**
 @abstract Check if <code>value</code> match the regExpr (according to ICU v3).
 @param value If `nil` exception is generated.
 @param regExpr If `regExpr` is not valid then  `NSInternalInconsistencyException` exception is raised. 
     If `nil` exception is generated.
     Example:
         @"([0-9]+)|([0-9]*\\.[0-9]+)"
 @return
 YES - value match specified regular expressin;
 NO  - value does'nt match specified regular expression.
 */
+ (BOOL)validateValue:(NSString *)value regExpr:(NSString *)regExpr;

@end
