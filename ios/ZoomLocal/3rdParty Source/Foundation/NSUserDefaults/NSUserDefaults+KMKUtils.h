//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>


@interface NSUserDefaults (KMKUtils)

/**
 Clear shared user defaults. The library method `resetStandardUserDefaults` only releases defautls object from 
 memory but does not clear the content of persistent store.
 */
+ (void)resetDefaults;

@end
