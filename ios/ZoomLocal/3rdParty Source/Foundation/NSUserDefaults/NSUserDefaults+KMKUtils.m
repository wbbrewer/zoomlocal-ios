//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "NSUserDefaults+KMKUtils.h"


@implementation NSUserDefaults (KMKUtils)

+ (void)resetDefaults
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

@end
