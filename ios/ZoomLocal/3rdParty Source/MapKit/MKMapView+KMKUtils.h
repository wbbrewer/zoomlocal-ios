//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.


#import <MapKit/MKMapView.h>


@interface MKMapView (KMKUtils)

@end


FOUNDATION_EXTERN MKCoordinateRegion CoordinateRegionBoundingMapPoints(MKMapPoint *points, NSUInteger count);
