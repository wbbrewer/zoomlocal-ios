//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>


@interface UIView (KMKSeparator)

/**
 Utility method for quick adding separator to the particular edge of the receiver.
 Receiver can change its frame safely after adding separator.
 @param height Height of the separator to add.
 @param color Color of the separator to add.
 */
- (void)addSeparatorToEdge:(UIRectEdge)rectEdge height:(CGFloat)height color:(UIColor *)color;

@end
