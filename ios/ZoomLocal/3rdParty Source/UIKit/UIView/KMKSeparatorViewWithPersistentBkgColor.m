//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "KMKSeparatorViewWithPersistentBkgColor.h"


@implementation KMKSeparatorViewWithPersistentBkgColor

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self common_init];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        [self common_init];
    }
    return self;
}

- (void)common_init
{
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = self.bounds;
    layer.backgroundColor = self.backgroundColor.CGColor;
    [self.layer insertSublayer:layer atIndex:0];
}

@end
