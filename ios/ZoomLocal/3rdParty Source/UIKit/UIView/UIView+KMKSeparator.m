//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "UIView+KMKSeparator.h"

#import "KMKSeparatorViewWithPersistentBkgColor.h"


@implementation UIView (KMKSeparator)

- (void)addSeparatorToEdge:(UIRectEdge)rectEdge height:(CGFloat)height color:(UIColor *)color
{
    CGRectEdge cgRectEdge = CGRectMaxYEdge;
    if (rectEdge == UIRectEdgeBottom) {
        cgRectEdge = CGRectMaxYEdge;
    }
    else if (rectEdge == UIRectEdgeTop) {
        cgRectEdge = CGRectMinYEdge;
    }
    
    // Adding sublayer does not allow using autoresizing mask, so we use KMKSeparatorViewWithPersistentBkgColor to prevent disappearing separator when selecting for example cell.
    KMKSeparatorViewWithPersistentBkgColor *separator = [KMKSeparatorViewWithPersistentBkgColor new];
    separator.backgroundColor = color;
    CGRect separatorFrame = CGRectZero;
    CGRect tempFrame = CGRectZero;
    CGRectDivide(self.bounds, &separatorFrame, &tempFrame, height, cgRectEdge);
    separator.frame = separatorFrame;
    separator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:separator];
}

@end
