//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import <UIKit/UIKit.h>


@interface UIView(KMKNib)

+ (id)viewWithDefaultNib;
+ (id)viewWithDefaultNibWithOwner:(id)owner;
+ (id)viewWithNib:(NSString *)nibName;
+ (id)viewWithNib:(NSString *)nibName owner:(id)owner;

// handles only Portrait and Landscape orientation
// xibName: ClassName + Portrait or Landscape
+ (id)viewWithOrientation:(UIInterfaceOrientation)orientation;
+ (id)viewWithNib:(NSString *)nibName orientation:(UIInterfaceOrientation)orientation;

@end