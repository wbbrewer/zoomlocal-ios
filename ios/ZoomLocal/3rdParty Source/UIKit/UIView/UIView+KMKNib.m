//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "UIView+KMKNib.h"


@implementation UIView(KMKNib)


+ (id)viewWithDefaultNib {
	return [self viewWithDefaultNibWithOwner:nil];
}

+ (id)viewWithDefaultNibWithOwner:(id)owner {
    NSString *nibName = NSStringFromClass([self class]);
    return [self viewWithNib:nibName owner:owner];
}

+ (id)viewWithNib:(NSString *)nibName {
    return [self viewWithNib:nibName owner:nil];
}

+ (id)viewWithNib:(NSString *)nibName owner:(id)owner {
	NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName owner:owner options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject *nibItem = nil;
    while ((nibItem = [nibEnumerator nextObject]) != nil) {
        if ([nibItem isKindOfClass:[self class]]) {
            return nibItem;
            break;
        }
    }
    return nil;	
}

+ (id)viewWithOrientation:(UIInterfaceOrientation)orientation {
    NSString *nibName = NSStringFromClass([self class]);
    return [UIView viewWithNib:nibName
                   orientation:orientation];
}

+ (id)viewWithNib:(NSString *)nibName orientation:(UIInterfaceOrientation)orientation {
    NSString *orientationString = @"";
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        orientationString = @"Portrait";
    }
    else {
        orientationString = @"Landscape";
    }
    
    NSString *fullNibName = [NSString stringWithFormat:@"%@%@",nibName,orientationString];

    return [UIView viewWithNib:fullNibName];
}


@end