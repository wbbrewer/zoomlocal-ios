//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import <UIKit/UIKit.h>


/**
 Class is supposed to be used inside nib files as class for cell separators.
 Cell separator when adding in nib file is added to content view. But when selecting cell content view set background color of its subviews to clear color.
 So if for separator is used merely UIView class then separator disappears when selecting cell. Using this class for separator enable avoding this issue.
 */
@interface KMKSeparatorViewWithPersistentBkgColor : UIView

@end
