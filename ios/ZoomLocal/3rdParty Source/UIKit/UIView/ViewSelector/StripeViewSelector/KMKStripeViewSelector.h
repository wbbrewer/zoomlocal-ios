//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>

#import "KMKUniversalCollectionViewCellWithBorder.h"


@interface KMKStripeViewSelector : NSObject <KMKViewSelector>

@property (strong, nonatomic) UIColor *tintColor;

@end
