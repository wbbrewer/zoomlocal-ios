//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKStripeViewSelector.h"

static const NSInteger kStripeViewTag = 1000;


@implementation KMKStripeViewSelector

- (void)addVisualSelectionIndicatorToView:(UIView *)view
{
    UIView *stripeView = [view viewWithTag:kStripeViewTag];
    
    if (stripeView == nil || [self isReallyStripeView:stripeView] == NO) {
        const CGFloat stripeHeight = 4.0;
        stripeView = [[UIView alloc] initWithFrame:CGRectMake(0.0,
                                                              CGRectGetHeight(view.frame) - stripeHeight,
                                                              CGRectGetWidth(view.frame),
                                                              stripeHeight)];
        stripeView.tag = kStripeViewTag;
        stripeView.backgroundColor = self.tintColor;
        [view addSubview:stripeView];
    }
}

- (void)removeVisualSelectionIndicatorFromView:(UIView *)view
{
    UIView *stripeView = [view viewWithTag:kStripeViewTag];
    
    if (stripeView != nil && [self isReallyStripeView:stripeView]) {
        [stripeView removeFromSuperview];
    }
}

#pragma mark - Private -

/// View can contain another view with the same tag so it is needed additional check.
- (BOOL)isReallyStripeView:(UIView *)stripeView
{
    BOOL isReal = (CGRectGetMaxY(stripeView.frame) - stripeView.superview.frame.size.height < 1.0);
    return isReal;
}

@end
