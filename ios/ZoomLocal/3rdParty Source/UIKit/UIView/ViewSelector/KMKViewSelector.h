//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <Foundation/Foundation.h>

/**
 Interface of strategy for selecting view.
 For example add stripe of specific color at the bottom of a view.
 */
@protocol KMKViewSelector <NSObject>

- (void)addVisualSelectionIndicatorToView:(UIView *)view;
- (void)removeVisualSelectionIndicatorFromView:(UIView *)view;

@end

