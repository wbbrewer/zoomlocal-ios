//
//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.
//

#import "UILabel+KMKUtils.h"

#import "NSString+KMKUtils.h"
#import "NSAttributedString+KMKUtils.h"


@implementation UILabel (KMKUtils)

#pragma mark Class methods

+ (CGSize)kmk_sizeForText:(NSString *)text
                    width:(CGFloat)width
            numberOfLines:(NSInteger)numberOfLines
                     font:(UIFont *)font
            lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    const NSInteger kNumberOfLines = (numberOfLines == 0 ? NSIntegerMax : numberOfLines);
    
    /*
     Constant is added to the number of lines to take into account that final result can be greater than numberOfLines multiplied by lineHeight. For example for label with:
     NSFont = "<UICTFont: 0x7f9951e82a90> font-family: \"HelveticaNeue-Medium\"; font-weight: normal; font-style: normal; font-size: 17.00pt";
     height for two lines, returned by this method, is 41.021000000000008 < 2*(lineHeight===20.757000000000005)
     
     */
    CGSize constrainingSize = CGSizeMake(width, (kNumberOfLines + 0.99) * [font lineHeight]);
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    // Do not use `attributesAtIndex:...` method. See comment to `kmk_boundingRectWithSize:...` method.
    NSDictionary *attributesDictionary = @{
                                           NSFontAttributeName : font,
                                           NSParagraphStyleAttributeName : paragraphStyle
                                           };
    
    CGRect rect = [text kmk_boundingRectWithSize:constrainingSize
                                         options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                      attributes:attributesDictionary
                                         context:NULL];
    
    CGRect integralRect = CGRectIntegral(rect);
    
    return integralRect.size;
}

#pragma mark Instance methods

// TODO: take into account possibility of attributed text
- (CGSize)kmk_multilineSizeConstrainedToSize:(CGSize)sizeConstraint
{
    NSAssert(self.attributedText == nil, @"Attributed string isn't supported!");
    const CGFloat maxLinesHeight = (self.numberOfLines > 0 ? self.font.lineHeight * self.numberOfLines : CGFLOAT_MAX);
    
    sizeConstraint.height = MIN(sizeConstraint.height, maxLinesHeight);
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    NSDictionary *attributesDictionary = @{
                                           NSFontAttributeName : self.font,
                                           NSParagraphStyleAttributeName : paragraphStyle
                                           };
    
    CGRect rect = [self.text boundingRectWithSize:sizeConstraint
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:attributesDictionary
                                          context:NULL];
    
    CGRect integralRect = CGRectIntegral(rect);
    
    return integralRect.size;
}

// TODO: take into account possibility of attributed text
- (CGSize)kmk_multilineSizeForWidth:(CGFloat)widthConstraint
{
    NSAssert(self.attributedText == nil, @"Attributed string isn't supported!");
    return [self kmk_multilineSizeConstrainedToSize:CGSizeMake(widthConstraint, CGFLOAT_MAX)];
}

- (void)kmk_sizeToFitMultiline
{
    NSAssert(self.attributedText == nil, @"Attributed string isn't supported!");
    CGRect bounds = self.bounds;
    CGSize preferredSize = [self kmk_multilineSizeForWidth:bounds.size.width];
    preferredSize.width = bounds.size.width;
    
    if (!CGSizeEqualToSize(self.bounds.size, preferredSize))
    {
        CGPoint centerOffset = CGPointMake((preferredSize.width - bounds.size.width) / 2.0,
                                           (preferredSize.height - bounds.size.height) / 2.0);
        centerOffset = CGPointApplyAffineTransform(centerOffset, self.transform);
        bounds.size = preferredSize;
        
        self.bounds = bounds;
        self.center = CGPointMake(self.center.x + centerOffset.x, self.center.y + centerOffset.y);
        
        //        // workaround (iOS 5.0): UILabel does not redraw its contents when the size is changed through the bounds property.
        //        [self setNeedsDisplay];
    }
}

#pragma mark Instance methods with support of attributed text

- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width
{
    CGSize size = CGSizeZero;
    size.width = width;
    
    if (self.attributedText != nil) {
        size = [self.attributedText kmk_oneLineSizeConstrainedToWidth:width];
    }
    else {
        size = [self.text kmk_multilineSizeConstrainedToWidth:width
                                                numberOfLines:self.numberOfLines
                                                         font:self.font
                                                lineBreakMode:self.lineBreakMode];
    }
    
    return size;
}

@end
