// Copyright (c) 2015, Yauheni Klishevich.
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

#import <UIKit/UIKit.h>


@interface UILabel (KMKUtils)

#pragma mark Class methods

/**
 @param numberOfLines = 0 corresponds the lack of limit for number of lines
 
 Method may be especially useful when it is needed to calculate the height of the label without creating it. For example
 such situation may occur when calculating the height of the cell without creating the cell itself.
 
 @return The size for label with the specified parameters. Returned value is always intergral (raised to the nearest hiegher integer value).
 
 @deprecated Depricated. Use method of NSString+KMKUtils category
 */
+ (CGSize)kmk_sizeForText:(NSString *)text
                    width:(CGFloat)width
            numberOfLines:(NSInteger)numberOfLines
                     font:(UIFont *)font
            lineBreakMode:(NSLineBreakMode)lineBreakMode NS_DEPRECATED_IOS(7_0, 7_0);

#pragma mark Instance methods

/// Returns the size peferred for the current drawing parameters (the current font, the number of lines, etc.)
/// and constrained to the specified rectangle.
/// Returned value is always intergral (raised to the nearest hiegher integer value).
- (CGSize)kmk_multilineSizeConstrainedToSize:(CGSize)sizeConstraint;

/// Convinience method. Returns the result of calling -[kmk_multilineSizeConstrainedToSize:]
/// with the specified width constraint and an extremely high value as the height constraint
/// (i.e. with unconstrained height).
/// Returned value is always intergral (raised to the nearest hiegher integer value).
- (CGSize)kmk_multilineSizeForWidth:(CGFloat)widthConstraint;

/// Sets the size of the receiver to fit multiline text with current drawing paramenters.
/// The current receiver's width is unchanged and used as the width constraint.
/// Resulting height and width is always intergral (raised to the nearest hiegher integer value). Origin is left as is.
- (void)kmk_sizeToFitMultiline;

#pragma mark Instance methods with support of attributed text

/**
 Always returns integers in width and height.
 */
- (CGSize)kmk_oneLineSizeConstrainedToWidth:(CGFloat)width;

@end
