 //  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKOneRowCollectionView.h"

@protocol KMKOneRowCollectionViewDatasource;


@interface KMKOneRowCollectionView () <UICollectionViewDataSource, UICollectionViewDelegate> {
    CGFloat _cellWidth;
    BOOL _wasCellWidthCustomized;
}

@property (readonly, nonatomic) UICollectionView *collectionView;

@end


@implementation KMKOneRowCollectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        [self init_common];
        _collectionView.frame = frame;
        [_collectionView addObserver:self forKeyPath:@"frame" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew) context:NULL];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
        [self init_common];
        _collectionView.frame = self.bounds;
        [_collectionView addObserver:self forKeyPath:@"frame" options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew) context:NULL];
    }
    return self;
}

- (void)init_common
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 0.0;
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self addSubview:_collectionView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"frame"] && !_wasCellWidthCustomized) {
        CGRect frame = [[change valueForKey:NSKeyValueChangeNewKey] CGRectValue];
        _cellWidth = frame.size.width;
    }
}

- (void)dealloc
{
    self.collectionView.dataSource = nil;
    self.collectionView.delegate = nil;
    [_collectionView removeObserver:self forKeyPath:@"frame"];
}

- (void)didMoveToWindow
{
    NSLog(@"%@", [self cellForItemAtColumn:0]);
}

#pragma mark -
#pragma mark Public

- (void)setPagingEnabled:(BOOL)pagingEnabled
{
    _collectionView.pagingEnabled = pagingEnabled;
}

- (BOOL)pagingEnabled
{
    return _collectionView.pagingEnabled;
}

- (void)setCellWidth:(CGFloat)width
{
    _cellWidth = width;
    _wasCellWidthCustomized = YES;
}

- (void)reloadData
{
    [self.collectionView reloadData];
}

- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier
{
    [_collectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

- (void)registerNib:(nullable UINib *)nib forCellWithReuseIdentifier:(NSString *)identifier
{
    [_collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
}

- (id)dequeueReusableCellWithReuseIdentifier:(NSString *)identifier forColumn:(NSInteger)column
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:column inSection:0];
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                forIndexPath:indexPath];
    return cell;
}

- (UICollectionViewCell *)cellForItemAtColumn:(NSInteger)column
{
    const NSUInteger indexes[2] = {0, column};
    return [self.collectionView cellForItemAtIndexPath:[[NSIndexPath alloc] initWithIndexes:indexes  length:2]];
}

- (NSArray *)visibleCells
{
    return [_collectionView visibleCells];
}

- (void)selectItemAtColumn: (NSInteger)column
                  animated: (BOOL)animated
            scrollPosition: (UICollectionViewScrollPosition)scrollPosition
{
    const NSUInteger indexes[2] = {0, column};
    if ([_collectionView numberOfItemsInSection:0] != 0) {
        [_collectionView selectItemAtIndexPath:[[NSIndexPath alloc] initWithIndexes:indexes  length:2]
                                      animated:animated
                                scrollPosition:scrollPosition];
    }
    else if (column != 0) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:[NSString stringWithFormat:@"Argument 'column' is out of range: '%ld'!", (long)column]
                                     userInfo:nil];
    }
}

- (NSInteger)columnOfSelectedItem
{
    NSIndexPath *indexPath = [_collectionView indexPathsForSelectedItems].firstObject;
    return (indexPath != nil ? indexPath.row : NSNotFound);
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = [self.dataSource numberOfColumnsInOneRowCollectionView:self];
    return number;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.dataSource oneRowCollectionView:self cellForItemInColumn:indexPath.row];
    return cell;
}

#pragma mark
#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(_cellWidth, collectionView.bounds.size.height);
    return size;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView
                        layout: (UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex: (NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_delegate respondsToSelector:@selector(oneRowCollectionView:didSelectItemAtColumn:)]) {
        [_delegate oneRowCollectionView:self didSelectItemAtColumn:indexPath.row];
    }
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_delegate respondsToSelector:@selector(oneRowCollectionView:didEndDisplayingCell:forItemAtColumn:)]) {
        [_delegate oneRowCollectionView:self didEndDisplayingCell:cell forItemAtColumn:indexPath.row];
    }
}

@end

