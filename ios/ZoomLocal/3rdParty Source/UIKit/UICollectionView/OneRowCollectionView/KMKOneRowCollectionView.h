//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

@protocol KMKOneRowCollectionViewDatasource;
@protocol KMKOneRowCollectionViewDelegate;


@interface KMKOneRowCollectionView : UIView

@property (weak, nonatomic) IBOutlet id<KMKOneRowCollectionViewDatasource> dataSource;
@property (weak, nonatomic) IBOutlet id<KMKOneRowCollectionViewDelegate> delegate;

/// Default value NO.
@property (assign, nonatomic) BOOL pagingEnabled;

- (void)reloadData;

- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier;
- (void)registerNib:(UINib *)nib forCellWithReuseIdentifier:(NSString *)identifier;
- (id)dequeueReusableCellWithReuseIdentifier:(NSString *)identifier forColumn:(NSInteger)column;

/**
 Set width of cells in receiver.
 Default value is width of receiver itself (that is self.bounds.size.width).
 */
- (void)setCellWidth:(CGFloat)width;

- (UICollectionViewCell *)cellForItemAtColumn:(NSInteger)column;
- (NSArray *)visibleCells;
- (void)selectItemAtColumn: (NSInteger)column
                  animated: (BOOL)animated
            scrollPosition: (UICollectionViewScrollPosition)scrollPosition;
/// `NSNotFound` if there is no selected item.
- (NSInteger)columnOfSelectedItem;

@end



@protocol KMKOneRowCollectionViewDatasource <NSObject>

- (NSInteger)numberOfColumnsInOneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView;

- (UICollectionViewCell *)oneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView
                           cellForItemInColumn:(NSInteger)column;

@end


@protocol KMKOneRowCollectionViewDelegate <NSObject>

@optional
- (void)oneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView didSelectItemAtColumn:(NSInteger)column;
- (void)oneRowCollectionView:(KMKOneRowCollectionView *)oneRowCollectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtColumn:(NSInteger)column;

@end;
