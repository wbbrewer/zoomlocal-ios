//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKSelectableCollectionViewCell.h"


@implementation KMKSelectableCollectionViewCell

#pragma mark - Properties -

- (void)setViewSelector:(id<KMKViewSelector>)viewSelector
{
    _viewSelector = viewSelector;
    
    if (_viewSelector != nil)
    {
        if (self.selected) {
            [_viewSelector addVisualSelectionIndicatorToView:self];
        }
        else {
            [_viewSelector removeVisualSelectionIndicatorFromView:self];
        }
    }
}

#pragma mark -
#pragma mark UICollectionViewCell override

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (_viewSelector != nil) {
        if (selected) {
            [_viewSelector addVisualSelectionIndicatorToView:self];
        }
        else {
            [_viewSelector removeVisualSelectionIndicatorFromView:self];
        }
    }
}


@end
