//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKViewSelector.h"


@interface KMKSelectableCollectionViewCell : UICollectionViewCell

// Strategy of selection.
@property (nonatomic, strong) id<KMKViewSelector> viewSelector;

@end
