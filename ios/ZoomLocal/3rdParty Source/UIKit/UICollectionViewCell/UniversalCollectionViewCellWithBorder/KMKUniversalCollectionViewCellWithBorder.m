//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKUniversalCollectionViewCellWithBorder.h"

#import "UILabel+KMKUtils.h"
#import <NSAttributedString+KMKUtils.h>
#import <NSString+KMKUtils.h>

static CGFloat kBoderWidth = 4.0;


@interface KMKUniversalCollectionViewCellWithBorder ()

@end

@implementation KMKUniversalCollectionViewCellWithBorder
{
    UIView *_viewForBoder;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self != nil) {
        self.backgroundColor = [UIColor whiteColor];
        _boderWidth = kBoderWidth;

        _viewForBoder = [[UIView alloc] initWithFrame:self.bounds];
        _viewForBoder.layer.borderWidth = _boderWidth;
        _viewForBoder.layer.borderColor = [UIColor colorWithWhite:0.7 alpha:1.0].CGColor;
        [self addSubview:_viewForBoder];
        self.clipsToBounds = YES;
        
        _rectEdgeToDrawBorder = UIRectEdgeNone;
        
        _backgroundImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:_backgroundImageView];
        
        _textLabel = [UILabel new];
        _textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0];
        [self.contentView addSubview:_textLabel];
        
        _detailTextLabel = [UILabel new];
        _detailTextLabel.font = [UIFont systemFontOfSize:12.0];
        
        [self.contentView addSubview:_detailTextLabel];
    }
    return self;
}

#pragma mark -
#pragma mark - Override

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
}


#pragma mark -
#pragma mark Public

+ (CGSize)sizeForContentSize:(CGSize)contentSize edgesToDrawBorder:(UIRectEdge)edges boderWidth:(CGFloat)boderWidth
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(-boderWidth * ((edges & UIRectEdgeTop) ? 1 : 0),
                                                  -boderWidth * ((edges & UIRectEdgeLeft) ? 1 : 0),
                                                  -boderWidth * ((edges & UIRectEdgeBottom) ? 1 : 0),
                                                  -boderWidth * ((edges & UIRectEdgeRight) ? 1 : 0));
    CGRect resRect = CGRectMake(0, 0, contentSize.width, contentSize.height);
    resRect = UIEdgeInsetsInsetRect(resRect, contentInsets);
    return resRect.size;
}

- (void)setBoderWidth:(CGFloat)boderWidth
{
    _boderWidth = boderWidth;
    _viewForBoder.layer.borderWidth = boderWidth;
    [self setNeedsLayout];
}

- (void)setBorderColor:(UIColor *)borderColor
{
    _viewForBoder.layer.borderColor = borderColor.CGColor;
}

#pragma mark -
#pragma mark UICollectionViewCell override

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self setSelected:NO];
    self.rectEdgeToDrawBorder = UIRectEdgeAll;
    _backgroundImageView.image = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    const CGSize kConstentSize = self.contentView.bounds.size;
    const CGFloat kOriginXOfLabels = 10.0;
    const CGFloat kRightMargin = 10.0;
    const CGFloat kWidthOfLabels = kConstentSize.width - kOriginXOfLabels - kRightMargin;
    
    
    UIEdgeInsets viewForBorderInsets = UIEdgeInsetsMake(-_boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeTop) ? 0 : 1),
                                                        -_boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeLeft) ? 0 : 1),
                                                        -_boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeBottom) ? 0 : 1),
                                                        -_boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeRight) ? 0 : 1));
    _viewForBoder.frame = UIEdgeInsetsInsetRect(self.bounds, viewForBorderInsets);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(_boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeTop) ? 1 : 0),
                                                  _boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeLeft) ? 1 : 0),
                                                  _boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeBottom) ? 1 : 0),
                                                  _boderWidth * ((_rectEdgeToDrawBorder & UIRectEdgeRight) ? 1 : 0));
    
    self.contentView.frame = UIEdgeInsetsInsetRect(self.bounds, contentInsets);
    
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = 10.0;
    textFrame.size.width = kWidthOfLabels;
    NSAssert(self.textLabel.numberOfLines == 1, @"Layout doesn't support multiline case!");
    
    textFrame.size.height = [self.textLabel kmk_oneLineSizeConstrainedToWidth:kWidthOfLabels].height;
    
    CGRect detailsTextFrame = CGRectZero;
    detailsTextFrame.origin.x = 10.0;
    detailsTextFrame.origin.y = CGRectGetMaxY(textFrame);
    detailsTextFrame.size.width = kWidthOfLabels;
    NSAssert(self.detailTextLabel.numberOfLines == 1, @"Layout doesn't support multiline case!");
    
    detailsTextFrame.size.height = [self.detailTextLabel kmk_oneLineSizeConstrainedToWidth:kWidthOfLabels].height;
    
    CGFloat yOffset = (kConstentSize.height - CGRectGetMaxY(detailsTextFrame)) / 2.0;
    textFrame = CGRectOffset(textFrame, 0, yOffset);
    detailsTextFrame = CGRectOffset(detailsTextFrame, 0, yOffset);
    
    
    self.textLabel.frame = textFrame;
    self.detailTextLabel.frame = detailsTextFrame;
}

@end
