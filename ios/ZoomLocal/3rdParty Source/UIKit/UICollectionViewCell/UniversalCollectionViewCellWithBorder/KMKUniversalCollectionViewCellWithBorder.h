//  Copyright (c) 2016, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKSelectableCollectionViewCell.h"

@class ZLEvent;
@class ZLObjectConnection;


/**
 Capable of drawing borders on specified edges of a cell.
 */
@interface KMKUniversalCollectionViewCellWithBorder : KMKSelectableCollectionViewCell

@property (nonatomic, strong) UIImageView *backgroundImageView;

/// Default font: System, medium, 15pt.
@property (nonatomic, strong) UILabel *textLabel;
/// Default font: System, regular, 12pt.
@property (nonatomic, strong) UILabel *detailTextLabel;


#pragma mark Borders

/// Size needed to show content with specified size `contentsSize` and borders around this content according `rectEdgeToDrawBorder` value.
/// Useful when edges changes from cell to cell, for example first and end cells can have border near other borders than internal cells.
+ (CGSize)sizeForContentSize:(CGSize)contentSize edgesToDrawBorder:(UIRectEdge)edges boderWidth:(CGFloat)boderWidth;

/// Default is UIRectEdgeNone
/// Border is drawn around the content view. So content view will have smaller size so that cell accommodate border.
@property (assign, nonatomic) UIRectEdge rectEdgeToDrawBorder;

/// Default is 4.0.
@property (assign, nonatomic) CGFloat boderWidth;

/// Default is grey with 70% of white.
@property (strong, nonatomic) UIColor *borderColor;

@end
