//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>


@interface UINavigationItem (KMKUtils)

/**
 @see comment to `setPropellyPositionedLeftBarButtonItems:' method.
 */
- (void)setPropellyPositionedLeftBarButtonItem:(UIBarButtonItem *)barButtonItem;

/**
 Used to get properly positioned bar button items in the navigation bar (i.e. with left margin 10.0).
 Direct assigning to leftBarButtonItem or leftBarButtonItems results in offset bar buttons with margin equal approximately 15.0;
 */
- (void)setPropellyPositionedLeftBarButtonItems:(NSArray *)barButtonItems;

/**
 @see comment to `setPropellyPositionedLeftBarButtonItems:' method.
 */
- (void)setPropellyPositionedRightBarButtonItem:(UIBarButtonItem *)barButtonItem;

/**
 @see comment to `setPropellyPositionedLeftBarButtonItems:' method.
 */
- (void)setPropellyPositionedRightBarButtonItems:(NSArray *)barButtonItems;

/**
 Set `titleView` which adjust its font size to fit title.
 Size of title view is properly adjusted by system.
 @param numberOfLines Maximum number of lines
 */
- (void)kmk_prepereForAdjustingFontSizeWithNumberOfLines:(NSInteger)numberOfLines;

@end