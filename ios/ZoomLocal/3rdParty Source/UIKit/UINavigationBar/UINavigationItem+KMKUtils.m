//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "UINavigationItem+KMKUtils.h"

static const CGFloat kOffsetX = 15.0;


@implementation UINavigationItem (UIUtils)

- (void)setPropellyPositionedLeftBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self setPropellyPositionedLeftBarButtonItems:@[barButtonItem]];
}

- (void)setPropellyPositionedLeftBarButtonItems:(NSArray *)barButtonItems
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                    target:nil
                                                                                    action:nil];
    [negativeSpacer setWidth:-kOffsetX]; // corresponds to the left margin of leftBarButtonItem equal to 10.0
    self.leftBarButtonItems = [@[negativeSpacer] arrayByAddingObjectsFromArray:barButtonItems];
}

- (void)setPropellyPositionedRightBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self setPropellyPositionedRightBarButtonItems:@[barButtonItem]];
}

- (void)setPropellyPositionedRightBarButtonItems:(NSArray *)barButtonItems
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                    target:nil
                                                                                    action:nil];
    [negativeSpacer setWidth:-kOffsetX]; // corresponds to the left margin of leftBarButtonItem equal to 10.0
    self.rightBarButtonItems = [@[negativeSpacer] arrayByAddingObjectsFromArray:barButtonItems];
}

// TODO: height of navigation bar in landscape is 32 pt. So label won't fit into bar by height.
// In most cases title will have one line for landscape mode and issue wont reproduced.
- (void)kmk_prepereForAdjustingFontSizeWithNumberOfLines:(NSInteger)numberOfLines
{
    UIFont *font = [UINavigationBar appearance].titleTextAttributes[NSFontAttributeName];
    UIFont *defaultNavigationBarFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0];
    font = font ?: defaultNavigationBarFont;
    
    UILabel *tlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 40)];
    tlabel.numberOfLines = numberOfLines;
    tlabel.text = self.title;
    tlabel.textAlignment = NSTextAlignmentCenter;
    tlabel.textColor = [UIColor whiteColor];
    tlabel.font = font;
    tlabel.adjustsFontSizeToFitWidth = YES;
    self.titleView = tlabel;
}

@end
