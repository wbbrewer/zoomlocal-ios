//
//  ZLConstants.m
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

#import "ZLConstants.h"

// Gulfport, Mississippi - 30°24′6″N 89°4′34″W
#define ZL_DEFAULT_MAP_CENTER_LOCATION  {30.4016666667, -89.07611111111}
#define ZL_DEFAULT_MAP_SPAN             {5.0, 5.0}
const MKCoordinateRegion ZLDefaultMapRegion = {ZL_DEFAULT_MAP_CENTER_LOCATION, ZL_DEFAULT_MAP_SPAN};

const NSTimeInterval ZLAnimationDurationNormal = 0.25;
const NSInteger ZLNumberOfItemsPerPageInServerResponse = 10;


@implementation UIColor (App)

//c71f5a 199,31,90
+ (UIColor *)magentaAppColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(199/255.0) green:(31/255.0) blue:(90/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)blueTextColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(65/255.0) green:(114/255.0) blue:(183/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)whiteTintColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor whiteColor];
    });
    
    return color;
}

//0468ad 4,104,173
+ (UIColor *)blueTintColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(4/255.0) green:(104/255.0) blue:(173/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)higlithtedBlueTintColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(130/255.0) green:(189/255.0) blue:(221/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)tealColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(43/255.0) green:(148/255.0) blue:(176/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)ultraLightTealColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithRed:(232/255.0) green:(244/255.0) blue:(247/255.0) alpha:1.0];
    });
    
    return color;
}

+ (UIColor *)grayTextColor
{
    static dispatch_once_t initialized;
    static UIColor *color = nil;
    
    dispatch_once(&initialized, ^{
        color = [UIColor colorWithWhite:0.44 alpha:1.0];
    });
    
    return color;
}

@end


@implementation NSFormatter (App)

+ (NSNumberFormatter *)radiusFormatter
{
    static dispatch_once_t initialized;
    static NSNumberFormatter *formatter = nil;
    
    dispatch_once(&initialized, ^{
        formatter = [NSNumberFormatter new];
//        http://useyourloaf.com/blog/using-number-formatters.html
        [formatter setPositiveFormat:@"0"];
    });
    
    return formatter;
}

@end


@implementation NSDateFormatter (App)

+ (NSDateFormatter *)shortStyleDateFormatter
{
    static dispatch_once_t initialized;
    static NSDateFormatter *formatter = nil;
    
    dispatch_once(&initialized, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.formatterBehavior = NSDateFormatterBehavior10_4;
        formatter.dateStyle = NSDateFormatterShortStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
    });
    
    return formatter;
}

+ (NSDateFormatter*)JSONDateFormatter
{
    static dispatch_once_t initialized;
    static NSDateFormatter *dateFormatter = nil;
    
    dispatch_once(&initialized, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    
    return dateFormatter;
}

@end

