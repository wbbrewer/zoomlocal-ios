//
//  ZLConstants.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 12/12/15.
//  Copyright © 2015 GMBC Solutions, LLC. All rights reserved.
//

@import UIKit;
#import <MapKit/MKGeometry.h>

// Place arround 'Gulfport, Mississippi'
FOUNDATION_EXTERN const MKCoordinateRegion ZLDefaultMapRegion;

FOUNDATION_EXTERN const NSTimeInterval ZLAnimationDurationNormal;

/// Used ad typical value for parameter "per_page" in requests to server.
FOUNDATION_EXTERN const NSInteger ZLNumberOfItemsPerPageInServerResponse;


@interface UIColor (App)

// Value is the same as in storyboard. (Magenta)
+ (UIColor *)magentaAppColor;
+ (UIColor *)blueTextColor;
+ (UIColor *)whiteTintColor;
/// Used as color for active elements.
+ (UIColor *)blueTintColor;
+ (UIColor *)higlithtedBlueTintColor;
+ (UIColor *)tealColor;
+ (UIColor *)ultraLightTealColor;
+ (UIColor *)grayTextColor;

@end


@interface NSFormatter (App)

+ (NSNumberFormatter *)radiusFormatter;

@end


@interface NSDateFormatter (App)

/**
 Example: "3/18/16"
 */
+ (NSDateFormatter *)shortStyleDateFormatter;
+ (NSDateFormatter*)JSONDateFormatter;

@end
