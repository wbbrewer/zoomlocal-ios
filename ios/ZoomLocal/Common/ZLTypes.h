//
//  ZLTypes.h
//  ZoomLocal
//
//  Created by Yauheni Klishevich on 30/01/16.
//  Copyright © 2016 GMBC Solutions, LLC. All rights reserved.
//

#ifndef ZLTypes_h
#define ZLTypes_h

typedef NS_ENUM (NSInteger, ZLShareButtonType) {
    ZLShareButtonTypePostToFacebook,
    ZLShareButtonTypePostToTwitter
};


#endif /* ZLTypes_h */
